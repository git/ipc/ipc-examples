/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== PEB.c ========
 */

/* this define must precede inclusion of any xdc header file */
#define Registry_CURDESC PEB__Desc
#define MODULE_NAME "PEB"

#include <xdc/std.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* local header files */
#include "Compute.h"
#include "PEB.h"
#include "../shared/PE_Msg.h"
#include "../shared/PEB_shared.h"

/* module structure */
typedef struct {
    Bool                running;        /* main loop run flag */
    Semaphore_Handle    ipcReady;       /* IPC synchronizing object */
    MessageQ_Handle     queue;          /* message queue (inbound) */
} PEB_Module;

/* private data */
Registry_Desc           Registry_CURDESC;
static Int              Mod_curInit = 0;
static PEB_Module       PEB_module;
static volatile Bool    PEB_ipcReady = FALSE;
static volatile Bool    PEB_taskWaiting = FALSE;


/*
 *  ======== PEB_destroy ========
 */
Void PEB_destroy(Void)
{
    Int status = 0;

    Log_print0(Diags_INFO, "PEB_destroy: -->");

    /* TODO enter gate */

    /* reference count the module usage */
    if (--Mod_curInit > 0) {
        goto leave;  /* object still in use */
    }

    /* delete the message queue */
    status = MessageQ_delete(&PEB_module.queue);

    if (status < 0) {
        Log_error1("PEB_destroy: message queue delete error=%d",
                (IArg)status);
        goto leave;
    }

    /* delete the semaphore */
    Semaphore_delete(&PEB_module.ipcReady);

leave:
    /* disable log events */
    Log_print1(Diags_INFO, "PEB_delete: <-- status=%d", (IArg)status);
    Diags_setMask(MODULE_NAME"-FEX");

    /*
     * Note that there isn't a Registry_removeModule() yet:
     *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=315448
     *
     * ... but this is where we'd call it.
     */

    /* TODO leave gate */
}

/*
 *  ======== PEB_ipcIsReady ========
 *  Signal the PEB task that IPC is ready.
 *
 *  This function is called from another execution context. Must be
 *  careful to ensure local resources have been created.
 */
Void PEB_ipcIsReady(Void)
{
    PEB_ipcReady = TRUE;

    /* semaphore might not yet exist, must check if task is waiting */
    if (PEB_taskWaiting) {
        Semaphore_post(PEB_module.ipcReady);
    }
}

/*
 *  ======== PEB_isReady ========
 */
Bool PEB_isReady(Void)
{
    return (PEB_module.running);
}

/*
 *  ======== PEB_exec ========
 */
Int PEB_exec(Void)
{
    Int             status = 0;
    MessageQ_Msg    mqMsg;
    PE_Msg         *msg;

    Log_print0(Diags_INFO, "PEB_exec: -->");
    PEB_module.running = TRUE;

    /* main loop */
    while (PEB_module.running) {

        /* wait for inbound message */
        Log_print0(Diags_INFO, "PEB_exec: waiting for message");
        status = MessageQ_get(PEB_module.queue, &mqMsg, MessageQ_FOREVER);

        if (status < 0) {
            Log_error1("PEB_exec: message get error=%d", (IArg)status);
            goto leave;
        }

        /* process the message */
        msg = (PE_Msg *)mqMsg;
        Log_print1(Diags_INFO, "PEB_exec: message received, cmd=%d",
                (IArg)msg->command);

        switch (msg->command) {
            case PE_Command_HELLO:
                Log_print1(Diags_INFO, "PEB_exec: cmd=HELLO, sender=%d",
                        (IArg)msg->arg1);
                Compute_hello();
                break;
        }

        /* free the message back */
        MessageQ_free(mqMsg);

    } /* while (running) */

leave:
    Log_print1(Diags_INFO, "PEB_exec: <-- %d", (IArg)status);
    return(status);
}

/*
 *  ======== PEB_taskFxn ========
 */
Void PEB_taskFxn(UArg arg0, UArg arg1)
{
    Int status = 0;

    Log_print0(Diags_INFO, "PEB_taskFxn: -->");

    /* setup phase */
    status = PEB_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = PEB_exec();

leave:
    /* shutdown phase */
    PEB_destroy();

    Log_print1(Diags_INFO, "PEB_taskFxn: <-- %d", (IArg)status);
}

/*
 *  ======== PEB_setup ========
 */
Int PEB_setup(Void)
{
    Int                 status = 0;
    Error_Block         eb;
    Semaphore_Params    semParams;
    Registry_Result     result;
    MessageQ_Params     msgqParams;
    Char                cbuf[48];

    /* TODO enter gate */

    /* reference count the module usage */
    if (Mod_curInit >= 1) {
        goto leave;  /* already initialized */
    }

    /* initialize module state */
    Error_init(&eb);
    PEB_module.running = FALSE;
    PEB_module.ipcReady = NULL;
    PEB_module.queue = NULL;

    /* register with xdc.runtime to get a diags mask */
    result = Registry_addModule(&Registry_CURDESC, MODULE_NAME);

    if ((result != Registry_SUCCESS) && (result != Registry_ALREADY_ADDED)) {
        status = -1;
        goto leave;
    }

    /* enable some trace */
    Diags_setMask(MODULE_NAME"+F");
    Log_print0(Diags_INFO, "PEB_setup: -->");

    /* initialize the semaphore */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;

    PEB_module.ipcReady = Semaphore_create(0, &semParams, &eb);

    if (Error_check(&eb)) {
        status = -2;
        goto leave;
    }

    /* wait here until IPC is ready */
    PEB_taskWaiting = TRUE;

    if (!PEB_ipcReady) {
        Log_print0(Diags_INFO, "PEB_setup: waiting for IPC");
        Semaphore_pend(PEB_module.ipcReady, BIOS_WAIT_FOREVER);
    }
    PEB_taskWaiting = FALSE;
    Log_print0(Diags_INFO, "PEB_setup: IPC ready");

    /* compute the message queue name */
    System_sprintf(cbuf, PEB_QueNameFmt, MultiProc_self());

    /* create local message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    PEB_module.queue = MessageQ_create(cbuf, &msgqParams);

    if (PEB_module.queue == NULL) {
        Log_error0("PEB_setup: message queue create failed");
        goto leave;
    }

leave:
    /* success, increment reference count */
    if (status >= 0) {
        Mod_curInit++;
    }

    /* error handling */
    else {
        /* unregister diags mask */
        /* release resources */
    }

    /* TODO leave gate */
    Log_print0(Diags_INFO, "PEB_setup: <--");
    return(status);
}

/*
 *  ======== PEB_stop ========
 */
Void PEB_stop(Void)
{
    PEB_module.running = FALSE;
}

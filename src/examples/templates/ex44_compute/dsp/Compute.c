/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Compute.c ========
 */

/* this define must precede inclusion of any xdc header file */
#define Registry_CURDESC Compute__Desc
#define MODULE_NAME "Compute"

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/ipc/HeapBufMP.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* local header files */
#include "Compute.h"
#include "../shared/Compute_shared.h"
#include "../shared/SvrMsg.h"

#define CLSZ MultiProc_clusterSize

typedef struct {
    MessageQ_QueueId    cmd;
    MessageQ_QueueId    data;
} Compute_Queues;

/* module structure */
typedef struct {
    Bool                run;            /* main loop run flag               */
    Semaphore_Handle    ipcReady;       /* IPC synchronizing object         */
    UInt16              myHeapId;       /* this processor's clusterId       */
    HeapBufMP_Handle    heaps[CLSZ];    /* message heaps (local and remote) */
    MessageQ_Handle     cmdQue;         /* command queue (inbound messages) */
    MessageQ_Handle     dataQue;        /* data queue (inbound messages)    */
    Compute_Queues      peer[CLSZ];     /* message queue IDs (outbound)     */
} Compute_Module;

/* private functions */
Void Compute_data(Compute_Msg *msg);
Void Compute_hello(Void);

/* private data */
Registry_Desc           Registry_CURDESC;
static Int              Mod_curInit = 0;
static Compute_Module   Compute_module;
static volatile Bool    Compute_ipcReady = FALSE;
static volatile Bool    Compute_taskWaiting = FALSE;


/*
 *  ======== Compute_data ========
 */
Void Compute_data(Compute_Msg *msg)
{
    switch (msg->arg1) {
        case Compute_Data_HELLO:
            Log_print1(Diags_INFO, "Compute_data: message received, HELLO, "
                    "sender=%d", (IArg)msg->arg2);
            break;

        default:
            Log_error1("unknown command: arg1=%d", (IArg)msg->arg1);
            break;
    }
}

/*
 *  ======== Compute_destroy ========
 */
Void Compute_destroy(Void)
{
    Int     status = 0;
    UInt16  selfId, hostId, procId;
    UInt16  clusterBase, clusterId;

    Log_print0(Diags_INFO, "Compute_destroy: -->");


    /* reference count the module usage */
    if (--Mod_curInit > 0) {
        goto leave;  /* object still in use */
    }

    /* needed to iterate over members of the cluster */
    selfId = MultiProc_self();
    hostId = MultiProc_getId("HOST");
    clusterBase = MultiProc_getBaseIdOfCluster();

    /* close all remote queues */
    for (clusterId = 0; clusterId < MultiProc_clusterSize; clusterId++) {
        procId = clusterBase + clusterId;

        /* skip myself and the host (not a compute processor) */
        if ((procId == selfId) || (procId == hostId)) {
            continue;
        }

        MessageQ_close(&Compute_module.peer[clusterId].data);
        MessageQ_close(&Compute_module.peer[clusterId].cmd);
    }

    /* delete the local message queues */
    status = MessageQ_delete(&Compute_module.dataQue);

    if (status < 0) {
        Log_error1("Compute_destroy: data queue delete error=%d", (IArg)status);
        goto leave;
    }

    status = MessageQ_delete(&Compute_module.cmdQue);

    if (status < 0) {
        Log_error1("Compute_destroy: command queue delete error=%d",
                (IArg)status);
        goto leave;
    }

    /* close all remote message heaps */
    for (clusterId = 0; clusterId < MultiProc_clusterSize; clusterId++) {

        /* compute global MultiProc_ID */
        procId = clusterBase + clusterId;

        /* skip myself and the host (not a compute processor) */
        if ((procId == selfId) || (procId == hostId)) {
            continue;
        }

        /* finalize the remote heap */
        MessageQ_unregisterHeap(clusterId);
        HeapBufMP_close(&Compute_module.heaps[clusterId]);
    }

    /* finalize the local message heap */
    MessageQ_unregisterHeap(Compute_module.myHeapId);
    HeapBufMP_delete(&Compute_module.heaps[Compute_module.myHeapId]);

    /* delete the semaphore */
    Semaphore_delete(&Compute_module.ipcReady);

leave:
    /* disable log events */
    Log_print1(Diags_INFO, "Compute_delete: <-- status=%d", (IArg)status);
    Diags_setMask(MODULE_NAME"-FEX");

    /*
     * Note that there isn't a Registry_removeModule() yet:
     *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=315448
     *
     * ... but this is where we'd call it.
     */
}

/*
 *  ======== Compute_isReady ========
 */
Bool Compute_isReady(Void)
{
    return (Compute_module.run);
}

/*
 *  ======== Compute_exec ========
 */
Int Compute_exec(Void)
{
    Int                 status = 0;
    MessageQ_Msg        mqMsg;
    Compute_Msg        *msg;

    Log_print0(Diags_INFO, "Compute_exec: -->");
    Compute_module.run = TRUE;

    /* main loop */
    while (Compute_module.run) {

        /* wait for inbound message */
        Log_print0(Diags_INFO, "waiting for command");
        status = MessageQ_get(Compute_module.cmdQue, &mqMsg, MessageQ_FOREVER);

        if (status < 0) {
            Log_error1("Compute_exec: message get error=%d", (IArg)status);
            goto leave;
        }

        /* process the message */
        msg = (Compute_Msg *)mqMsg;
        Log_print1(Diags_INFO, "message received, cmd=%d", (IArg)msg->command);

        switch (msg->command) {
            case Compute_Cmd_PROCESS:
                Log_print1(Diags_INFO, "processed message from procId=%d",
                        (IArg)msg->arg1);
                break;

            case Compute_Cmd_DATA:
                Compute_data(msg);
                break;

            default:
                Log_error1("unknown command, cmd=%d", (IArg)msg->command);
                break;
        }

        /* return the message to its heap */
        MessageQ_free(mqMsg);

    } /* while (run) */

leave:
    Log_print1(Diags_INFO, "Compute_exec: <-- %d", (IArg)status);
    return(status);
}

/*
 *  ======== Compute_hello ========
 */
Void Compute_hello(Void)
{
    Int         status;
    UInt16      selfId, hostId, procId;
    UInt16      clusterBase, clusterId;

    MessageQ_QueueId    qid;
    MessageQ_Msg        mqMsg;
    Compute_Msg        *msg;


    /* needed to iterate over members of the cluster */
    selfId = MultiProc_self();
    hostId = MultiProc_getId("HOST");
    clusterBase = MultiProc_getBaseIdOfCluster();

    /* send a command to every other compute processor in the cluster */
    for (clusterId = 0; clusterId < MultiProc_clusterSize; clusterId++) {

        /* compute global MultiProc_ID */
        procId = clusterBase + clusterId;

        /* skip myself and the host (not a compute processor) */
        if ((procId == selfId) || (procId == hostId)) {
            continue;
        }

        /* allocate the message */
        mqMsg = MessageQ_alloc(Compute_module.myHeapId, sizeof(Compute_Msg));

        if (mqMsg == NULL) {
            Log_error0("Error in MessageQ_alloc");
            status = -1;
            goto leave;
        }

        /* fill in message payload */
        msg = (Compute_Msg *)mqMsg;
        msg->command = Compute_Cmd_DATA;
        msg->arg1 = Compute_Data_HELLO;
        msg->arg2 = selfId;
        qid = Compute_module.peer[clusterId].cmd;

        /* send message */
        status = MessageQ_put(qid, mqMsg);

        if (status < 0) {
            Log_error0("Error in MessageQ_put");
            goto leave;
        }
        Log_print2(Diags_INFO, "Comput_hello: message sent: procId=%d, "
                "qid=0x%08x\n", (IArg)procId, (IArg)qid);
    }
leave:
    return;
}

/*
 *  ======== Compute_ipcIsReady ========
 *  Signal the Compute task that IPC is ready.
 *
 *  This function is called from another execution context. Must be
 *  careful to ensure local resources have been created.
 */
Void Compute_ipcIsReady(Void)
{
    Compute_ipcReady = TRUE;

    /* semaphore might not yet exist, must check if task is waiting */
    if (Compute_taskWaiting) {
        Semaphore_post(Compute_module.ipcReady);
    }
}

/*
 *  ======== Compute_taskFxn ========
 */
Void Compute_taskFxn(UArg arg0, UArg arg1)
{
    Int status = 0;

    Log_print0(Diags_INFO, "Compute_taskFxn: -->");

    /* setup phase */
    status = Compute_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Compute_exec();

leave:
    /* shutdown phase */
    Compute_destroy();

    Log_print1(Diags_INFO, "Compute_taskFxn: <-- %d", (IArg)status);
}

/*
 *  ======== Compute_setup ========
 */
Int Compute_setup(Void)
{
    #define CBUF_SZ     48

    Int                 status = 0;
    Int                 i;
    Error_Block         eb;
    Semaphore_Params    semParams;
    Registry_Result     result;
    MessageQ_Params     msgqParams;
    Char                cbuf[CBUF_SZ];
    HeapBufMP_Params    heapParams;
    HeapBufMP_Handle    heap;
    UInt16              selfId, hostId, procId;
    UInt16              clusterBase, clusterId;
    MessageQ_QueueId    qid;


    /* reference count the module usage */
    if (Mod_curInit >= 1) {
        goto leave;  /* already initialized */
    }

    /* initialize module state */
    Error_init(&eb);
    Compute_module.run = FALSE;
    Compute_module.ipcReady = NULL;
    Compute_module.myHeapId = MultiProc_self() - MultiProc_getBaseIdOfCluster();
    Compute_module.cmdQue = NULL;
    Compute_module.dataQue = NULL;

    for (i = 0; i < CLSZ; i++) {
        Compute_module.heaps[i] = NULL;
        Compute_module.peer[i].cmd = MessageQ_INVALIDMESSAGEQ;
        Compute_module.peer[i].data = MessageQ_INVALIDMESSAGEQ;
    }

    /* register with xdc.runtime to get a diags mask */
    result = Registry_addModule(&Registry_CURDESC, MODULE_NAME);

    if ((result != Registry_SUCCESS) && (result != Registry_ALREADY_ADDED)) {
        status = -1;
        goto leave;
    }

    /* enable some trace */
    Diags_setMask(MODULE_NAME"+F");
    Log_print0(Diags_INFO, "Compute_setup: -->");

    /* initialize the semaphore */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;

    Compute_module.ipcReady = Semaphore_create(0, &semParams, &eb);

    if (Error_check(&eb)) {
        status = -2;
        goto leave;
    }

    /* wait here until IPC is ready */
    Compute_taskWaiting = TRUE;

    if (!Compute_ipcReady) {
        Log_print0(Diags_INFO, "Compute_setup: waiting for IPC");
        Semaphore_pend(Compute_module.ipcReady, BIOS_WAIT_FOREVER);
    }
    Compute_taskWaiting = FALSE;
    Log_print0(Diags_INFO, "Compute_setup: IPC ready");

    /* create a heap for outbound message pool (to cluster members only) */
    HeapBufMP_Params_init(&heapParams);
    System_snprintf(cbuf, CBUF_SZ, "COMPUTE_HEAP_%d", MultiProc_self());
    heapParams.name = cbuf;
    heapParams.regionId = 0;
    heapParams.blockSize = Compute_Msg_BLOCK;
    heapParams.numBlocks = MultiProc_clusterSize;

    heap = HeapBufMP_create(&heapParams);

    if (heap == NULL) {
        Log_error0("Compute_setup: heap create failed");
        status = -3;
        goto leave;
    }
    Compute_module.heaps[Compute_module.myHeapId] = heap;

    /* register heap with MessageQ */
    status = MessageQ_registerHeap(heap, Compute_module.myHeapId);

    if (status < 0) {
        Log_error0("Compute_setup: heap already exists");
        status = -4;
        goto leave;
    }

    /* needed to iterate over members of the cluster */
    selfId = MultiProc_self();
    hostId = MultiProc_getId("HOST");
    clusterBase = MultiProc_getBaseIdOfCluster();

    /* open remote message pools to recycle inbound messages */
    for (clusterId = 0; clusterId < MultiProc_clusterSize; clusterId++) {

        /* compute global MultiProc_ID */
        procId = clusterBase + clusterId;

        /* skip myself and the host (not a compute processor) */
        if ((procId == selfId) || (procId == hostId)) {
            continue;
        }

        System_snprintf(cbuf, CBUF_SZ, "COMPUTE_HEAP_%d", procId);

        do {
            status = HeapBufMP_open(cbuf, &heap);

            if (status == MessageQ_E_NOTFOUND) {
                Task_sleep(1);
            }
        } while(status == MessageQ_E_NOTFOUND);

        Compute_module.heaps[clusterId] = heap;
        Log_print1(Diags_INFO, "opened compute heap %d", (IArg)procId);

        /* register the remote message pool for message recycling */
        status = MessageQ_registerHeap(heap, clusterId);

        if (status < 0) {
            Log_error0("Compute_setup: heap already exists");
            status = -5;
            goto leave;
        }
    }

    /* create command queue (reserved queue index) */
    MessageQ_Params_init(&msgqParams);
    msgqParams.queueIndex = Compute_CMD_QUE;
    Compute_module.cmdQue = MessageQ_create(NULL, &msgqParams);

    if (Compute_module.cmdQue == NULL) {
        Log_error0("Compute_setup: command queue create failed");
        status = -6;
        goto leave;
    }

    /* create data queue (reserved queue index) */
    MessageQ_Params_init(&msgqParams);
    msgqParams.queueIndex = Compute_DATA_QUE;
    Compute_module.dataQue = MessageQ_create(NULL, &msgqParams);

    if (Compute_module.dataQue == NULL) {
        Log_error0("Compute_setup: data queue create failed");
        status = -7;
        goto leave;
    }

    /* open compute message queues (only for peers in the cluster) */
    for (clusterId = 0; clusterId < MultiProc_clusterSize; clusterId++) {
        procId = clusterBase + clusterId;

        /* skip myself and the host (not a compute processor) */
        if ((procId == selfId) || (procId == hostId)) {
            continue;
        }

        /* compute queueId for remote queues */
        qid = MessageQ_openQueueId(Compute_CMD_QUE, procId);
        Compute_module.peer[clusterId].cmd = qid;
        Log_print1(Diags_INFO, "opened command queue 0x%x", (IArg)qid);

        qid = MessageQ_openQueueId(Compute_DATA_QUE, procId);
        Compute_module.peer[clusterId].data = qid;
        Log_print1(Diags_INFO, "opened data queue 0x%x", (IArg)qid);
    }

leave:
    /* success, increment reference count */
    if (status >= 0) {
        Mod_curInit++;
    }

    /* error handling */
    else {
        /* unregister diags mask */
        /* release resources */
    }

    Log_print0(Diags_INFO, "Compute_setup: <--");
    return (status);
}

/*
 *  ======== Compute_stop ========
 */
Void Compute_stop(Void)
{
    Compute_module.run = FALSE;
}

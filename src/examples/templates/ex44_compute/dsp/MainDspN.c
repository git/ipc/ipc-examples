/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== MainDspN.c ========
 *  Platform: DRA7XX_bios_elf
 */

/* xdctools header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>             /* Program.global in cfg script */
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>

/* package header files */
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/SharedRegion.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

/* local header files */
#include "Compute.h"
#include "Control.h"
#include "PEB.h"

#define PRI_LOW         2
#define PRI_MED         3
#define PRI_HIGH        5


Void MainDspN_startupFirst(Void);

UInt8 MainDspN_configData[20] = {
    0x98, 0x07, 0xfc, 0x20, 0x3e,
    0xe7, 0x2e, 0x68, 0x57, 0xda,
    0x95, 0xec, 0xd6, 0x19, 0xfe,
    0xed, 0x55, 0xf0, 0x50, 0xe6
};

/*
 *  ======== main ========
 */
Int main(Int argc, Char* argv[])
{
    Error_Block eb;
    Task_Params taskParams;

//  { volatile Int spin=1; while (spin); }
    Log_print0(Diags_ENTRY, "main: -->");

    /* must initialize the error block before using it */
    Error_init(&eb);

    /* create the IPC task */
    Task_Params_init(&taskParams);
    taskParams.instance->name = "Ipc";
    taskParams.priority = PRI_LOW;
    taskParams.stackSize = 0x600;
    Task_create(Control_taskFxn, &taskParams, &eb);

    if (Error_check(&eb)) {
        System_abort("main: failed to create ipc task");
    }

    /* create the PEB task */
    Task_Params_init(&taskParams);
    taskParams.instance->name = "PEB";
    taskParams.priority = PRI_MED;
    taskParams.stackSize = 0x600;
    Task_create(PEB_taskFxn, &taskParams, &eb);

    if (Error_check(&eb)) {
        System_abort("main: failed to create peb task");
    }

    /* create the compute task */
    Task_Params_init(&taskParams);
    taskParams.instance->name = "Compute";
    taskParams.priority = PRI_HIGH;
    taskParams.stackSize = 0x1000;
    Task_create(Compute_taskFxn, &taskParams, &eb);

    if (Error_check(&eb)) {
        System_abort("main: failed to create compute task");
    }

    /* start scheduler, this never returns */
    BIOS_start();

    /* should never get here */
    Log_print0(Diags_EXIT, "main: <--");
    return (0);
}

/*
 *  ======== MainDspN_startupFirst ========
 */
Void MainDspN_startupFirst(Void)
{
    Int status;
    UInt16 baseId;
    SharedRegion_Entry *sre;


    /*  Extract the cluster baseId from the config data array
     *
     *  Before loading the program, the config data array was
     *  "patched" with the correct cluster baseId. Use that value
     *  now to set the cluster baseId in the MultiProc module.
     */
    baseId = *(UInt16 *)(MainDspN_configData + 2);

    /* run-time configuration of the cluster baseId */
    status = MultiProc_setBaseIdOfCluster(baseId);

    if (status < 0) {
        System_abort("MainDspN_startupFirst: failed to set baseId");
    }

    /*  Internal IPC cluster configuration
     *
     *  This function will perform run-time configuration of IPC
     *  internal data structures related to the cluster baseId.
     */
    status = Ipc_clusterConfig();

    if (status < 0) {
        System_abort("MainDspN_startupFirst: ipc cluster config failed");
    }

    /*  Define the shared region owner
     *
     *  Now that we have a valid cluster baseId, we can look up the
     *  procId by name and assign the shared region owner. This example
     *  uses only SR #0. Do this for each region you have configured.
     */
    sre = SharedRegion_getEntryPtr(0);
    sre->ownerProcId = MultiProc_getId(SR0_owner); /* DSP1 is owner */
    sre->isValid = TRUE;
}

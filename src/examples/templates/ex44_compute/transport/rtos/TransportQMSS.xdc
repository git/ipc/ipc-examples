/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== TransportQMSS.xdc ========
 */
package transport.rtos;

import xdc.runtime.Diags;
import xdc.runtime.Log;

/*!
 *  ======== TransportQMSS ========
 *  Sample implementation of an INetworkTransport interface
 */
module TransportQMSS inherits ti.sdo.ipc.interfaces.INetworkTransport
{

    /*!
     *  ======== Type ========
     *  Enumerate the hardware types supported by this transport
     */
    enum Type {
        Type_7,                 /*! optimized for small messages (command) */
        Type_11                 /*! optimized for large messages (data) */
    };

    /*!
     *  ======== L_type ========
     */
    config Log.Event L_type = {
        mask: Diags.INFO,
        msg: "L_type: type=%d"
    };

instance:

    /*!
     *  ======== type ========
     *  Hardware type binding
     */
    config Type type = TransportQMSS.Type_7;

    /*!
     *  ======== create ========
     *  Create a transport instance
     */
    create();

internal:

    /* instance object */
    struct Instance_State {
        Type    type;
    };

    /* module object */
    struct Module_State {
        UInt    serial;         /* packet serial number */
    }
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== TransportQMSS.c ========
 */
#include <xdc/std.h>
#include <xdc/runtime/Log.h>

#include "package/internal/TransportQMSS.xdc.h"


/*
 *  ======== TransportQMSS_Instance_init ========
 */
Void TransportQMSS_Instance_init(TransportQMSS_Object *obj,
        const TransportQMSS_Params *params)
{

    obj->type = params->type;
    Log_write1(TransportQMSS_L_type, (UArg)obj->type);

    switch (obj->type) {
        case TransportQMSS_Type_7:
            /* ... */
            break;

        case TransportQMSS_Type_11:
            /* ... */
            break;
    }

    return;
}

/*
 *  ======== TransportQMSS_bind ========
 */
Int TransportQMSS_bind(TransportQMSS_Object *obj, UInt32 queueId)
{
    Int status = 0;

    /* ... */

    return (status);
}

/*
 *  ======== TransportQMSS_unbind ========
 */
Int TransportQMSS_unbind(TransportQMSS_Object *obj, UInt32 queueId)
{
    Int status = 0;

    /* ... */

    return (status);
}

/*
 *  ======== TransportQMSS_put ========
 */
Bool TransportQMSS_put(TransportQMSS_Object *obj, Ptr msg)
{
    Bool status = TRUE;

    /* ... */

    return (status);
}

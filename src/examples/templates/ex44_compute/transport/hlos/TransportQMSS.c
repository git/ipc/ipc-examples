/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== TransportQMSS.c ========
 */

#include <assert.h>
#include <stdlib.h>

#include <ti/ipc/Std.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include "TransportQMSS.h"


/*
 *  ======== params structure evolution ========
 */
typedef struct {
    Int __version;
    /* ... */
} TransportQMSS_Params_Version1;

/*
 *  ======== TransportQMSS_Object ========
 *  The instance object definition
 */
typedef struct {
    INetworkTransport_Object base;      /* inheritance */
    Int status;                         /* if needed */
    /* ... */                           /* add additional members here */
} TransportQMSS_Object;

/*
 *  ======== instance function declarations ========
 */
Int TransportQMSS_bind(Void *handle, UInt32 queueId);
Int TransportQMSS_unbind(Void *handle, UInt32 queueId);
Bool TransportQMSS_put(Void *handle, Ptr msg);

/*
 *  ======== TransportQMSS_Fxns ========
 *  The instance function table
 */
INetworkTransport_Fxns TransportQMSS_Fxns = {
    TransportQMSS_bind,
    TransportQMSS_unbind,
    TransportQMSS_put
};

/*
 *  ======== TransportQMSS_Params_init__S ========
 */
Void TransportQMSS_Params_init__S(TransportQMSS_Params *params, Int version)
{
    TransportQMSS_Params_Version1 *params1;

    switch (version) {

        case TransportQMSS_Params_VERSION_1:
            params1 = (TransportQMSS_Params_Version1 *)params;
            params1->__version = TransportQMSS_Params_VERSION_1;
            /* ... */
            break;

        default:
            assert(FALSE);
            break;
    }
}

/*
 *  ======== TransportQMSS_create ========
 */
TransportQMSS_Handle TransportQMSS_create(TransportQMSS_Params *pp)
{
    TransportQMSS_Params_Version1 *pp1;
    TransportQMSS_Params params;
    TransportQMSS_Object *obj;

    /* initialize local params structure */
    TransportQMSS_Params_init(&params);

    /* initialize local params using caller's given params */
    switch (pp->__version) {

        case TransportQMSS_Params_VERSION_1:
            pp1 = (TransportQMSS_Params_Version1 *)pp;
            /* params.foobar = pp1->foobar; */
            break;
    }

    /* allocate the instance object */
    obj = malloc(sizeof(TransportQMSS_Object));

    /* initialize the inherited interface */
    obj->base.base.interfaceType = INetworkTransport_TypeId;
    obj->base.fxns = &TransportQMSS_Fxns;

    /* ... */

    return ((TransportQMSS_Handle)obj);
}

/*
 *  ======== TransportQMSS_delete ========
 */
Void TransportQMSS_delete(TransportQMSS_Handle *hp)
{
    TransportQMSS_Object *obj;

    obj = *(TransportQMSS_Object **)hp;

    /* ... */

    free(obj);
    *hp = NULL;
}

/*
 *  ======== TransportQMSS_upCast ========
 */
INetworkTransport_Handle TransportQMSS_upCast(TransportQMSS_Handle inst)
{
    return ((INetworkTransport_Handle)inst);
}

/*
 *  ======== TransportQMSS_downCast ========
 */
TransportQMSS_Handle TransportQMSS_downCast(INetworkTransport_Handle base)
{
    return ((TransportQMSS_Handle)base);
}

/*
 *  ======== TransportQMSS_bind ========
 */
Int TransportQMSS_bind(Void *handle, UInt32 queueId)
{
    TransportQMSS_Object *obj;
    Int status;

    obj = (TransportQMSS_Object *)handle;

    /* ... */

    status = 0;

    return (status);
}

/*
 *  ======== TransportQMSS_unbind ========
 */
Int TransportQMSS_unbind(Void *handle, UInt32 queueId)
{
    TransportQMSS_Object *obj;
    Int status;

    obj = (TransportQMSS_Object *)handle;

    /* ... */

    status = 0;

    return (status);
}

/*
 *  ======== TransportQMSS_put ========
 */
Bool TransportQMSS_put(Void *handle, Ptr msg)
{
    TransportQMSS_Object *obj;
    Bool status;

    obj = (TransportQMSS_Object *)handle;

    /* ... */

    status = TRUE;

    return (status);
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== TransportQMSS.h ========
 */

#include <ti/ipc/interfaces/INetworkTransport.h>


/*
 *  ======== TransportQMSS_Params ========
 *  Optional create parameters
 *
 *  The '__version' parameter is used internally. You may omit this if
 *  you wish. This parameter can be used to support binary compatibility
 *  of old call sites with new library implementations.
 */
typedef struct {
    Int __version;      /* used for tracking implementation evolution */

    /* ... */           /* add your specific create parameters here */
} TransportQMSS_Params;

/*
 *  ======== TransportQMSS_Params_VERSION_1 ========
 *  Date: 07 Jan 2015
 *
 *  Initial implementation of the params structure.
 */
#define TransportQMSS_Params_VERSION_1          1

/*
 *  ======== TransportQMSS_Params_VERSION ========
 *  Defines the current params structure version
 *
 *  Internal use only.
 */
#define TransportQMSS_Params_VERSION    TransportQMSS_Params_VERSION_1

/*
 *  ======== TransportQMSS_Params_init__S ========
 *  Actual function which initializes the params structure
 *
 *  This function is called indirectly from TransportQMSS_Params_init.
 *  It uses params version to support binary compatibility with old
 *  call sites.
 */
Void TransportQMSS_Params_init__S(TransportQMSS_Params *params, Int version);

/*
 *  ======== TransportQMSS_Params_init ========
 *  Initialize the parameter structure to its default values
 *
 *  Using a static inline method binds the parameter structure version
 *  at the calls site. This allows new library implementations to recognize
 *  old call sites and thus to dereference the parameter structure correctly.
 */
static inline Void TransportQMSS_Params_init(TransportQMSS_Params *params)
{
    if (params != NULL) {
        TransportQMSS_Params_init__S(params, TransportQMSS_Params_VERSION);
    }
}

/*
 *  ======== TransportQMSS_Handle ========
 *  Opaque handle to transport instance object
 */
typedef struct TransportQMSS_Object *TransportQMSS_Handle;

/*
 *  ======== TransportQMSS_create ========
 *  Create an instance of this transport
 *
 *  If your create function has required arguments, add them to the
 *  function signature before 'params'. The params are typically
 *  considered as optional. You can also add a return status code
 *  to your function signature.
 */
TransportQMSS_Handle TransportQMSS_create(TransportQMSS_Params *params);

/*
 *  ======== TransportQMSS_delete ========
 *  Finalize an instance object
 *
 *  If you delete function can fail, change the function signature to
 *  return a status code.
 */
Void TransportQMSS_delete(TransportQMSS_Handle *hp);

/*
 *  ======== TransportQMSS_upCast ========
 *  Instance converter, return a handle to the inherited interface
 */
INetworkTransport_Handle TransportQMSS_upCast(TransportQMSS_Handle inst);

/*
 *  ======== TransportQMSS_downCast ========
 *  Instance converter, return an opaque handle to the instance object
 *
 *  It is the caller's responsibility to ensure the underlying object
 *  is of the correct type.
 */
TransportQMSS_Handle TransportQMSS_downCast(INetworkTransport_Handle base);

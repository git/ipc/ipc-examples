/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== App.c ========
 */

/* Standard headers */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>

#include "ResMgr.h"
#include "../shared/Compute_shared.h"
#include "../shared/Control_shared.h"
#include "../shared/PE_Msg.h"
#include "../shared/PEB_shared.h"
#include "../transport/hlos/TransportQMSS.h"

/* App defines:  Must match on remote proc side: */
#define HEAPID              0u

#define TRANSPORT_QMSS_ID       1

typedef struct {
    UInt16              procId;
    MessageQ_QueueId    pebQue;
    MessageQ_QueueId    controlQue;
} App_DSP;

/* module structure */
typedef struct {
    Bool        ipcReady;
    Int         dspCount;       /* number of elements in dsp array */
    App_DSP    *dspAry;         /* array of dsp objects */

    MessageQ_Handle         queue;          /* inbound messages */
    TransportQMSS_Handle    transQMSS;
} App_Module;

/* private functions */
Void App_destroy(Void);
Int App_setup(Void);

/* private data */
static App_Module App_module;


/*
 *  ======== App_destroy ========
 */
Void App_destroy(Void)
{
    Int i;

    printf("App_destroy: -->\n");

    /* finalize the secondary transport */
    MessageQ_unregisterTransportId(TRANSPORT_QMSS_ID);
    TransportQMSS_delete(&App_module.transQMSS);

    /* close the message queues */
    for (i = 0; i < App_module.dspCount; i++) {
        MessageQ_close(&App_module.dspAry[i].pebQue);
        MessageQ_close(&App_module.dspAry[i].controlQue);
    }

    free(App_module.dspAry);
    App_module.dspAry = NULL;

    /* delete the local message queue */
    MessageQ_delete(&App_module.queue);

    /* finalize IPC */
    App_module.ipcReady = FALSE;
    Ipc_stop();

    printf("App_destroy: <--\n");
}

/*
 *  ======== App_exec ========
 */
Int App_exec(Void)
{
    Int status;
    Int i;
    MessageQ_Msg        mqMsg;
    MessageQ_QueueId    qid;
    PE_Msg             *msg;
    Compute_Msg        *compMsg;
    UInt16              procId;

    printf("App_exec: -->\n");

    /* send each PEB a single message (one-way) */
    for (i = 0; i < App_module.dspCount; i++) {

        qid = App_module.dspAry[i].pebQue;

        mqMsg = MessageQ_alloc(HEAPID, sizeof(PE_Msg));

        if (mqMsg == NULL) {
            printf("Error in MessageQ_alloc\n");
            status = -1;
            goto leave;
        }

        /* fill in message payload */
        msg = (PE_Msg *)mqMsg;
        msg->command = PE_Command_HELLO;
        msg->arg1 = MultiProc_self();
        msg->arg2 = 0;

        /* send message */
        status = MessageQ_put(qid, mqMsg);

        if (status < 0) {
            printf("Error in MessageQ_put\n");
            status = -2;
            goto leave;
        }
        printf("App_exec: message sent to PEB queue: qid=0x%08x\n", qid);
    }

    /* send each Compute CMD queue a single message (one-way) */
    for (i = 0; i < App_module.dspCount; i++) {

        /* open queue by queueId */
        procId = App_module.dspAry[i].procId;
        qid = MessageQ_openQueueId(Compute_CMD_QUE, procId);

        mqMsg = MessageQ_alloc(HEAPID, sizeof(Compute_Msg));

        if (mqMsg == NULL) {
            printf("Error in MessageQ_alloc\n");
            status = -1;
            goto leave;
        }

        /* fill in message payload */
        compMsg = (Compute_Msg *)mqMsg;
        compMsg->command = Compute_Cmd_PROCESS;
        compMsg->arg1 = MultiProc_self();
        compMsg->arg2 = 0;

        /* send message */
        status = MessageQ_put(qid, mqMsg);

        if (status < 0) {
            printf("Error in MessageQ_put\n");
            status = -2;
            goto leave;
        }
        printf("App_exec: message sent to Compute queue: qid=0x%08x\n", qid);

        /* close queue */
        MessageQ_close(&qid);
    }

leave:
    printf("App_exec: <-- status=%d\n", status);

    return (status);
}

/*
 *  ======== App_setup ========
 */
Int App_setup(Void)
{
    Int         status = 0;
    Int         i;
    UInt16      clSize;
    UInt16      clBase;
    UInt16      procId;
    Char        name[48];
    Bool        ready;

    MessageQ_QueueId            queueId;
    MessageQ_Msg                mqMsg;
    MessageQ_QueueId            qid;
    Control_Msg                *ctrlMsg;
    ITransport_Handle           transport;
    INetworkTransport_Handle    transNetwork;
    TransportQMSS_Params        transQmssParams;


    printf("App_setup: -->\n");

    /* initialize module state */
    App_module.ipcReady = FALSE;
    App_module.dspCount = 0;
    App_module.dspAry = NULL;
    App_module.queue = NULL;
    App_module.transQMSS = NULL;

    /* configure the transport factory */
    Ipc_transportConfig(&TransportRpmsg_Factory);

    /* initialize IPC */
    printf("App_setup: initalizing IPC\n");
    status = Ipc_start();

    if (status < 0) {
        printf("Error: failed to initialize IPC, error=%d\n", status);
        goto leave;
    }
    App_module.ipcReady = TRUE;
    printf("App_setup: IPC ready, status=%d\n", status);

    /* create an anonymous message queue (inbound) */
    App_module.queue = MessageQ_create(NULL, NULL);

    if (App_module.queue == NULL) {
        status = -2;
        printf("Error: message queue create failed\n");
        goto leave;
    }

    /* needed to enumerate processors in cluster */
    clSize = MultiProc_getNumProcsInCluster();
    clBase = MultiProc_getBaseIdOfCluster();

    /* print cluster members */
    printf("App_setup: cluster baseId=%d, cluster members:\n", clBase);

    for (i = 0, procId = clBase; i < clSize; i++, procId++) {
        printf("App_setup: %2d %s\n", procId, MultiProc_getName(procId));
    }

    /* allocate memory for dsp array (only for peers in the cluster) */
    App_module.dspCount = clSize - 1; /* exclude the HOST */
    App_module.dspAry = malloc(App_module.dspCount * sizeof(App_DSP));

    if (App_module.dspAry == NULL) {
        printf("Error: %s:%d\n", __FILE__, __LINE__);
        goto leave;
    }

    for (i = 0; i < App_module.dspCount; i++) {
        App_module.dspAry[i].procId = MultiProc_INVALIDID;
        App_module.dspAry[i].pebQue = MessageQ_INVALIDMESSAGEQ;
        App_module.dspAry[i].controlQue = MessageQ_INVALIDMESSAGEQ;
    }

    /* open the message queues on each DSP processor */
    for (i = 0, procId = clBase; i < App_module.dspCount; procId++) {

        if (procId == MultiProc_self()) {
            continue;
        }

        App_module.dspAry[i].procId = procId;

        /* open the bookkeeper message queue */
        sprintf(name, PEB_QueNameFmt, procId);

        do {
            printf("App_setup: MessageQ_open(%s)\n", name);
            status = MessageQ_open(name, &queueId);

            if (status == MessageQ_E_NOTFOUND) {
                sleep(1);
            }
        } while (status == MessageQ_E_NOTFOUND);

        if (status < 0) {
            printf("Error: MessageQ_open, name=%s error=%d\n", name, status);
            goto leave;
        }

        App_module.dspAry[i].pebQue = queueId;
        printf("App_setup: PEB queue: proc=%s, qid=0x%08x\n",
                MultiProc_getName(procId), queueId);

        /* open the control message queue */
        sprintf(name, Control_QueNameFmt, procId);

        do {
            printf("App_setup: MessageQ_open(%s)\n", name);
            status = MessageQ_open(name, &queueId);

            if (status == MessageQ_E_NOTFOUND) {
                sleep(1);
            }
        } while (status == MessageQ_E_NOTFOUND);

        if (status < 0) {
            printf("Error: MessageQ_open, name=%s error=%d\n", name, status);
            goto leave;
        }

        App_module.dspAry[i].controlQue = queueId;
        printf("App_setup: Control queue: proc=%s, qid=0x%08x\n",
                MultiProc_getName(procId), queueId);

        /* increment the dsp index here (not in the for loop above) */
        i++;
    }

    /* busy wait until resource manager is ready */
    do {
        ready = TRUE;

        if (!ResMgr_isReady()) {
            ready = FALSE;
        }

        if (!ready) {
            usleep(20000); /* 20 milliseconds */
        }

    } while (!ready);

    /* send each control task a start message */
    for (i = 0; i < App_module.dspCount; i++) {

        mqMsg = MessageQ_alloc(HEAPID, sizeof(Control_Msg));

        if (mqMsg == NULL) {
            printf("Error in MessageQ_alloc\n");
            status = -1;
            goto leave;
        }

        /* fill in message payload */
        ctrlMsg = (Control_Msg *)mqMsg;
        ctrlMsg->command = Control_Cmd_INITIALIZE;
        ctrlMsg->arg1 = MultiProc_self();
        ctrlMsg->arg2 = 0;

        /* set return address and send the message */
        MessageQ_setReplyQueue(App_module.queue, mqMsg);
        qid = App_module.dspAry[i].controlQue;

        status = MessageQ_put(qid, mqMsg);

        if (status < 0) {
            printf("Error in MessageQ_put\n");
            status = -2;
            goto leave;
        }
        printf("App_setup: start message sent: procId=%d qid=0x%08x\n",
                App_module.dspAry[i].procId, qid);

        /* wait for reply message */
        status = MessageQ_get(App_module.queue, &mqMsg, MessageQ_FOREVER);

        if (status < 0) {
            printf("Error in MessageQ_get\n");
            status = -3;
            goto leave;
        }

        /* TODO validate message content? */

        /* return the message to the pool */
        MessageQ_free(mqMsg);
    }

    /* create the qmss transport instance */
    TransportQMSS_Params_init(&transQmssParams);

    App_module.transQMSS = TransportQMSS_create(&transQmssParams);

    if (App_module.transQMSS == NULL) {
        status = -1;
        printf("Error: TransportQMSS create failed\n");
        goto leave;
    }
    printf("App_setup: TransportQMSS instance created\n");

    /* register qmss transport with MessageQ */
    transNetwork = TransportQMSS_upCast(App_module.transQMSS);
    transport = INetworkTransport_upCast(transNetwork);
    MessageQ_registerTransportId(TRANSPORT_QMSS_ID, transport);

leave:
    printf("App_setup: <-- status=%d\n", status);
    return (status);
}

/*
 *  ======== App_threadFxn ========
 */
Void *App_threadFxn(Void *arg)
{
    Int status = 0;

    printf("App_threadFxn: -->\n");

    /* setup phase */
    status = App_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = App_exec();

leave:
    /* shutdown phase */
    App_destroy();

    printf("App_threadFxn: <-- status=%d\n", status);
    return ((Void *)status);
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== MainHost.c ========
 */

/* standard headers */
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>

#include "App.h"
#include "ResMgr.h"


/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    Int         status;
    pthread_t   rmThr;          /* resource manager */
    Int         rmArg;
    Int         rmStatus;
    pthread_t   appThr;         /* application */
    Int         appArg;
    Int         appStatus;

    /* start the resource manager thread */
    status = pthread_create(&rmThr, NULL, &ResMgr_threadFxn, &rmArg);

    if (status < 0) {
        printf("Error: resource manager thread create failed\n");
        goto leave;
    }

    /* run the application in its own thread */
    status = pthread_create(&appThr, NULL, &App_threadFxn, &appArg);

    if (status < 0) {
        printf("Error: application thread create failed\n");
        goto leave;
    }

    /* wait for the application thread to shutdown */
    pthread_join(appThr, (void **)(&appStatus));
    printf("main: application thread as terminated\n");

    /* shutdown the resource manager thread */
    ResMgr_shutdown();
    pthread_join(rmThr, (void **)(&rmStatus));

    /* application status */
    status = ((appStatus < 0) || (rmStatus < 0)) ? -1 : 0;

leave:
    printf("main: appStatus=%d\n", appStatus);
    printf("main: rmStatus=%d\n", rmStatus);
    printf("main: <-- status=%d\n", status);
    return(status);
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== ResMgr.c ========
 */

/* Standard headers */
#include <stdio.h>
#include <stdlib.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/transports/TransportRpmsg.h>

#include "ResMgr.h"
#include "../shared/ResMgr_shared.h"

/* module structure */
typedef struct {
    Bool            run;                /* main loop run flag */
    Bool            ipcReady;
    MessageQ_Handle queue;              /* inbound messages */
} ResMgr_Module;

/* private functions */
Void ResMgr_destroy(Void);
Int ResMgr_setup(Void);

/* private data */
static ResMgr_Module ResMgr_module = {
    .run        = FALSE,
    .ipcReady   = FALSE,
    .queue      = NULL
};


/*
 *  ======== ResMgr_destroy ========
 */
Void ResMgr_destroy(Void)
{

    printf("ResMgr_destroy: -->\n");

    /* delete the message queue */
    MessageQ_delete(&ResMgr_module.queue);

    /* finalize IPC */
    ResMgr_module.ipcReady = FALSE;
    Ipc_stop();

    printf("ResMgr_destroy: <--\n");
}

/*
 *  ======== ResMgr_exec ========
 */
Int ResMgr_exec(Void)
{
    Int                 status;
    MessageQ_Msg        mqMsg;
    MessageQ_QueueId    qid;
    ResMgr_Msg         *msg;

    printf("ResMgr_exec: -->\n");
    ResMgr_module.run = TRUE;

    /* main loop */
    while (ResMgr_module.run) {

        printf("ResMgr_exec: waiting for message\n");

        /* wait for inbound message */
        status = MessageQ_get(ResMgr_module.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            ResMgr_module.run = FALSE;
            status = 0;
            goto leave;
        }
        else if (status < 0) {
            printf("Error: ResMgr_exec: message get error=%d\n", status);
            goto leave;
        }

        msg = (ResMgr_Msg *)mqMsg;
        printf("ResMgr_exec: message received, cmd=%d\n", msg->cmd);

        /* process the message */
        switch (msg->cmd) {

            case ResMgr_Cmd_REQUEST:
                printf("ResMgr_exec: cmd=REQUEST, sender=%d\n", msg->arg1);
                break;

            case ResMgr_Cmd_RELEASE:
                printf("ResMgr_exec: cmd=RELEASE, sender=%d\n", msg->arg1);
                break;

            default:
                printf("Error: ResMgr_exec: unknown command: cmd=%d", msg->cmd);
                break;
        }

        /* return the message to sender */
        qid = MessageQ_getReplyQueue(mqMsg);

        status = MessageQ_put(qid, mqMsg);

        if (status < 0) {
            printf("Error in MessageQ_put\n");
            status = -2;
            goto leave;
        }
        printf("ResMgr_exec: message sent: qid=0x%08x\n", qid);
    }

leave:
    printf("ResMgr_exec: <-- status=%d\n", status);

    return (status);
}

/*
 *  ======== ResMgr_isReady ========
 */
Bool ResMgr_isReady(Void)
{
    return (ResMgr_module.run);
}

/*
 *  ======== ResMgr_setup ========
 */
Int ResMgr_setup(Void)
{
    Int status = 0;

    MessageQ_Params msgqParams;

    printf("ResMgr_setup: -->\n");

    /* initialize module state */
    ResMgr_module.run = FALSE;
    ResMgr_module.ipcReady = FALSE;
    ResMgr_module.queue = NULL;

    /* configure the transport factory */
    Ipc_transportConfig(&TransportRpmsg_Factory);

    /* initialize IPC */
    printf("ResMgr_setup: initalizing IPC\n");
    status = Ipc_start();

    if (status < 0) {
        printf("Error: failed to initialize IPC, error=%d\n", status);
        goto leave;
    }
    ResMgr_module.ipcReady = TRUE;
    printf("ResMgr_setup: IPC ready, status=%d\n", status);

    /* create message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    msgqParams.queueIndex = ResMgr_QueIndex;
    ResMgr_module.queue = MessageQ_create(NULL, &msgqParams);

    if (ResMgr_module.queue == NULL) {
        status = -2;
        printf("Error: message queue create failed\n");
        goto leave;
    }

leave:
    printf("ResMgr_setup: <-- status=%d\n", status);
    return (status);
}

/*
 *  ======== ResMgr_shutdown ========
 */
Void ResMgr_shutdown(Void)
{

    /* unblock resource manager thread waiting in message queue */
    MessageQ_unblock(ResMgr_module.queue);
}

/*
 *  ======== ResMgr_threadFxn ========
 */
Void *ResMgr_threadFxn(Void *arg)
{
    Int status = 0;

    printf("ResMgr_threadFxn: -->\n");

    /* setup phase */
    status = ResMgr_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = ResMgr_exec();

leave:
    /* shutdown phase */
    ResMgr_destroy();

    printf("ResMgr_threadFxn: <-- status=%d\n", status);
    return ((Void *)status);
}

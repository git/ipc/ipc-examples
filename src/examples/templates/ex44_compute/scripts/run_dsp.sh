#!/bin/sh

# must have lad running
# lad_tci6638 -l log.txt -b ###

set -x

prog="compute_dspN_patched.xe66"

# load the dsp processors
mpmcl load dsp0 $prog
mpmcl load dsp1 $prog
mpmcl load dsp2 $prog
mpmcl load dsp3 $prog
mpmcl load dsp4 $prog
mpmcl load dsp5 $prog
mpmcl load dsp6 $prog
mpmcl load dsp7 $prog

# run the dsp processors
mpmcl run dsp0
mpmcl run dsp1
mpmcl run dsp2
mpmcl run dsp3
mpmcl run dsp4
mpmcl run dsp5
mpmcl run dsp6
mpmcl run dsp7

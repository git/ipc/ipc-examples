#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Error: must specify cluster baseId"
    exit -1
fi

baseId=$1

set -x
perl patchExec.pl $baseId compute_dspN.xe66 compute_dspN_patched.xe66

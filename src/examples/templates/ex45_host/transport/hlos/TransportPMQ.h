/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== TransportPMQ.h ========
 */

#include <ti/ipc/interfaces/INetworkTransport.h>

/*
 *  ======== TransportPMQ_Config ========
 */
typedef struct {
    UInt16 heapId;              /* heap used for message pool */
} TransportPMQ_Config;

/*
 *  ======== TransportPMQ_Handle ========
 *  Opaque handle to transport instance object
 */
typedef struct TransportPMQ_Module *TransportPMQ_Handle;

/*
 *  ======== TransportPMQ_setup ========
 *  Initialize the transport
 */
Int TransportPMQ_setup(Void);

/*
 *  ======== TransportPMQ_destroy ========
 *  Finalize the transport
 */
Void TransportPMQ_destroy(Void);

/*
 *  ======== TransportPMQ_handle ========
 *  Return the transport handle
 *
 *  This module does not support instance creation. When the module
 *  is initialized, a single implicit instance is created. This method
 *  returns the handle to this implicit instance.
 */
TransportPMQ_Handle TransportPMQ_handle(Void);

/*
 *  ======== TransportPMQ_upCast ========
 *  Instance converter, return a handle to the inherited interface
 */
INetworkTransport_Handle TransportPMQ_upCast(TransportPMQ_Handle inst);

/*
 *  ======== TransportPMQ_downCast ========
 *  Instance converter, return an opaque handle to the instance object
 *
 *  It is the caller's responsibility to ensure the underlying object
 *  is of the correct type.
 */
TransportPMQ_Handle TransportPMQ_downCast(INetworkTransport_Handle base);

/*
 *  ======== TransportPMQ_cfg ========
 *  The module configuration object
 */
extern TransportPMQ_Config TransportPMQ_cfg;

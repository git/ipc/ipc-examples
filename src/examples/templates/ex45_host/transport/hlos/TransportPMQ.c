/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== TransportPMQ.c ========
 */

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <mqueue.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

extern FILE *stderr;

#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/NameServer.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include "TransportPMQ.h"

#define MQ_NAME_FMT     "/TransportPMQ_mq_%d"
#define MQ_MAXQUE_SIZE  10
#define MQ_MAXMSG_SIZE  128
#define SEM_NAME_FMT    "/TransportPMQ_sem_%d"
#define NAME_LEN        32
#define NS_NAME         "TransportPMQ"
#define NS_KEY_LEN      16


/*
 *  ======== TransportPMQ_Module ========
 */
typedef struct {
    INetworkTransport_Object base;      /* inheritance */
    UInt16 heapId;                      /* heap for message pool */
    int pid;                            /* my process ID */
    NameServer_Handle ns;               /* transport name server instance */
    mqd_t mque;                         /* message queue */
    struct mq_attr mqattr;              /* message queue attributes */
    unsigned int priority;              /* message priority (fixed) */
    sem_t *sem;                         /* semaphore to serialize writers */
    pthread_t threadId;                 /* worker thread, the mailman */
    Bool threadStarted;                 /* mailman has been started */
    pthread_mutex_t gate;               /* module gate */
    int refCount;                       /* user reference count */
} TransportPMQ_Module;

/*
 *  ======== private functions ========
 */
void *TransportPMQ_mailman(void *arg);

/*
 *  ======== instance function declarations ========
 */
Int TransportPMQ_bind(Void *handle, UInt32 queueId);
Int TransportPMQ_unbind(Void *handle, UInt32 queueId);
Bool TransportPMQ_put(Void *handle, Ptr msg);

/*
 *  ======== TransportPMQ_Fxns ========
 *  The instance function table
 */
INetworkTransport_Fxns TransportPMQ_Fxns = {
    TransportPMQ_bind,
    TransportPMQ_unbind,
    TransportPMQ_put
};

/*
 *  ======== TransportPMQ_module ========
 *  The module state object
 */
static TransportPMQ_Module TransportPMQ_module = {
    .base.base.interfaceType = INetworkTransport_TypeId,
    .base.fxns = &TransportPMQ_Fxns,
    .heapId = 0xFFFF,
    .pid = -1,
    .ns = NULL,
    .mque = (mqd_t)-1,
    .priority = 1,
    .sem = NULL,
    .threadStarted = FALSE,
    // only _NP (non-portable) type available in CG tools which we're using
    .gate = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP,
    .refCount = 0
};

/*
 *  ======== TransportPMQ_setup ========
 */
Int TransportPMQ_setup(Void)
{
    int status = 0;
    NameServer_Params nsParams;
    char name[NAME_LEN];
    int flags;
    mode_t mode;
    struct mq_attr attr;


    /* enter critical section */
    pthread_mutex_lock(&TransportPMQ_module.gate);

    /* only first thread performs setup procedure */
    if (TransportPMQ_module.refCount >= 1) {
        TransportPMQ_module.refCount++;
        status = 1;
        goto done;
    }

    /* configure the module */
    TransportPMQ_module.heapId = TransportPMQ_cfg.heapId;

    /* identify myself */
    TransportPMQ_module.pid = (int)getpid();

    /* name server instance for mapping queues to owners */
    NameServer_Params_init(&nsParams);
    nsParams.maxValueLen = sizeof(int);
    nsParams.maxNameLen = NS_KEY_LEN;

    TransportPMQ_module.ns = NameServer_create("TransportPMQ", &nsParams);

    if (TransportPMQ_module.ns == NULL) {
        status = -1;
        goto done;
    }

    /* create the message queue for all inbound messages */
    snprintf(name, NAME_LEN - 1, MQ_NAME_FMT, TransportPMQ_module.pid);
    flags = O_RDWR |  O_CREAT | O_EXCL;
    mode = S_IWUSR | S_IWGRP | S_IWOTH;
    attr.mq_maxmsg = MQ_MAXQUE_SIZE;
    attr.mq_msgsize = MQ_MAXMSG_SIZE;

    TransportPMQ_module.mque = mq_open(name, flags, mode, &attr);

    if (TransportPMQ_module.mque == (mqd_t)-1) {
        perror("Error: TransportPMQ_create: mq_open failed");
        status = -1;
        goto done;
    }

    /* create a named semaphore to serialize message queue writers */
    snprintf(name, NAME_LEN - 1, SEM_NAME_FMT, TransportPMQ_module.pid);
    flags = O_CREAT | O_EXCL;
    mode = S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH;

    TransportPMQ_module.sem = sem_open(name, flags, mode, 1);

    if (TransportPMQ_module.sem == SEM_FAILED) {
        perror("Error: TransportPMQ_create: sem_open failed");
        TransportPMQ_module.sem = NULL;
        status = -1;
        goto done;
    }

    /* create a worker thread for delivering inbound messages */
    status = pthread_create(&TransportPMQ_module.threadId, NULL,
            &TransportPMQ_mailman, (void *)&TransportPMQ_module);

    if (status < 0) {
        perror("Error: TransportPMQ_create: pthread_create failed");
        status = -1;
        goto done;
    }
    TransportPMQ_module.threadStarted = TRUE;

    /* getting here means we have successfully started */
    TransportPMQ_module.refCount++;

done:
    if (status < 0) {
        TransportPMQ_destroy();
    }

    /* leave critical section */
    pthread_mutex_unlock(&TransportPMQ_module.gate);

    return (status);
}

/*
 *  ======== TransportPMQ_destroy ========
 */
Void TransportPMQ_destroy(Void)
{
    Int status = 0;
    char name[NAME_LEN];


    /* enter critical section */
    pthread_mutex_lock(&TransportPMQ_module.gate);

    /* only last thread performs stop procedure */
    if (TransportPMQ_module.refCount > 1) {
        TransportPMQ_module.refCount--;
        goto done;
    }

    if (TransportPMQ_module.threadStarted) {
        TransportPMQ_module.threadStarted = FALSE;

        status = sem_wait(TransportPMQ_module.sem);

        if (status == -1) {
            perror("Error: TransportPMQ_delete: sem_wait failed");
            /* report error, and keep going */
        }

        /* send empty message to unblock worker thread */
        mq_send(TransportPMQ_module.mque, name, 0,
                TransportPMQ_module.priority);

        /* wait for worker thread to exit */
        pthread_join(TransportPMQ_module.threadId, NULL);
    }

    if (TransportPMQ_module.sem != NULL) {
        snprintf(name, NAME_LEN - 1, SEM_NAME_FMT, TransportPMQ_module.pid);
        sem_unlink(name);
        TransportPMQ_module.sem = NULL;
    }

    if (TransportPMQ_module.mque != (mqd_t)-1) {
        snprintf(name, NAME_LEN - 1, MQ_NAME_FMT, TransportPMQ_module.pid);
        mq_unlink(name);
        TransportPMQ_module.mque = -1;
    }

    if (TransportPMQ_module.ns != NULL) {
        NameServer_delete(&TransportPMQ_module.ns);
    }

    /*  If the count is already zero, it probably means that the setup
     *  method failed and we are doing cleanup. If this method was called
     *  too many times, then its a don't care because all resources have
     *  already been released. Otherwise, decrement the count (should go
     *  to zero).
     */
    if (TransportPMQ_module.refCount > 0) {
        TransportPMQ_module.refCount--;
    }

done:
    /* leave critical section */
    pthread_mutex_unlock(&TransportPMQ_module.gate);
}

/*
 *  ======== TransportPMQ_handle ========
 */
TransportPMQ_Handle TransportPMQ_handle(Void)
{
    TransportPMQ_Handle handle;

    handle = (TransportPMQ_Handle)&TransportPMQ_module;
    return (handle);
}

/*
 *  ======== TransportPMQ_upCast ========
 */
INetworkTransport_Handle TransportPMQ_upCast(TransportPMQ_Handle inst)
{
    return ((INetworkTransport_Handle)inst);
}

/*
 *  ======== TransportPMQ_downCast ========
 */
TransportPMQ_Handle TransportPMQ_downCast(INetworkTransport_Handle base)
{
    return ((TransportPMQ_Handle)base);
}

/*
 *  ======== TransportPMQ_bind ========
 */
Int TransportPMQ_bind(Void *handle, UInt32 queueId)
{
    Int status = 0;
    TransportPMQ_Module *mod;
    Char key[NS_KEY_LEN];
    Ptr entry;

    mod = (TransportPMQ_Module *)handle;

    /* make the queueId into a string */
    snprintf(key, NS_KEY_LEN - 1, "0x%08x", queueId);

    /* bind the queueId to our process ID */
    entry = NameServer_add(mod->ns, key, &mod->pid, sizeof(int));

    if (entry == NULL) {
        fprintf(stderr, "Error: TransportPMQ_bind: "
                "NameServer_add failed, key=%s\n", key);
        status = -1;
        goto done;
    }

done:
    return (status);
}

/*
 *  ======== TransportPMQ_unbind ========
 */
Int TransportPMQ_unbind(Void *handle, UInt32 queueId)
{
    Int status = 0;
    TransportPMQ_Module *mod;
    Char key[NS_KEY_LEN];

    mod = (TransportPMQ_Module *)handle;

    /* make the queueId into a string */
    snprintf(key, NS_KEY_LEN - 1, "0x%08x", queueId);

    /* unbind the queueId from our process ID */
    status = NameServer_remove(mod->ns, key);

    if (status < 0) {
        fprintf(stderr, "Error: TransportPMQ_unbind: "
                "NameServer_remove failed, key=%s\n", key);
        status = -1;
        goto done;
    }

done:
    return (status);
}

/*
 *  ======== TransportPMQ_put ========
 */
Bool TransportPMQ_put(Void *handle, Ptr msg)
{
    Int status = 0;
    Bool rval = TRUE;
    TransportPMQ_Module *mod;
    MessageQ_Msg mqMsg;
    UInt32 dstQue;
    Char key[NS_KEY_LEN];
    char name[NAME_LEN];
    int dstPID;
    int flags;
    sem_t *sem;
    mqd_t mqueue;
    char *ptr;
    UInt32 size;
    size_t nbytes;

    mod = (TransportPMQ_Module *)handle;
    mqMsg = (MessageQ_Msg)msg;

    /* extract the destination address */
    dstQue = MessageQ_getDstQueue(mqMsg);

    /* make the destination queue into a key name */
    snprintf(key, NS_KEY_LEN - 1, "0x%08x", dstQue);
    size = sizeof(int);

    /* ask name server for the owner process */
    status = NameServer_getLocal(mod->ns, key, &dstPID, &size);

    if (status < 0) {
        fprintf(stderr, "Error: TransportPMQ_put: "
                "NameServer_getLocal failed, key=%s\n", key);
        rval = FALSE;
        goto done;
    }

    /* open the semaphore */
    snprintf(name, NAME_LEN - 1, SEM_NAME_FMT, dstPID);
    flags = O_RDWR;
    sem = sem_open(name, flags);

    if (sem == SEM_FAILED) {
        perror("Error: TransportPMQ_put: sem_open failed");
        rval = FALSE;
        goto done;
    }

    /* open message queue */
    snprintf(name, NAME_LEN - 1, MQ_NAME_FMT, dstPID);
    flags = O_WRONLY;
    mqueue = mq_open(name, flags);

    if (mqueue == (mqd_t)-1) {
        perror("Error: TransportPMQ_put: mq_open failed");
        rval = FALSE;
        goto done_sem;
    }

    /* acquire exclusive use of the message queue */
    status = sem_wait(sem);

    if (status == -1) {
        perror("Error: TransportPMQ_put: sem_wait failed");
        rval = FALSE;
        goto done_mq;
    }

    /* send only the message header */
    status = mq_send(mqueue, msg, sizeof(MessageQ_MsgHeader), mod->priority);

    if (status == -1) {
        perror("Error: TransportPMQ_put: mq_send header failed");
        rval = FALSE;
        goto done_post;
    }

    /* send the message payload */
    ptr = (char *)msg + sizeof(MessageQ_MsgHeader);
    size = MessageQ_getMsgSize(mqMsg) - sizeof(MessageQ_MsgHeader);

    do {
        nbytes = (size < MQ_MAXMSG_SIZE ? size : MQ_MAXMSG_SIZE);

        status = mq_send(mqueue, ptr, nbytes, mod->priority);

        if (status == -1) {
            perror("Error: TransportPMQ_put: mq_send payload failed");
            rval = FALSE;
            goto done_post;
        }

        ptr += nbytes;
        size -= nbytes;
    } while (size > 0);

    /* message successfully delivered, free local copy */
    MessageQ_free(mqMsg);

done_post:
    status = sem_post(sem);

done_mq:
    mq_close(mqueue);

done_sem:
    sem_close(sem);

done:
    return (rval);
}

/*
 *  ======== TransportPMQ_mailman ========
 */
void *TransportPMQ_mailman(void *arg)
{
    Int status = 0;
    Bool run = TRUE;
    TransportPMQ_Module *mod;
    MessageQ_Msg mqMsg;
    MessageQ_QueueId queueId;
    Int size;
    ssize_t nbytes;
    char *ptr;
    char buf[MQ_MAXMSG_SIZE];

    mod = (TransportPMQ_Module *)arg;

    while (run) {

        /* wait for new message, expecting only the header */
        nbytes = mq_receive(mod->mque, buf, MQ_MAXMSG_SIZE, NULL);

        if (nbytes == -1) {
            perror("Error: TransportPMQ_mailman: mq_receive header failed");
            status = -1;
            goto done;
        }

        if ((nbytes == 0) && (!mod->threadStarted)) {
            run = FALSE;
            goto done;
        }

        if (nbytes == 0) {
            fprintf(stderr, "Warning: TransportPMQ_mailman: "
                    "received empty message\n");
            continue;
        }

        /* allocate a local message to hold inbound data */
        size = MessageQ_getMsgSize((MessageQ_MsgHeader *)buf);

        mqMsg = MessageQ_alloc(mod->heapId, size);

        if (mqMsg == NULL) {
            fprintf(stderr, "Error: TransportPMQ_mailman: "
                    "MessageQ_alloc failed\n");
            status = -1;
            goto done;
        }

        /* copy header into message, then fill message with payload */
        memcpy(mqMsg, buf, sizeof(MessageQ_MsgHeader));

        ptr = (char *)mqMsg + sizeof(MessageQ_MsgHeader);
        size -= sizeof(MessageQ_MsgHeader);

        do {
            nbytes = mq_receive(mod->mque, ptr, MQ_MAXMSG_SIZE, NULL);

            if (nbytes == -1) {
                perror("Error: TransportPMQ_mailman: mq_receive payload "
                        "failed");
                status = -1;
                goto done;
            }

            ptr += nbytes;
            size -= nbytes;
        } while (size > 0);

        /* place message on destination queue */
        queueId = MessageQ_getDstQueue(mqMsg);
        MessageQ_put(queueId, mqMsg);
    }

done:
    return ((void *)status);
}

#
#  ======== readme.txt ========
#

Overview
=========================================================================
This example illustrates IPC communication between two host applications.
It also provides a transport implementation using POSIX message queue.

There are two programs: thing1 and thing2. The program thing2 creates
an IPC message queue and then waits on the queue for messages to arrive.
The program thing1 opens the IPC message queue created by thing2 and then
sends a few messages.

The last message sent by thing1 is a shutdown message. This causes thing2
to delete its message queue and exit. You can suppress the shutdown message
with a command linen option when invoking thing1. In this case thing2 will
not exit. You can invoke thing1 again and repeat the cycle.

The host folder contains the source files for the thing1 and thing2programs.
The transport folder contains the source files for the TransportPMQ module.
Both thing1 and thing2 enable this transport and register it with MessageQ.
When sending a message, the corresponding transport ID is used which will
cause MessageQ to invoke the TransportPMQ module when delivering the message.


Build Instructions
=========================================================================

 1. Create a work folder on your file system.

    mkdir work

 2. Extract this example into your work folder.

    cd work
    unzip ex45_host.zip

 3. Setup the build environment. Edit products.mak and set the install paths
    as defined by your physical development area. Each example has its own
    products.mak file; you may also create a products.mak file in the parent
    directory which will be used by all examples.

    edit ex45_compute/products.mak

    TOOLCHAIN_LONGNAME = arm-linux-gnueabihf
    TOOLCHAIN_INSTALL_DIR = <...>/linaro-arm-gnueabihf_4_7_2013_03
    TOOLCHAIN_PREFIX = $(TOOLCHAIN_INSTALL_DIR)/bin/$(TOOLCHAIN_LONGNAME)-

    IPC_INSTALL_DIR = <...>/ipc_m_mm_pp_bb

    Your DESTDIR must point to your IPC "install" location. In other words,
    the location you specified when you ran 'make install' within the IPC
    product.

    DESTDIR = <...>

    Note: To build this example, you must install IPC into DESTDIR.

 4. Build the example. This will build only debug versions of the executables.
    Edit the lower makefiles and uncomment the release goals to build both
    debug and release executables.

    cd ex45_host
    make

 5. Issue the following commands to clean your example.

    cd ex45_host
    make clean

 6. Copy the HOST executables, and the supporting scripts to your target
    file system.

    ex45_host/host/bin/debug/thing1
    ex45_host/host/bin/debug/thing2
    ex45_host/scripts/run_all.sh
    ex45_host/scripts/run_lad.sh


Running The Example
=========================================================================
A typical way to run this example is to run thing2 in the background and
then to run thing1 in the foreground. But this is not required. You can
run thing1 first if you prefer. It will spin in a loop trying to open the
message queue created by thing2. Once the queue is created by thing2, both
programs proceed as usual.

 1. Start LAD. You must start the LAD daemon before running any IPC
    program. If it is not already running, use the following command
    to start it.

    lad_tci6638 -l log.txt

    You can also use the helper script provided with the example.

    run_lad.sh

    Note: The LAD daemon is built with the IPC product and is available
    in the DESTDIR folder. Copy it to your target file system.

    DESTDIR/bin/lad_tci6638

 2. You must run both thing1 and thing2 concurrently. To do this from
    one shell, run the first program in the background. It does not matter
    which one is run first. Then run the second program in the foreground.

    thing2 &
    thing1

    You can also use the helper script provided with the example.

    run_all.sh

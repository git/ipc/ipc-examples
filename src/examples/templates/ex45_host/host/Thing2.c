/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Thing_2.c ========
 */

/* standard headers */
#include <stdio.h>

extern FILE *stderr;

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>

#include "../shared/Thing.h"
#include "../transport/hlos/TransportPMQ.h"

/* module structure */
typedef struct {
    MessageQ_Handle     inQue;                  /* inbound messages */
    TransportPMQ_Handle transPMQ;
} Thing2_Module;

/* private functions */
Int Thing2_setup(Void);
Int Thing2_exec(Void);
Void Thing2_destroy(Void);

/* private data */
static Thing2_Module Thing2_module;

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    Int status;


    printf("Thing2_main: -->\n");

    /* setup phase */
    status = Thing2_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Thing2_exec();

leave:
    /* shutdown phase */
    Thing2_destroy();

    printf("Thing2_main: <-- status=%d\n", status);
    return(status);
}

/*
 *  ======== Thing2_setup ========
 */
Int Thing2_setup(Void)
{
    Int status = 0;
    ITransport_Handle transport;
    INetworkTransport_Handle transNetwork;


    printf("Thing2_setup: -->\n");

    /* initialize module state */
    Thing2_module.inQue = NULL;

    /* configure the transport factory */
    Ipc_transportConfig(&TransportRpmsg_Factory);

    /* initialize IPC */
    printf("Thing2_setup: initalizing IPC\n");
    status = Ipc_start();

    if (status < 0) {
        fprintf(stderr, "Error: failed to initialize IPC, error=%d\n", status);
        goto leave;
    }
    printf("Thing2_setup: IPC ready, status=%d\n", status);

    /* setup the TransportPMQ module */
    status = TransportPMQ_setup();

    if (status < 0) {
        status = -1;
        fprintf(stderr, "Error: TransportPMQ setup failed\n");
        goto leave;
    }
    printf("Thing2_setup: TransportPMQ module initialized\n");
    Thing2_module.transPMQ = TransportPMQ_handle();

    /* register qmss transport with MessageQ */
    transNetwork = TransportPMQ_upCast(Thing2_module.transPMQ);
    transport = INetworkTransport_upCast(transNetwork);
    MessageQ_registerTransportId(TRANSPORT_PMQ_ID, transport);

    /* create the message queue (inbound) */
    Thing2_module.inQue = MessageQ_create(Thing2_QUEUE_NAME, NULL);

    if (Thing2_module.inQue == NULL) {
        status = -1;
        fprintf(stderr, "Error: message queue create failed\n");
        goto leave;
    }

leave:
    printf("Thing2_setup: <-- status=%d\n", status);
    return (status);
}

/*
 *  ======== Thing2_exec ========
 */
Int Thing2_exec(Void)
{
    Int status;
    MessageQ_Msg mqMsg;
    Thing_Msg *msg;
    Bool run = TRUE;


    printf("Thing2_exec: -->\n");

    /* main loop */
    while (run) {

        /* wait for inbound message */
        status = MessageQ_get(Thing2_module.inQue, &mqMsg, MessageQ_FOREVER);

        if (status < 0) {
            fprintf(stderr, "Error: message get failed, error=%d\n", status);
            goto leave;
        }

        /* process the message */
        msg = (Thing_Msg *)mqMsg;

        switch (msg->cmd) {
            case Thing_Cmd_HELLO:
                printf("Thing2_exec: message received, pid=%d\n", msg->arg1);
                break;

            case Thing_Cmd_GOODBYE:
                run = FALSE;
                printf("Thing2_exec: shutdown received, pid=%d\n", msg->arg1);
                break;

            default:
                fprintf(stderr, "Error: Thing2_exec: unknown command %d\n",
                        msg->cmd);
                break;
        }

        /* return the message to its heap */
        MessageQ_free(mqMsg);
    }

leave:
    printf("Thing2_exec: <-- status=%d\n", status);

    return (status);
}

/*
 *  ======== Thing2_destroy ========
 */
Void Thing2_destroy(Void)
{

    printf("Thing2_destroy: -->\n");

    /* finalize the secondary transport */
    MessageQ_unregisterTransportId(TRANSPORT_PMQ_ID);
    TransportPMQ_destroy();

    /* delete the inbound message queue */
    MessageQ_delete(&Thing2_module.inQue);

    /* finalize IPC */
    Ipc_stop();

    printf("Thing2_destroy: <--\n");
}

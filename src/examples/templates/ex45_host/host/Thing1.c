/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Thing1.c ========
 */

/* standard headers */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

extern FILE *stderr;

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/NameServer.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>

#include "../shared/Thing.h"
#include "../transport/hlos/TransportPMQ.h"

/* App defines:  Must match on remote proc side: */
#define HEAPID 0

/* module structure */
typedef struct {
    pid_t               pid;                    /* my process ID */
    MessageQ_QueueId    outQue;                 /* outbound messages */
    TransportPMQ_Handle transPMQ;
} Thing1_Module;

/* private functions */
Int Thing1_setup(Void);
Int Thing1_exec(Void);
Void Thing1_destroy(Void);
Int Thing1_parseArgs(Int argc, Char *argv[]);

#define Main_USAGE "\
Usage:\n\
    thing1 [options]\n\
\n\
Options:\n\
    h   : print this help message\n\
    n   : no shutdown message\n\
\n"

/* private data */
static Thing1_Module Thing1_module;
static Bool Thing1_n_opt = FALSE;               /* no shutdown message */

/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    Int status;


    printf("Thing1_main: -->\n");

    /* parse command line */
    status = Thing1_parseArgs(argc, argv);

    if (status < 0) {
        goto leave;
    }

    /* setup phase */
    status = Thing1_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Thing1_exec();

leave:
    /* shutdown phase */
    Thing1_destroy();

    printf("Thing1_main: <-- status=%d\n", status);
    return (status);
}

/*
 *  ======== Thing1_setup ========
 */
Int Thing1_setup(Void)
{
    Int status = 0;
    ITransport_Handle transport;
    INetworkTransport_Handle transNetwork;


    printf("Thing1_setup: -->\n");

    /* initialize module state */
    Thing1_module.pid = getpid();
    Thing1_module.outQue = MessageQ_INVALIDMESSAGEQ;
    Thing1_module.transPMQ = NULL;

    printf("Thing1_setup: pid=%d\n", Thing1_module.pid);

    /* configure the transport factory */
    Ipc_transportConfig(&TransportRpmsg_Factory);

    /* initialize IPC */
    printf("Thing1_setup: initalizing IPC\n");
    status = Ipc_start();

    if (status < 0) {
        fprintf(stderr, "Error: failed to initialize IPC, error=%d\n", status);
        goto leave;
    }
    printf("Thing1_setup: IPC ready, status=%d\n", status);

    /* setup the TransportPMQ module */
    status = TransportPMQ_setup();

    if (status < 0) {
        status = -1;
        fprintf(stderr, "Error: TransportPMQ setup failed\n");
        goto leave;
    }
    printf("Thing1_setup: TransportPMQ module initialized\n");
    Thing1_module.transPMQ = TransportPMQ_handle();

    /* register qmss transport with MessageQ */
    transNetwork = TransportPMQ_upCast(Thing1_module.transPMQ);
    transport = INetworkTransport_upCast(transNetwork);
    MessageQ_registerTransportId(TRANSPORT_PMQ_ID, transport);

    /* open the remote message queue */
    do {
        status = MessageQ_open(Thing2_QUEUE_NAME, &Thing1_module.outQue);

        if (status == MessageQ_E_NOTFOUND) {
            sleep(1);
        }
    } while (status == MessageQ_E_NOTFOUND);

    if (status < 0) {
        fprintf(stderr, "Error: Thing1_setup: MessageQ_open failed, "
                "error=%d\n", status);
        goto leave;
    }
    printf("Thing1_setup: opened message queue %s\n", Thing2_QUEUE_NAME);

leave:
    printf("Thing1_setup: <-- status=%d\n", status);
    return (status);
}

/*
 *  ======== Thing1_exec ========
 */
Int Thing1_exec(Void)
{
    Int status = 0;
    Int i;
    MessageQ_Msg mqMsg;
    Thing_Msg *msg;

    printf("Thing1_exec: -->\n");

    /* send messages to destination queue */
    for (i = 1; i <= 4; i++) {

        /* allocate a new messate */
        mqMsg = MessageQ_alloc(HEAPID, sizeof(Thing_Msg));

        if (mqMsg == NULL) {
            fprintf(stderr, "Error in MessageQ_alloc\n");
            status = -1;
            goto leave;
        }

        /* use posix message queue transport for message delivery */
        MessageQ_setTransportId(mqMsg, TRANSPORT_PMQ_ID);

        /* fill in message payload */
        msg = (Thing_Msg *)mqMsg;
        msg->cmd = Thing_Cmd_HELLO;
        msg->arg1 = Thing1_module.pid;
        msg->arg2 = 0;

        /* send message */
        status = MessageQ_put(Thing1_module.outQue, mqMsg);

        if (status < 0) {
            fprintf(stderr, "Error in MessageQ_put\n");
            status = -2;
            goto leave;
        }
        printf("Thing1_exec: message sent: %d\n", i);

        /* delay */
        sleep(1);
    }

    /* send shutdown message */
    if (!Thing1_n_opt) {
        mqMsg = MessageQ_alloc(HEAPID, sizeof(Thing_Msg));

        if (mqMsg == NULL) {
            fprintf(stderr, "Error in MessageQ_alloc\n");
            status = -1;
            goto leave;
        }

        /* use posix message queue transport for message delivery */
        MessageQ_setTransportId(mqMsg, TRANSPORT_PMQ_ID);

        /* fill in message payload */
        msg = (Thing_Msg *)mqMsg;
        msg->cmd = Thing_Cmd_GOODBYE;
        msg->arg1 = Thing1_module.pid;
        msg->arg2 = 0;

        /* send message */
        status = MessageQ_put(Thing1_module.outQue, mqMsg);

        if (status < 0) {
            fprintf(stderr, "Error in MessageQ_put\n");
            status = -2;
            goto leave;
        }
        printf("Thing1_exec: shutdown message sent\n");
    }

leave:
    printf("Thing1_exec: <-- status=%d\n", status);

    return (status);
}

/*
 *  ======== Thing1_destroy ========
 */
Void Thing1_destroy(Void)
{

    printf("Thing1_destroy: -->\n");

    /* finalize the secondary transport */
    MessageQ_unregisterTransportId(TRANSPORT_PMQ_ID);
    TransportPMQ_destroy();

    /* close the outbound message queue */
    MessageQ_close(&Thing1_module.outQue);

    /* finalize IPC */
    Ipc_stop();

    printf("Thing1_destroy: <--\n");
}

/*
 *  ======== Thing1_parseArgs ========
 */
Int Thing1_parseArgs(Int argc, Char *argv[])
{
    Int x, cp, opt, argNum;
    Int status = 0;


    /* parse the command line options */
    for (opt = 1; (opt < argc) && (argv[opt][0] == '-'); opt++) {
        for (x = 0, cp = 1; argv[opt][cp] != '\0'; cp++) {
            x = (x << 8) | (int)argv[opt][cp];
        }

        switch (x) {
            case 'h': /* -h */
                printf("%s", Main_USAGE);
                exit(0);
                break;

            case 'n': /* -n */
                Thing1_n_opt = TRUE;
                break;

            default:
                printf("Error: invalid option, %c\n", (Char)x);
                printf("%s", Main_USAGE);
                status = -1;
                goto leave;
        }
    }

    /* parse the command line arguments */
    for (argNum = 1; opt < argc; argNum++, opt++) {

        switch (argNum) {
            default:
                printf("Error: too many arguments\n");
                printf("%s", Main_USAGE);
                status = -1;
                goto leave;
        }
    }

leave:
    return(status);
}

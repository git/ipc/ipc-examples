/*
 * Copyright (c) 2012-2013, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== SvrHost.c ========
 *
 */

#define Registry_CURDESC SvrHost__Desc
#define MODULE_NAME "SvrHost"

#include <stdio.h>

#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* local header files */
#include "SvrHost.h"
#include "../shared/SvrMsg.h"

/* module structure */
typedef struct {
    MessageQ_Handle localMsgQue;
    MessageQ_QueueId svrQue;
} SvrHost_Module;


/* private data */
static SvrHost_Module   Module;


/*
 *  ======== SvrHost_destroy ========
 */
Int SvrHost_destroy(Void)
{
    Int status = 0;

    /* TODO enter gate */
    printf("--> SvrHost_destroy\n");

    /* delete the local message queue */
    status = MessageQ_delete(&Module.localMsgQue);

    if (status < 0) {
          printf("SvrHost_destroy: message queue delete error\n");
        goto leave;
    }

leave:
      printf("<-- SvrHost_delete: status =%d\n", status);

    return(status);
}

/*
 *  ======== SvrHost_run ========
 */
Int SvrHost_run(Void)
{
    Int                 status = 0;
    Bool                running = TRUE;
    SvrMsg_Msg *        msg;
    MessageQ_QueueId    queId;
    UInt16              procId;
    UInt16              numProcs;
    UInt16              count = 0;

    printf("--> SvrHost_run:\n");

    numProcs = MultiProc_getNumProcessors();

    while (running) {

        /* wait for inbound message */
        status = MessageQ_get(Module.localMsgQue, (MessageQ_Msg *)&msg,
            MessageQ_FOREVER);

    if (status < 0) {
            printf("SvrHost_run: MessageQ_get failed\n");
            goto leave;
        }

        /* process the message */
        queId = MessageQ_getReplyQueue(msg);
        procId = MessageQ_getProcId(queId);

        printf("SvrHost_run: message received, procId=%d, "
                "cmd=%d \n", procId, msg->cmd);

        switch (msg->cmd) {
            case SvrMsg_Cmd_START:
                msg->svrProcId = MultiProc_self();
                /* send message back */
                MessageQ_put(queId, (MessageQ_Msg)msg);
                break;

            case SvrMsg_Cmd_PROCESS:
                break;

            case SvrMsg_Cmd_STOP:
                count++;
                /*
                 * When stop commands have been recieved from all procs
                 * terminate server
                 */
                if (count == numProcs - 1) {
                    running = FALSE;
                }
                break;

            default:
                printf("SvrHost_run: unknown message\n");
                break;
        }
    } /* while (running) */

leave:
    printf("<-- SvrHost_run\n");
    return(status);
}


/*
 *  ======== SvrHost_setup ========
 */
Int SvrHost_setup(Void)
{
    Int                 status = 0;
    Int                 count;
    MessageQ_Params     msgqParams;
    UInt16              procId;
    Char                cbuf[48];

    /* TODO enter gate */
     printf("--> SvrHost_setup:\n");

    /* initialize module state */
    Module.localMsgQue = NULL;

    printf("Initialize Module state\n");

    count = MultiProc_getNumProcessors();

    /* compute the server queue name */
    procId = MultiProc_self();
    sprintf(cbuf,SvrMsg_SvrQueNameFmt, MultiProc_getName(procId));

    /* create local message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    Module.localMsgQue = MessageQ_create(cbuf, &msgqParams);

    if (Module.localMsgQue == NULL) {
        printf("SvrHost_setup: message queue create failed\n");
        goto leave;
    }

      printf("SvrHost_setup : slave is ready\n");

leave:

    /* TODO leave gate */
    printf("<-- SvrHost_setup:\n");
    return(status);
}

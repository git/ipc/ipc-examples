/*
 * Copyright (c) 2012-2013, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* NOTE: This example does not protect against concurrent access (not multithread safe). */

/*
 *  ======== AppHost.c ========
 *
 */

/* this define must precede inclusion of any xdc header file */
#define Registry_CURDESC AppHost__Desc
#define MODULE_NAME "AppHost"

#define HEAPID 0

#include <stdio.h>

#include <ti/ipc/Std.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* local header files */
#include "AppHost.h"
#include "../shared/SvrMsg.h"

/* module structure */
typedef struct {
    MessageQ_Handle     localMsgQue;    /* created locally */
    MessageQ_QueueId    svrQue[10];     /* opened remotely */
    SvrMsg_Msg *        msg;
} AppHost_Module;


/* private data */
static AppHost_Module   Module;


/*
 *  ======== AppHost_destroy ========
 */
Int AppHost_destroy(Void)
{
    Int32   status = 0;
    UInt16  numProcs;
    UInt16  procId;

    printf("--> AppHost_destroy:\n");

    /* TODO enter gate */

    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {

         if (procId == MultiProc_self()) {
             /* linux does not currently support intraprocessor communication */
         }
         else {
        /* fill and send message */
        Module.msg->cmd = SvrMsg_Cmd_STOP;
        MessageQ_put(Module.svrQue[procId], (MessageQ_Msg)Module.msg);
        }
    }

    /* close each remote processor's server queue */
    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {

         if (procId == MultiProc_self()) {
             /* linux does not currently support intraprocessor communication */
         }
         else {
        /* close remote message queue */
        status = MessageQ_close(&Module.svrQue[procId]);

        if (status < 0) {
            printf("AppHost_destroy: prodcId=%d, server que close =%d\n",
                    procId, status);
            goto leave;
        }
        }
    }

    /* delete the local message queue */
    status = MessageQ_delete(&Module.localMsgQue);

    if (status < 0) {
        printf("AppHost_destroy: MessageQ_delete failed, que delete error = %d \n", status);
        goto leave;
    }

leave:

    /* TODO leave gate */
    printf("<-- AppHost_destroy:\n");
    return(status);
}

/*
 *  ======== AppHost_run ========
 */
Int AppHost_run(Void)
{
    Int     status = 0;
    Int     count;
    UInt16  numProcs;
    UInt16  procId;


    printf("--> AppHost_run:\n");

    /* ping each remote server */
    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {

        if (procId == MultiProc_self()) {
             /* linux does not currently support intraprocessor communication */
        }
        else {

            for (count = 1; count <= SvrMsg_NUM_MSGS; count++) {

                printf("AppHost_run:ping procId=%d\n",procId);

                /* fill and send message */
                Module.msg->cmd = SvrMsg_Cmd_START;
                Module.msg->svrProcId = MultiProc_INVALIDID;
                status = MessageQ_put(Module.svrQue[procId],
                    (MessageQ_Msg)Module.msg);

                /* wait for return message */
                status = MessageQ_get(Module.localMsgQue,
                    (MessageQ_Msg *)&Module.msg,MessageQ_FOREVER);

                if (status < 0) {
                    printf("AppHost_run: message receive error=%d\n", status);
                    goto leave;
                }
            }
            printf("AppHost_run: ack received, procId=%d \n", procId);
        }
    }

leave:
    printf("<-- AppHost_run:\n");
    return(status);
}

/*
 *  ======== AppHost_setup ========
 */
Int AppHost_setup(Void)
{
    Int                 status = 0;
    MessageQ_Params     msgqParams;
    UInt16              numProcs;
    UInt16              procId;
    Char                cbuf[48];

    /* TODO enter gate */

    /* initialize module state */
    Module.localMsgQue = NULL;
    Module.msg = NULL;

    /* create local message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    Module.localMsgQue = MessageQ_create(Host_AppMsgQue, &msgqParams);


    if (Module.localMsgQue == NULL) {
        printf("Apphose_setup: message queue create failed\n");
        status = -1;
        goto leave;
    }

    /* open each remote processor's server queue */
    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {
        if (procId == MultiProc_self()) {
             /* linux does not currently support intraprocessor communication */
        }
        else {
             sprintf(cbuf, SvrMsg_SvrQueNameFmt, MultiProc_getName(procId));

            do {
                status = MessageQ_open(cbuf, &Module.svrQue[procId]);

                if (status == MessageQ_E_NOTFOUND) {
                    usleep(1000);
                }
            }

            while (status == MessageQ_E_NOTFOUND);

            if (status < 0) {
                printf("AppHost_setup: could not open slave message que\n");
            }

        printf("AppHost_setup: opended server queue, value of status is %d \n",
                status);
        }
    }

    /* allocate a message */
    Module.msg = (SvrMsg_Msg *)MessageQ_alloc(HEAPID, sizeof(SvrMsg_Msg));

    if (Module.msg == NULL) {
        printf("AppHost_setup: coudl not allocate message\n");
        status = -1;
        goto leave;
    }

    /* initialize message header */
    MessageQ_setReplyQueue(Module.localMsgQue, (MessageQ_Msg)Module.msg);

    printf("AppHost_setup: slave is ready\n");

leave:

    /* TODO leave gate */
    printf("<-- AppHost_setup:\n");
    return(status);
}

#
#  ======== readme.txt ========
#

ping - Send a message between all cores in the system

Overview
=========================================================================
This example is used to exercise every communication path between all
processors in the system (including local delivery on the current
processor). The example is also organized in a suitable manner to develop
an application with different compute units on each processor.

Each executable will create two tasks: 1) the server task, and 2) the
application task. The server task creates a message queue and then waits
on that queue for incoming messages. When a message is received, the
server task simply sends it back to the original sender.

The application task creates its own message queue and then opens every
server message queue in the system (including the server queue on the
local processor). The task sends a message to a server and waits for the
message to be returned. This is repeated for each server in the system
(including the local server).

% if (this.platform.match(/^OMAPL138_linux_elf$/)) {

IPC Build Instructions
=========================================================================

The IPC product provides a top level GNU makefile to allow users to easily
build the various component modules, tests, and sample applications for the
supported devices. GNU make version 3.81 or greater is required. The XDC
tools (provided with most SDKs and CCS distributions) includes a pre-compiled
version of GNU make 3.81 in $(XDC_INSTALL_DIR)/gmake.

1. To build the IPC BIOS-side libraries and test run:

 make -f ipc-bios.mak

2. To configure and build the IPC Linux-side user libraries and tests run:

 make -f ipc.linux.mak
 make

3. To install the Linux-side test, you can issue the followig command:

 make install prefix=$HOME/ipc_3_00

This will install the files on your local hosts filesystem in /home/<user>/
ipc_3_00 directory which could then be copies over to the target's
filesystem for execution.

If your target's filesystem is accessible from your local host, you can set
the prefix variable to point to your target's filesystem as follows:

 make install prefix=$HOME/targetFS/usr

% } else  {
% }

Build Instructions
=========================================================================

 1. Setup a development area. A typical setup might look like this. The
    Depot folder contains installed products. The work folder contains
    each of the examples (eg. hello, ping, etc.). Each example contains
    its own products.mak file which will include a parent products.mak
    file if it exists. The parent file must be created by you.

    testbench/
     |_ Depot/
% if (this.platform.match(/^DRA7XX_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ gnu_arm_vv_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_arp32_m_m_p/
     |   |_ ti_c6x_m_m_p/
     |   |_ ti_tms470_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^(AM572X|AM571X)_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ gnu_arm_vv_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_c6x_m_m_p/
     |   |_ ti_tms470_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^VAYUsim_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_arp32_m_m_p/
     |   |_ ti_c6x_m_m_p/
     |   |_ ti_tms470_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^OMAPL138_linux_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_c6x_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^TI814X_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_c6x_m_m_p/
     |   |_ ti_tms470_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^(C6472|C6678)_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_c6x_m_m_p/
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_arp32_m_m_p/
     |   |_ ti_c6x_m_m_p/
     |   |_ ti_tms470_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_arp32_m_m_p/
     |   |_ ti_c6x_m_m_p/
     |   |_ ti_tms470_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^(TCI6636|66AK2E|TCI6630|66AK2G)_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti_c6x_m_m_p/
     |   |_ xdctools_m_mm_pp_bb/
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
     |   |_ bios_m_mm_pp_bb/
     |   |_ gcc-linaro_vv_bb/
     |   |_ ipc_m_mm_pp_bb/
     |   |_ ti-cgt-arm_m_m_p/
% } else {
%     throw("Unsupported platform: " + platform);
% }
     |
     |_ work/
         |_ ping/
% if (this.platform.match(/^DRA7XX_bios_elf$/)) {
         |   |_ dsp1/
         |   |_ dsp2/
         |   |_ eve1/
         |   |_ eve2/
         |   |_ eve3/
         |   |_ eve4/
         |   |_ host/
         |   |_ ipu1/
         |   |_ ipu1-0/
         |   |_ ipu1-1/
         |   |_ ipu2/
         |   |_ shared/
% } else if (this.platform.match(/^AM572X_bios_elf$/)) {
         |   |_ dsp1/
         |   |_ dsp2/
         |   |_ host/
         |   |_ ipu1/
         |   |_ ipu1-0/
         |   |_ ipu1-1/
         |   |_ ipu2/
         |   |_ shared/
% } else if (this.platform.match(/^AM571X_bios_elf$/)) {
         |   |_ dsp1/
         |   |_ host/
         |   |_ ipu1/
         |   |_ ipu1-0/
         |   |_ ipu1-1/
         |   |_ ipu2/
         |   |_ shared/
% } else if (this.platform.match(/^VAYUsim_bios_elf$/)) {
         |   |_ dsp1/
         |   |_ dsp2/
         |   |_ eve1/
         |   |_ eve2/
         |   |_ eve3/
         |   |_ eve4/
         |   |_ ipu1/
         |   |_ ipu1-0/
         |   |_ ipu1-1/
         |   |_ shared/
% } else if (this.platform.match(/^TI814X_bios_elf$/)) {
         |   |_ dsp/
         |   |_ host/
         |   |_ shared/
         |   |_ video/
% } else if (this.platform.match(/^OMAPL138_linux_elf$/)) {
         |   |_ dsp/
         |   |_ host/
         |   |_ shared/
% } else if (this.platform.match(/^C6472_bios_elf$/)) {
         |   |_ core0/
         |   |_ core1/
         |   |_ core2/
         |   |_ core3/
         |   |_ core4/
         |   |_ core5/
% } else if (this.platform.match(/^C6678_bios_elf$/)) {
         |   |_ core0/
         |   |_ core1/
         |   |_ core2/
         |   |_ core3/
         |   |_ core4/
         |   |_ core5/
         |   |_ core6/
         |   |_ core7/
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
         |   |_ dsp/
         |   |_ eve/
         |   |_ host/
         |   |_ shared/
         |   |_ video/
% } else if (this.platform.match(/^TCI6636_bios_elf$/)) {
         |   |_ core0/
         |   |_ core1/
         |   |_ core2/
         |   |_ core3/
         |   |_ core4/
         |   |_ core5/
         |   |_ core6/
         |   |_ core7/
         |   |_ host/
         |   |_ shared/
% } else if (this.platform.match(/^(66AK2E|66AK2G)_bios_elf$/)) {
         |   |_ core0/
         |   |_ host/
         |   |_ shared/
% } else if (this.platform.match(/^TCI6630_bios_elf$/)) {
         |   |_ core0/
         |   |_ core1/
         |   |_ core2/
         |   |_ core3/
         |   |_ host/
         |   |_ shared/
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
         |   |_ dsp1/
         |   |_ dsp2/
         |   |_ eve1/
         |   |_ ipu1/
         |   |_ ipu1-0/
         |   |_ ipu1-1/
         |   |_ shared/
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
         |   |_ host/
         |   |_ r5f-0/
         |   |_ r5f-1/
         |   |_ shared/
% }
         |   |_ makefile
         |   |_ products.mak
         |   |_ readme.txt
         |
         |_ products.mak

    In the ping example, there is a top-level makefile which simply builds
    each of the lower directories. There is a directory for each processor
    named by the IPC processor name. The folder named 'shared' contains
    common files used by all executables, such as config.bld which defines
    the memory map.

 2. Unpack the ping.zip file into the work folder.

    cd work
    unzip <...>/ping.zip

 3. Setup the build environment. Edit products.mak and set the install paths
    as defined by your physical development area. Each example has its own
    products.mak file; you may also create a products.mak file in the parent
    directory which will be used by all examples.

    DEPOT = /testbench/Depot

    BIOS_INSTALL_DIR                = $(DEPOT)/bios_m_mm_pp_bb
    IPC_INSTALL_DIR                 = $(DEPOT)/ipc_m_mm_pp_bb
    XDC_INSTALL_DIR                 = $(DEPOT)/xdctools_m_mm_pp_bb
% if (this.platform.match(/^AM65XX_bios_elf$/)) {
    PDK_INSTALL_DIR                 = $(DEPOT)/pdk_m_mm_pp_bb
% }

% if (this.platform.match(/^DRA7XX_bios_elf$/)) {
    gnu.targets.arm.A15F            = $(DEPOT)/gnu_arm_vv_bb
    ti.targets.elf.C66              = $(DEPOT)/ti_c6x_m_m_p
    ti.targets.arm.elf.M4           = $(DEPOT)/ti_tms470_m_m_p
    ti.targets.arp32.elf.ARP32_far  = $(DEPOT)/ti_arp32_m_m_p
% } else if (this.platform.match(/^(AM572X|AM571X)_bios_elf$/)) {
    gnu.targets.arm.A15F            = $(DEPOT)/gnu_arm_vv_bb
    ti.targets.elf.C66              = $(DEPOT)/ti_c6x_m_m_p
    ti.targets.arm.elf.M4           = $(DEPOT)/ti_tms470_m_m_p
    ti.targets.arp32.elf.ARP32_far  = $(DEPOT)/ti_arp32_m_m_p
% } else if (this.platform.match(/^VAYUsim_bios_elf$/)) {
    ti.targets.elf.C66              = $(DEPOT)/ti_c6x_m_m_p
    ti.targets.arm.elf.M4           = $(DEPOT)/ti_tms470_m_m_p
    ti.targets.arp32.elf.ARP32_far  = $(DEPOT)/ti_arp32_m_m_p
% } else if (this.platform.match(/^TI814X_bios_elf$/)) {
    ti.targets.arm.elf.A8           = $(DEPOT)/ti_tms470_m_m_p
    ti.targets.elf.C674             = $(DEPOT)/ti_c6x_m_m_p
    ti.targets.arm.elf.M3           = $(DEPOT)/ti_tms470_m_m_p
% } else if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    TOOLCHAIN_LONGNAME              = arm-none-linux-gnueabi
    TOOLCHAIN_INSTALL_DIR           = $(DEPOT)/CodeSourcery/Sourcery_G++_Lite
    ti.targets.elf.C674             = $(DEPOT)/ti_c6x_m_m_p
% } else if (this.platform.match(/^C6472_bios_elf$/)) {
    ti.targets.elf.C64P             = $(DEPOT)/ti_c6x_m_m_p
% } else if (this.platform.match(/^C6678_bios_elf$/)) {
    ti.targets.elf.C66              = $(DEPOT)/ti_c6x_m_m_p
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
    ti.targets.arm.elf.A8           = $(DEPOT)/ti_tms470_m_m_p
    ti.targets.elf.C674             = $(DEPOT)/ti_c6x_m_m_p
    ti.targets.arm.elf.M3           = $(DEPOT)/ti_tms470_m_m_p
    ti.targets.arp32.elf.ARP32_far  = $(DEPOT)/ti_arp32_m_m_p
% } else if (this.platform.match(/^(TCI6636|66AK2E|TCI6630|66AK2G)_bios_elf$/)) {
    gnu.targets.arm.A15F            = $(DEPOT)/gnu_arm_vv_bb
    ti.targets.elf.C66              = $(DEPOT)/ti_c6x_m_m_p
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
    ti.targets.elf.C66              = $(DEPOT)/ti_c6x_m_m_p
    ti.targets.arm.elf.M4           = $(DEPOT)/ti_tms470_m_m_p
    ti.targets.arp32.elf.ARP32_far  = $(DEPOT)/ti_arp32_m_m_p
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
    gnu.targets.arm.A53F            = $(DEPOT)/gnu_linaro_vv_bb
    ti.targets.arm.elf.R5F           = $(DEPOT)/ti-cgt-arm_m_m_p
% }

 4. Build the example. This will build only debug versions of the executables.
    Edit the lower makefiles and uncomment the release goals to build both
    debug and release executables.

    cd ping
    make

    Look in the following folders for the generated files. All executables
    are copied to these folders for convenience; it makes it easier when
    loading them into CCS.

    install/ping/debug/
    install/ping/release/

 5. Optional. Use the install goal to copy the executables to a specific
    folder of your choice.

    cd ping
    make install EXEC_DIR=/<full pathname>/<platform>

    The executables are copied to the following folders.

    /<full pathname>/<platform>/ping/debug
    /<full pathname>/<platform>/ping/release


Build Configuration
=========================================================================

This example can be built for a subset of all available processors. For
example, you might want to build and run this example for the following
% if (this.platform.match(/^(VAYUsim|DRA7XX)_bios_elf$/)) {
processors: HOST, DSP1, DSP2, EVE1, EVE2. Use the following instructions to configure
% } else if (this.platform.match(/^(AM572X)_bios_elf$/)) {
processors: HOST, DSP1, DSP2, IPU1, IPU2. Use the following instructions to configure
% } else if (this.platform.match(/^(AM571X)_bios_elf$/)) {
processors: HOST, DSP1, IPU1. Use the following instructions to configure
% } else if (this.platform.match(/^TI814X_bios_elf$/) ||
%     (this.platform.match(/^C6A8149_bios_elf$/))) {
processors: DSP and HOST. Use the following instructions to configure
% } else if (this.platform.match(/^(C6472|C6678)_bios_elf$/)) {
processors: CORE0 and CORE1. Use the following instructions to configure
% } else if (this.platform.match(/^(TCI6636|TCI6630)_bios_elf$/)) {
processors: HOST, CORE0, CORE1. Use the following instructions to configure
% } else if (this.platform.match(/^(66AK2E|66AK2G)_bios_elf$/)) {
processors: HOST, CORE0. Use the following instructions to configure
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
processors: DSP1, DSP2, EVE1. Use the following instructions to configure
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
processors: HOST, R5F-0, R5F-1. Use the following instructions to configure
% }
this example for a subset of processors.

Note: you must always run the set of processors you configured and built.
% if (this.platform.match(/^(VAYUsim|DRA7XX|AM572X|AM571X)_bios_elf$/)) {
In this example, you must run the DSP1, EVE1, and EVE2 executables; you
% } else if (this.platform.match(/^(AM572X)_bios_elf$/)) {
In this example, you must run the HOST, DSP1, DSP2, IPU1, and IPU2 executables; you
% } else if (this.platform.match(/^(AM571X)_bios_elf$/)) {
In this example, you must run the HOST, DSP1 and  IPU1 executables; you
% } else if (this.platform.match(/^TI814X_bios_elf$/) ||
%     (this.platform.match(/^C6A8149_bios_elf$/))) {
In this example, you must run the DSP and HOST executables; you
% } else if (this.platform.match(/^(C6472|C6678)_bios_elf$/)) {
In this example, you must run the CORE0 and CORE1 executables; you
% } else if (this.platform.match(/^(TCI6636|TCI6630)_bios_elf$/)) {
In this example, you must run the HOST, CORE0, and CORE1 executables; you
% } else if (this.platform.match(/^(66AK2E|66AK2G)_bios_elf$/)) {
In this example, you must run the HOST and CORE0 executables; you
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
In this example, you must run the DSP1, DSP2, and EVE1 executables; you
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
In this example, you must run the HOST, R5F-0  and R5F-1 executables; you
% }
cannot run just one of them. Because the IPC has been configured for
ProcSync_ALL, all processors must participate in the call to Ipc_start().

 1. Specify the processor list in the top-level makefile.

    edit ping/makefile

    Edit the list of processors in the PROCLIST variable.

% if (this.platform.match(/^(DRA7XX|AM572X)_bios_elf$/)) {
# ipu1: implies smp mode
# ipu1-0 ipu1-1: implies non-smp mode
# eve1, eve2, eve3, eve4 not added by default. Can be added as needed.
# PROCLIST = dsp1 dsp2 eve1 eve2 eve3 eve4 ipu1 ipu2 host
PROCLIST = dsp1 dsp2 ipu1 ipu2 host
% } else if (this.platform.match(/^AM572X_bios_elf$/)) {
PROCLIST = dsp1 dsp2 ipu1 ipu2 host
% } else if (this.platform.match(/^AM571X_bios_elf$/)) {
PROCLIST = dsp1 ipu1 host
% } else if (this.platform.match(/^VAYUsim_bios_elf$/)) {
%     // No HOST on vayu CCS simulator
PROCLIST = dsp1 dsp2 eve1 eve2 eve3 eve4 ipu1
% } else if (this.platform.match(/^TI814X_bios_elf$/)) {
PROCLIST = dsp video-m3 host
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
PROCLIST = dsp eve video-m3 host
% } else if (this.platform.match(/^OMAPL138_linux_elf$/)) {
PROCLIST = dsp  host
% } else if (this.platform.match(/^C6472_bios_elf$/)) {
PROCLIST = core0 core1 core2 core3 core4 core5
% } else if (this.platform.match(/^C6678_bios_elf$/)) {
PROCLIST = core0 core1 core2 core3 core4 core5 core6 core7
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
# ipu1: implies smp mode
# ipu1-0 ipu1-1: implies non-smp mode
PROCLIST = dsp1 ipu1-0 eve1
% } else if (this.platform.match(/^TCI6636_bios_elf$/)) {
PROCLIST = host core0 core1 core2 core3 core4 core5 core6 core7
% } else if (this.platform.match(/^(66AK2E|66AK2G)_bios_elf$/)) {
PROCLIST = host core0
% } else if (this.platform.match(/^TCI6630_bios_elf$/)) {
PROCLIST = host core0 core1 core2 core3
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
# Currently only supporting one core or dual core in lockstep
# PROCLIST = host r5f-0 r5f-1
PROCLIST = host r5f-0
% } else {
%   throw new Error("unsupported platform: " + this.platform);
% }

 2. Clean and rebuild the example.

    cd ping
    make clean
    make

    Look in the following folders for the generated files.

    ping/install/ping/debug
    ping/install/ping/release


% if (this.platform.match(/^VAYUsim_bios_elf$/)) {
Running on CCS Vayu Simulator
=========================================================================
Use CCS with the Vayu simulator to run this example.

 1. Create a Vayu_FS launch configuration.

 2. Launch the Vayu_FS configuration.

 3. Load each executable onto its respective processor. Note that the CCS
    processor name may not match exactly with the executable name. Use
    the following mapping.

    server_dsp1.xe66    --> DSP_1
    server_dsp2.xe66    --> DSP_2
    server_eve1.xearp32 --> EVE_1
    server_eve2.xearp32 --> EVE_2
    server_eve3.xearp32 --> EVE_3
    server_eve4.xearp32 --> EVE_4
    server_ipu1.xem4    --> benelli_core_0
    server_ipu1.xem4    --> benelli_core_1

    Note that the same executable is loaded onto both Benelli processors.
    This executable is configured to run SMP/BIOS, so logically the Benelli
    is referred to as a single processor (IPU1) but due to the nature of
    the simulator, the executable must be loaded onto each core.

 4. Set a software breakpoint in each executable's "done" function. Use the
    CCS Disassembly window. For the Benelli processors, set the same
    breakpoint on both core_0 and core_1. Use the following mapping of
    CCS processor names to function names.

    DSP_1           --> MainDsp1_done
    DSP_2           --> MainDsp2_done
    EVE_1           --> MainEve1_done
    EVE_2           --> MainEve2_done
    EVE_3           --> MainEve3_done
    EVE_4           --> MainEve4_done
    benelli_core_0  --> MainIpu1_done
    benelli_core_1  --> MainIpu1_done

 5. Run the processors. Place all the processors into a group. Select the
    group and run them. This should release all the processors and run
    the example. Wait until each breakpoint is reached. You will still need
    to halt one of the Benelli processors.

 6. Use the CCS RTOS Object View (ROV) to inspect the LoggerBuf module for
    each processor. You should see log events from both the server task
    and the application task.
% } else if (this.platform.match(/^TI814X_bios_elf$/) ||
%     (this.platform.match(/^C6A8149_bios_elf$/))) {
Running on CCS
=========================================================================
Use CCS with an emulator hooked up to the CentEVE board to run this example.

 1. Create a launch configuration for the board + emulator combo.
% if (this.platform.match(/^TI814X_bios_elf$/)) {
    For the A8, set it up to use the initialization scripts from the pg1 or pg2 folder in
    <IPC_INSTALL_DIR>/packages/ti/sdo/ipc/examples/multicore/evmTI81XX, depending
    on your revision of silicon
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
    For the A8, set it up to use the initialization script DM814x_EVM_PG2_1_HDMI.gel
    from <IPC_INSTALL_DIR>/packages/ti/sdo/ipc/examples/multicore/evmC6A8149
    For the EVE, set it up to use script arp32.gel from the same directory.
% }
 2. Launch the configuration.

 3. Disable the CCS feature to automatically run to main on a program load. In
    CCS 5.1, you can do this by right-clicking on your newly created target
    configuration and select properties. The "Auto Run Options" are under the
    Debug tab, Generic Debugger Options. Uncheck the box next to "On a program
    load or restart". Do this for each core.

 4. Load each executable onto its respective processor. Note that the CCS
    processor name may not match exactly with the executable name. Use
    the following mapping.

    server_dsp.xe674    --> DSP
% if (this.platform.match(/^C6A8149_bios_elf$/)) {
    server_eve.xearp32F --> EVE
% }
    server_host.xea8f   --> A8
    server_video.xem3   --> VIDEO-M3 (aka M3 RTOS)
    server_video.xem3   --> VPSS-M3  (aka M3 ISS)

    Note that the same executable is loaded onto both M3 processors on the
    Ducati.
    This executable is configured to run SMP/BIOS, so logically the Ducati
    is referred to as a single processor (VIDEO-M3) but the executables
    are loaded twice to ensure the debugger is in the right state and has
    the debug symbols loaded.

 5. Clear bit T to 0 in the CPSR register of the Cortex A8 using CCS. The
    A8 comes up in Thumb mode, hence we need to toggle this bit to get it
    back to ARM mode.

 6. Run core 1 of the Ducati (VPSS-M3) first. Then run the other cores.

 7. Disconnect all cores and power cycle the board between runs of the example.
% if (this.platform.match(/^TI814X_bios_elf$/)) {

Known issue
=========================================================================
When the DSP and the M3's are configured to use the non-debug version of
BIOS and IPC, it has been seen that sometimes the example does not terminate
correctly. Hence both cores are currently configured to use the debug
version of the libraries.
% }
% } else if (this.platform.match(/^(DRA7XX|AM572X)_bios_elf$/)) {
Running with CCS on Virtio VPVayu simulator
=========================================================================
 1. Launch Virtio VPVayu

    Start > Innovator
    File > Open Project or File > C:/Virtio/Platforms/VPVayu/VPVayu.vsp

    Toolbar > vayu_general (Platform)

    If you are using the CortexA15_0 core (with HOST):
        Params > User Parameters
        ArmCortexA15_DebugEnable: true

        Params > VPVayu SDP > Vayu > CPU_SS
        InitValue_Core0: 0

    If you are *not* using the CortexA15_0 core (no host):
        Params > User Parameters
        ArmCortexA15_DebugEnable: false

        Params > VPVayu SDP > Vayu > CPU_SS
        InitValue_Core0: 1

    Build/Run > Start or Go (F5), this launches the Virtio Vayu simulator

    Wait for the following message in the Output Window
    CortexA15x2CT_FM: CADI Debug Server started

    Minimize the windows.
    * note: The Cockpit::VPVayu window will indicate '(Not Responding)'.

 2. Launch CCS

    Start > CCS_5_3_0
    Launch one of the following target configurations:
    + VPVayu_5.2
    + VPVayu_5.2_no_host

 3. Place HOST in non-secure mode. If you forget this, the SYS/BIOS tick
    will not run

    In Debug window, select CortexA15_0
    Scripts > Vayu Configuration > enterNonSecureMode

 4. Hide unused cores. This just removes clutter from the Debug window.
    Select any cores you will not be using, then select RMB > Hide Cores.

 5. Group remaining cores. This is more of a convenience than a necessity.

    In Debug window, select cores:
    + IPU1_core_0
    + IPU1_core_1
    RMB > Group cores

    In Debug window, select cores:
    + IPU2_core_0
    + IPU2_core_1
    RMB > Group cores

    In Debug window, select cores:
    + DSP_1
    + DSP_2
    + EVE_1
    + EVE_2
    + EVE_3
    + EVE_4
    RMB > Group cores

 6. Load each executable onto its respective processor. Note that the CCS
    processor name may not match exactly with the executable name. Use
    the following mapping.

    server_dsp1.xe66      --> DSP_1
    server_dsp2.xe66      --> DSP_2
    server_eve1.xearp32f  --> EVE_1
    server_eve2.xearp32f  --> EVE_2
    server_eve3.xearp32f  --> EVE_3
    server_eve4.xearp32f  --> EVE_4
    server_ipu1.xem4      --> IPU1_core_0
    server_ipu2.xem4      --> IPU2_core_0
    server_host.xa15fg    --> CortexA15_0

    When loading the executable onto IPU1 or IPU2, you only need to load
    core_0, not both cores. After loading core_0, select core_1 and just
    load symbols, then restart the core to update the PC register.

 7. Set a software breakpoint in each executable's "done" function. Use the
    CCS Disassembly window. For the Benelli processors, you will need to set
    the same breakpoint on both cores. Use the following mapping of CCS
    processor names to function names.

    DSP_1        --> MainDsp1_done
    DSP_2        --> MainDsp2_done
    EVE_1        --> MainEve1_done
    EVE_2        --> MainEve2_done
    EVE_3        --> MainEve3_done
    EVE_4        --> MainEve4_done
    IPU1_core_0  --> MainIpu1_done
    IPU1_core_1  --> MainIpu1_done
    IPU2_core_0  --> MainIpu2_done
    IPU2_core_1  --> MainIpu2_done
    CortexA15_0  --> MainHost_done

 8. Run the processors. I recommend running CortexA15_0 first, then other
    processors. As each processor hits the "done" breakpoint, make a note
    and let the processor continue. Do this for each processor. If the
    CortexA15_0 status shows 'Suspended - Cross-triggering', then simply
    run it again.

    Select CortexA15_0 > Run > Resume (F8)
    Select Group3 > Run > Resume (F8)
    Select Group2 (Synchronous) > Run > Resume (F8)
    Select Group1 (Synchronous) > Run > Resume (F8)

 9. Inspecting log events. Use the CCS RTOS Object View (ROV) to inspect
    the LoggerBuf module for each processor. You should see log events from
    both the server task and the application task.

    Select CortexA15_0 > Run > Suspend
    Select Group3 > Run > Suspend
    Select Group2 > Run > Suspend
    Select Group1 > Run > Suspend

    Select CortexA15_0
    Open ROV window
    Select LoggerBuf module

    There should be several log events in window. Repeat this for each
    processor. The number of events will depend on how many processors
    are configured into your configuration.
% } else if (this.platform.match(/^OMAPL138_linux_elf$/)) {
Run Instructions
=========================================================================

1. Edit the variable FIRMWARE in the ./slaveloader script to the name of your dsp firmware:

    FIRMWARE=<name_of_firmware>

This script is used to load and unload the dsp firmware as well as load the rpmsg socket driver.

2. Launch a serial terminal from your console (i.e. minicom).

    minicom -w -D /dev/ttyUSBx

If you have not done so you wil press enter to stop autoboot to configure the bootargs. Once this is done type "print" to see the list of bootargs and use the command editenv <NAME_OF_BOOT_ARG> to make changes. You will want to configure the proper IP address, point to the correct kernel image and filesystem you will use. Once the bootargs are configured type "saveenv" and "boot" to save the args and boot the device.

3. Now we will login to the device. The login is "root" with no password as shown below.

    omapl138-lcdk login:root
    root@omapl138-lcdk:~#

(Optional): SSH into a another shell by running ifconfig on the target to find the IP address and bring up a different terminal and run: telnet <IP_address>. This can be useful for debugging if necessary.


4. Run the following commands to execute the application.

   # /usr/bin/lad_omapl138 log

   # ./slaveloader.sh load

   # ./app_host

The first of these commands starts the NameServer daemon.

The second script allows you to load/unload the dsp firmware (to unload replace load with unload) and rpmsg socket driver.

The third command executes the host side code.

5. To debug you can the DSP side you can display the trace by:

   # cat /debug/remoteproc/remoteproct0/trace0

or to view the LAD log by:

vi /tmp/LAD/log

5. When complete you can unload the firmware and remove the rpmsg socket by:

   # ./slaveloader.sh unload


** NOTE: to restart the application, the user must power cycle the device. **
% }

#
#  Copyright (c) 2012-2018, Texas Instruments Incorporated
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  *  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#  *  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#  *  Neither the name of Texas Instruments Incorporated nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#
#  ======== makefile ========
#

# edit PROCLIST list to control how many executables to build
% if (this.platform.match(/^DRA7XX_bios_elf$/)) {
# ipu1: implies smp mode
# ipu1-0 ipu1-1: implies non-smp mode
# eve1, eve2, eve3, eve4 not added by default. Can be added as needed.
# PROCLIST = dsp1 dsp2 eve1 eve2 eve3 eve4 ipu1 ipu2 host
PROCLIST = dsp1 dsp2 ipu1 ipu2 host
% } else if (this.platform.match(/^AM572X_bios_elf$/)) {
PROCLIST = dsp1 dsp2 ipu1 ipu2 host
% } else if (this.platform.match(/^AM571X_bios_elf$/)) {
PROCLIST = dsp1 ipu1 host
% } else if (this.platform.match(/^VAYUsim_bios_elf$/)) {
%     // No HOST on vayu CCS simulator
PROCLIST = dsp1 dsp2 eve1 eve2 eve3 eve4 ipu1
% } else if (this.platform.match(/^TI814X_bios_elf$/)) {
PROCLIST = dsp video-m3 host
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
PROCLIST = dsp eve video-m3 host
% } else if (this.platform.match(/^OMAPL138_linux_elf$/)) {
PROCLIST = dsp  host
% } else if (this.platform.match(/^C6472_bios_elf$/)) {
PROCLIST = core0 core1 core2 core3 core4 core5
% } else if (this.platform.match(/^C6678_bios_elf$/)) {
PROCLIST = core0 core1 core2 core3 core4 core5 core6 core7
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
# ipu1: implies smp mode
# ipu1-0 ipu1-1: implies non-smp mode
PROCLIST = dsp1 ipu1-0 eve1
% } else if (this.platform.match(/^TCI6636_bios_elf$/)) {
PROCLIST = host core0 core1 core2 core3 core4 core5 core6 core7
% } else if (this.platform.match(/^(66AK2E|66AK2G)_bios_elf$/)) {
PROCLIST = host core0
% } else if (this.platform.match(/^TCI6630_bios_elf$/)) {
PROCLIST = host core0 core1 core2 core3
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
# Currently only supporting one core or dual core in lockstep
# PROCLIST = host r5f-0 r5f-1
PROCLIST = host r5f-0 r5f-1
% } else {
%   throw new Error("unsupported platform: " + this.platform);
% }

EXBASE = .
include $(EXBASE)/products.mak
.PHONY: $(PROCLIST)

.PHONY: install

all: $(PROCLIST)

$(PROCLIST):
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(MAKE) -C $@ PROCLIST="$(PROCLIST)"

help:
	@$(ECHO) "make                                  # build executables"
	@$(ECHO) "make clean                            # clean everything"
	@$(ECHO) "make install EXEC_DIR=/.../testbench  # install folder"

# setup install goal
EXEC_DIR=$(CURDIR)/install
override EXEC_DIR:=$(EXEC_DIR)/ex11_ping

install: $(PROCLIST) $(addsuffix _install,$(PROCLIST))
$(addsuffix _install,$(PROCLIST)):
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	@$(MKDIR) $(EXEC_DIR)/debug
	@$(MKDIR) $(EXEC_DIR)/release
	$(MAKE) -C $(subst _install,,$@) EXEC_DIR=$(EXEC_DIR) install

install_rov: $(PROCLIST) $(addsuffix _install_rov,$(PROCLIST))
$(addsuffix _install_rov,$(PROCLIST)):
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	@$(MKDIR) $(EXEC_DIR)/debug
	@$(MKDIR) $(EXEC_DIR)/release
	$(MAKE) -C $(subst _install_rov,,$@) EXEC_DIR=$(EXEC_DIR) install_rov

clean: $(addsuffix _clean,$(PROCLIST))
	$(RMDIR) install

$(addsuffix _clean,$(PROCLIST)):
	$(MAKE) -C $(subst _clean,,$@) clean


#  ======== standard macros ========
ifneq (,$(wildcard $(XDC_INSTALL_DIR)/xdc.exe))
    # use these on Windows
    CP      = $(XDC_INSTALL_DIR)/bin/cp
    ECHO    = $(XDC_INSTALL_DIR)/bin/echo
    INSTALL = $(XDC_INSTALL_DIR)/bin/cp
    MKDIR   = $(XDC_INSTALL_DIR)/bin/mkdir -p
    RM      = $(XDC_INSTALL_DIR)/bin/rm -f
    RMDIR   = $(XDC_INSTALL_DIR)/bin/rm -rf
else
    # use these on Linux
    CP      = cp
    ECHO    = echo
    INSTALL = install
    MKDIR   = mkdir -p
    RM      = rm -f
    RMDIR   = rm -rf
endif

/*
 * Copyright (c) 2017-2018 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
%%{
    var platform = this.arguments[0];
    var core = this.arguments[1];
    var suffix = this.arguments[2];
    var target = this.arguments[3];
    var procName = core.toUpperCase();
    var lcCore = core.toLowerCase();
%%}

/*
 *  ======== `core`.cfg ========
 *  Platform: `platform`
 *  Target: `target`
 */

/* root of the configuration object model */
var Program = xdc.useModule('xdc.cfg.Program');

/* application uses the following modules and packages */
xdc.useModule('xdc.runtime.Assert');
xdc.useModule('xdc.runtime.Diags');
xdc.useModule('xdc.runtime.Error');
xdc.useModule('xdc.runtime.Log');
xdc.useModule('xdc.runtime.Registry');

xdc.useModule('ti.sysbios.gates.GateHwi');
xdc.useModule('ti.sysbios.knl.Semaphore');
xdc.useModule('ti.sysbios.knl.Task');

/*
 *  ======== IPC Configuration ========
 */
xdc.useModule('ti.ipc.ipcmgr.IpcMgr');

Program.global.procName = "`procName`";
var ipc_cfg = xdc.loadCapsule("../shared/ipc.cfg.xs");

var BIOS        = xdc.useModule('ti.sysbios.BIOS');
BIOS.addUserStartupFunction('&IpcMgr_ipcStartup');

/*
 *  ======== SYS/BIOS Configuration ========
 */
if (Program.build.profile == "debug") {
    BIOS.libType = BIOS.LibType_Debug;
} else {
    BIOS.libType = BIOS.LibType_Custom;
}
BIOS.smpEnabled = false;


var Core = xdc.useModule('ti.sysbios.family.arm.v7r.keystone3.Core');

/* no rts heap */
Program.argSize = 100;  /* minimum size */
Program.stack = 0x1000;

var Task = xdc.useModule('ti.sysbios.knl.Task');
Task.common$.namedInstance = true;

/* default memory heap */
var Memory = xdc.useModule('xdc.runtime.Memory');
var HeapMem = xdc.useModule('ti.sysbios.heaps.HeapMem');
var heapMemParams = new HeapMem.Params();
heapMemParams.size = 0x8000;
Memory.defaultHeapInstance = HeapMem.create(heapMemParams);

/* create a heap for MessageQ messages */
var HeapBuf = xdc.useModule('ti.sysbios.heaps.HeapBuf');
var params = new HeapBuf.Params;
params.align = 8;
params.blockSize = 512;
params.numBlocks = 256;
var msgHeap = HeapBuf.create(params);

var MessageQ  = xdc.useModule('ti.sdo.ipc.MessageQ');
MessageQ.registerHeapMeta(msgHeap, 0);

/* Setup MessageQ transport */
var VirtioSetup = xdc.useModule('ti.ipc.transports.TransportRpmsgSetup');
MessageQ.SetupTransportProxy = VirtioSetup;

/* Setup NameServer remote proxy */
var NameServer = xdc.useModule("ti.sdo.utils.NameServer");
var NsRemote = xdc.useModule("ti.ipc.namesrv.NameServerRemoteRpmsg");
NameServer.SetupProxy = NsRemote;

/* Enable Memory Translation module that operates on the BIOS Resource Table */
var Resource = xdc.useModule('ti.ipc.remoteproc.Resource');

/*  Use SysMin because trace buffer address is required for Linux/QNX
 *  trace debug driver, plus provides better performance.
 */
var System = xdc.useModule('xdc.runtime.System');
var SysMin = xdc.useModule('ti.trace.SysMin');
System.SupportProxy = SysMin;
SysMin.bufSize  = 0x8000;

Program.sectMap[".tracebuf"] = "TRACE_BUF";
Program.sectMap[".errorbuf"] = "EXC_DATA";


var Timer = xdc.module('ti.sysbios.timers.dmtimer.Timer');
Timer.checkFrequency = false; /* Disable frequency check */
for (var i = 0; i < 4; i++) {
    Timer.intFreqs[i].lo = 20000000; /* Set a high Timer frequency value
                                        as default may be too small and
                                        cause frquenct interrupts. */
    Timer.intFreqs[i].hi = 0;
}

/* use external timers because they keep running when IPU is not */
var halTimer = xdc.useModule('ti.sysbios.hal.Timer');
halTimer.TimerProxy = Timer;

/* ----------------------------- TICK ---------------------------------------*/
var Clock = xdc.useModule('ti.sysbios.knl.Clock');
Clock.tickSource = Clock.TickSource_USER;

var Timer = xdc.useModule('ti.sysbios.timers.dmtimer.Timer');
var timerParams = new Timer.Params();
timerParams.period = Clock.tickPeriod;
timerParams.periodType = Timer.PeriodType_MICROSECS;
/* Smart-idle wake-up-capable mode */
timerParams.tiocpCfg.idlemode = 0x3;
/* Wake-up generation for Overflow */
timerParams.twer.ovf_wup_ena = 0x1;
Timer.create(Clock.timerId, Clock.doTick, timerParams);

/* configure the R5F MPU */
xdc.loadCapsule("R5fmpu.cfg");

/* idle functions */
var Idle = xdc.useModule('ti.sysbios.knl.Idle');

/* function to flush unicache on each core */
Idle.addCoreFunc('&VirtQueue_cacheWb', 0);
Idle.addCoreFunc('&VirtQueue_cacheWb', 1);

/* TODO: DEH disabled for now
var Deh = xdc.useModule('ti.deh.Deh');
*/

/* Watchdog detection functions in each core */
/* Must be placed before pwr mgmt */
//Idle.addCoreFunc('&ti_deh_Deh_idleBegin', 0);
//Idle.addCoreFunc('&ti_deh_Deh_idleBegin', 1);

/* Add idle function: Needed for safe reset from host */
xdc.useModule('ti.sdo.ipc.family.am65xx.Power');
Idle.addFunc('&Power_Idle');

/*
 *  ======== Instrumentation Configuration ========
 */

/* system logger */
var LoggerSys = xdc.useModule('xdc.runtime.LoggerSys');
var LoggerSysParams = new LoggerSys.Params();
var Defaults = xdc.useModule('xdc.runtime.Defaults');
Defaults.common$.logger = LoggerSys.create(LoggerSysParams);

/* enable runtime Diags_setMask() for non-XDC spec'd modules */
var Diags = xdc.useModule('xdc.runtime.Diags');
Diags.setMaskEnabled = true;

/* override diags mask for selected modules */
xdc.useModule('xdc.runtime.Main');
Diags.setMaskMeta("xdc.runtime.Main",
    Diags.ENTRY | Diags.EXIT | Diags.INFO, Diags.RUNTIME_ON);

var Registry = xdc.useModule('xdc.runtime.Registry');
Registry.common$.diags_ENTRY = Diags.RUNTIME_OFF;
Registry.common$.diags_EXIT  = Diags.RUNTIME_OFF;
Registry.common$.diags_INFO  = Diags.RUNTIME_OFF;
Registry.common$.diags_USER1 = Diags.RUNTIME_OFF;
Registry.common$.diags_LIFECYCLE = Diags.RUNTIME_OFF;
Registry.common$.diags_STATUS = Diags.RUNTIME_OFF;

var Main = xdc.useModule('xdc.runtime.Main');
Main.common$.diags_ASSERT = Diags.ALWAYS_ON;
Main.common$.diags_INTERNAL = Diags.ALWAYS_ON;

var Hwi = xdc.useModule('ti.sysbios.family.arm.v7r.keystone3.Hwi');
// TODO: Hwi.nvicCCR.DIV_0_TRP = 1;

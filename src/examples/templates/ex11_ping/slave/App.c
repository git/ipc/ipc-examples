/*
 * Copyright (c) 2012-2013, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
%%{
    this.platform = this.arguments[0];
    var platform = this.arguments[0];
    var core = this.arguments[1];
    var tCore = core.replace(/-/,"_");
%%}

/*
 *  ======== App`core`.c ========
 *  Platform: `platform`
 */

/* this define must precede inclusion of any xdc header file */
#define Registry_CURDESC App`tCore`__Desc
#define MODULE_NAME "App`tCore`"

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
#define HEAPID 0

#include <xdc/std.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/System.h>

#include <ti/ipc/MessageQ.h>
% } else  {
#include <xdc/std.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/System.h>

#include <ti/ipc/MessageQ.h>
#include <ti/ipc/SharedRegion.h>
% }

#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

/* local header files */
#include "App`core`.h"
#include "../shared/SvrMsg.h"

/* module structure */
typedef struct {
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
    IHeap_Handle        heap;           /* SR_0 heap handle */
% }
    MessageQ_Handle     localMsgQue;    /* created locally */
    MessageQ_QueueId    svrQue[10];     /* opened remotely */
    SvrMsg_Msg *        msg;
} App`tCore`_Module;

/* friend data */
extern Semaphore_Handle Svr`tCore`_doneFlag;

/* private data */
Registry_Desc           Registry_CURDESC;
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
static Int              Mod_curInit = 0;
% }
static App`tCore`_Module   Module;


/*
 *  ======== App`tCore`_destroy ========
 */
Int App`tCore`_destroy(Void)
{
    Int     status = 0;
    UInt16  numProcs;
    UInt16  procId;

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("--> App`tCore`_destroy:\n");
% } else  {
    Log_print0(Diags_ENTRY, "--> App`tCore`_destroy:");
% }

    /* TODO enter gate */

    /* wait for server thread to finish */
    while (Semaphore_getCount(Svr`tCore`_doneFlag) > 0) {
        Task_sleep(1);
    }

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else  {
    /* reference count the module usage */
    if (--Mod_curInit > 0) {
        goto leave;  /* object still in use */
    }
% }

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {

        /* fill and send message */
        Module.msg->cmd = SvrMsg_Cmd_STOP;
        MessageQ_put(Module.svrQue[procId], (MessageQ_Msg)Module.msg);

    }

% } else  {
    /* free the static message buffer */
    Memory_free(Module.heap, Module.msg, sizeof(SvrMsg_Msg));
%}

    /* close each remote processor's server queue */
    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {

        /* close remote message queue */
        status = MessageQ_close(&Module.svrQue[procId]);

        if (status < 0) {
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
            System_printf("App`tCore`_destroy: procId=%d, server queue close "
                    "error=%d \n", procId, status);
% } else  {
            Log_error2("App`tCore`_destroy: procId=%d, server queue close "
                    "error=%d", (IArg)procId, (IArg)status);
% }
            goto leave;
        }
    }

    /* delete the local message queue */
    status = MessageQ_delete(&Module.localMsgQue);

    if (status < 0) {
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
        System_printf("App`tCore`_destroy: queue delete error=%d \n", status);
% } else  {
        Log_error1("App`tCore`_destroy: queue delete error=%d", (IArg)status);
% }
        goto leave;
    }

leave:
    /* disable log events */
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("<-- App`tCore`_destroy: status=%d \n", status);
% } else  {
    Log_print1(Diags_EXIT, "<-- App`tCore`_destroy: status=%d", (IArg)status);
% }
    Diags_setMask(MODULE_NAME"-FEX");

    /*
     * Note that there isn't a Registry_removeModule() yet:
     *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=315448
     *
     * ... but this is where we'd call it.
     */

    /* TODO leave gate */
    return(status);
}

/*
 *  ======== App`tCore`_run ========
 */
Int App`tCore`_run(Void)
{
    Int     status = 0;
    Int     count;
    UInt16  numProcs;
    UInt16  procId;

% if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("--> App`tCore`_run: \n");
% }   else  {
    Log_print0(Diags_ENTRY, "--> App`tCore`_run:");
% }

    /* ping each remote server */
    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {
        for (count = 1; count <= SvrMsg_NUM_MSGS; count++) {
% if (this.platform.match(/^OMAPL138_linux_elf$/)) {
            System_printf("App`tCore`_run: ping procId=%d \n", procId);
% }    else  {
            Log_print1(Diags_INFO, "App`tCore`_run: ping procId=%d",
                    (IArg)procId);
% }

            /* fill and send message */
            Module.msg->cmd = SvrMsg_Cmd_START;
            Module.msg->svrProcId = MultiProc_INVALIDID;
            MessageQ_put(Module.svrQue[procId], (MessageQ_Msg)Module.msg);

            /* wait for return message */
            status = MessageQ_get(Module.localMsgQue,
                    (MessageQ_Msg *)&Module.msg, MessageQ_FOREVER);

            if (status < 0) {
% if (this.platform.match(/^OMAPL138_linux_elf$/)) {
                System_printf("App`tCore`_run: message receive error=%d \n",
                        status);
% } else  {
                Log_error1("App`tCore`_run: message receive error=%d",
                        (IArg)status);
% }
                goto leave;
            }
            else if (Module.msg->svrProcId != procId) {
% if (this.platform.match(/^OMAPL138_linux_elf$/)) {
                System_printf("App`tCore`_run: message receive error=%d \n",
                        status);
% } else  {
                Log_error2("App`tCore`_run: message delivery error, remote "
                        "procId=%d, server procId=%d",
                        (IArg)procId, (IArg)Module.msg->svrProcId);
% }
                goto leave;
            }

% if (this.platform.match(/^OMAPL138_linux_elf$/)) {
            System_printf("App`tCore`_run: ack received, procId=%d",
                    procId);
% } else  {
            Log_print1(Diags_INFO, "App`tCore`_run: ack received, procId=%d",
                    (IArg)procId);
% }
        }
    }

leave:
%     if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("<-- App`tCore`_run: %d \n", status);
% }    else  {
    Log_print1(Diags_EXIT, "<-- App`tCore`_run: %d", (IArg)status);
% }
    return(status);
}

/*
 *  ======== App`tCore`_setup ========
 */
Int App`tCore`_setup(Void)
{
    Int                 status = 0;
    Registry_Result     result;
    MessageQ_Params     msgqParams;
    UInt16              numProcs;
    UInt16              procId;
    Char                cbuf[48];

    /* TODO enter gate */

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
    /* reference count the module usage */
    if (Mod_curInit >= 1) {
        goto leave;  /* already initialized */
    }
% }

    /* initialize module state */
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
    Module.heap = NULL;
% }
    Module.localMsgQue = NULL;
    Module.msg = NULL;

    /* register with xdc.runtime to get a diags mask */
    result = Registry_addModule(&Registry_CURDESC, MODULE_NAME);

    if ((result != Registry_SUCCESS) && (result != Registry_ALREADY_ADDED)) {
        status = -1;
        goto leave;
    }

    /* enable some trace */
    Diags_setMask(MODULE_NAME"+FEX");
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("--> App`tCore`_setup:\n");
% } else  {
    Log_print0(Diags_ENTRY, "--> App`tCore`_setup:");
% }

    /* create local message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    Module.localMsgQue = MessageQ_create(`tCore`_AppMsgQue, &msgqParams);

    if (Module.localMsgQue == NULL) {
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
        System_printf("App`tCore`_setup: message queue create failed");
% } else  {
        Log_error0("App`tCore`_setup: message queue create failed");
% }
        status = -1;
        goto leave;
    }

    /* open each remote processor's server queue */
    numProcs = MultiProc_getNumProcessors();
    for (procId = 0; procId < numProcs; procId++) {

        /* compute the remote processor server queue name */
        System_sprintf(cbuf, SvrMsg_SvrQueNameFmt, MultiProc_getName(procId));

        /* open the remote message queue, loop until queue is available */
        do {
            status = MessageQ_open(cbuf, &Module.svrQue[procId]);
            if (status == MessageQ_E_NOTFOUND) {
                Task_sleep(1);
            }
        } while (status == MessageQ_E_NOTFOUND);

        if (status < 0) {
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
            System_printf("App`tCore`_setup: could not open slave message queue \n");
% } else  {
            Log_error0("App`tCore`_setup: could not open slave message queue");
% }
            goto leave;
        }

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
        System_printf("App`tCore`_setup: procId=%d, opened server queue \n",
                procId);
% } else  {
        Log_print1(Diags_INFO,"App`tCore`_setup: procId=%d, opened server queue",
                (IArg)procId);
% }
    }

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
     /* allocate a static message */
    Module.msg = (SvrMsg_Msg *)MessageQ_alloc(HEAPID,sizeof(SvrMsg_Msg));

    if (Module.msg == NULL) {
        System_printf("AppDsp_setup: could not allocate message\n");
        status = -1;
        goto leave;
    }

    /* initialize message header */
    MessageQ_setReplyQueue(Module.localMsgQue, (MessageQ_Msg)Module.msg);

    System_printf("AppDsp_setup: slave is ready \n");
% } else  {
    /* get the SR_0 heap handle */
    Module.heap = (IHeap_Handle)SharedRegion_getHeap(0);

    /* allocate a static message from SR_0 heap */
    Module.msg = (SvrMsg_Msg *)Memory_alloc(Module.heap, sizeof(SvrMsg_Msg),
            0, NULL);

    if (Module.msg == NULL) {
        Log_error0("App`tCore`_setup: could not allocate message");
        status = -1;
        goto leave;
    }

    /* initialize message header */
    MessageQ_staticMsgInit((MessageQ_Msg)Module.msg, sizeof(SvrMsg_Msg));
    MessageQ_setReplyQueue(Module.localMsgQue, (MessageQ_Msg)Module.msg);

    Log_print0(Diags_INFO,"App`tCore`_setup: slave is ready");
% }

leave:
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
    /* success, increment reference count */
    if (status >= 0) {
        Mod_curInit++;
    }

    /* error handling */
    else {
        /* unregister diags mask */
        /* release resources */
    }

% }
    /* TODO leave gate */
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("<-- App`tCore`_setup:\n");
% } else  {
    Log_print0(Diags_EXIT, "<-- App`tCore`_setup:");
% }
    return(status);
}

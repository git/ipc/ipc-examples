/*
 * Copyright (c) 2012-2013, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
%%{
    this.platform = this.arguments[0];
    var platform = this.arguments[0];
    var core = this.arguments[1];  /* e.g. "Eve1" */
    var tCore = core.replace(/-/,"_");
%%}

/*
 *  ======== Svr`tCore`.c ========
 *  Platform: `platform`
 */

/* this define must precede inclusion of any xdc header file */
#define Registry_CURDESC Svr`tCore`__Desc
#define MODULE_NAME "Svr`tCore`"

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
#include <xdc/std.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Registry.h>

% } else  {
#include <xdc/std.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/System.h>

% }
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

/* local header files */
#include "Svr`core`.h"
#include "../shared/SvrMsg.h"

/* module structure */
typedef struct {
    MessageQ_Handle localMsgQue;
} Svr`tCore`_Module;

/* friend data */
Semaphore_Handle Svr`tCore`_doneFlag = NULL;

/* private data */
Registry_Desc           Registry_CURDESC;
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
static Int              Mod_curInit = 0;
% }
static Svr`tCore`_Module   Module;


/*
 *  ======== Svr`tCore`_destroy ========
 */
Int Svr`tCore`_destroy(Void)
{
    Int status = 0;

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("--> Svr`tCore`_destroy:");
% } else  {
    Log_print0(Diags_ENTRY, "--> Svr`tCore`_destroy:");
% }

    /* TODO enter gate */

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
    /* reference count the module usage */
    if (--Mod_curInit > 0) {
        goto leave;  /* object still in use */
    }

% }
    /* delete the local message queue */
    status = MessageQ_delete(&Module.localMsgQue);

    if (status < 0) {
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
        System_printf("Svr`tCore`_destroy: message queue delete error=%d \n",
                status);
% } else  {
        Log_error1("Svr`tCore`_destroy: message queue delete error=%d",
                (IArg)status);
% }
        goto leave;
    }

    /* delete the done flag */
    Semaphore_delete(&Svr`tCore`_doneFlag);

leave:
    /* disable log events */
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("<-- Svr`tCore`_delete: status=%d \n", status);
% } else  {
    Log_print1(Diags_EXIT, "<-- Svr`tCore`_delete: status=%d", (IArg)status);
% }
    Diags_setMask(MODULE_NAME"-FEX");

    /*
     * Note that there isn't a Registry_removeModule() yet:
     *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=315448
     *
     * ... but this is where we'd call it.
     */

    /* TODO leave gate */
    return(status);
}

/*
 *  ======== Svr`tCore`_run ========
 */
Int Svr`tCore`_run(Void)
{
    Int                 status = 0;
    Bool                running = TRUE;
    SvrMsg_Msg *        msg;
    MessageQ_QueueId    queId;
    UInt16              procId;
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    UInt16              numProcs;
    UInt16              count = 0;
% } else  {
% }

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("--> Svr`tCore`_run:\n");

    numProcs = MultiProc_getNumProcessors();
% } else  {
    Log_print0(Diags_ENTRY, "--> Svr`tCore`_run:");
% }

    while (running) {

        /* wait for inbound message */
        status = MessageQ_get(Module.localMsgQue, (MessageQ_Msg *)&msg,
            MessageQ_FOREVER);

        if (status < 0) {
 %  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
            System_printf("Svr`tCore`_run: message get error=%d \n", status);
% } else  {
            Log_error1("Svr`tCore`_run: message get error=%d", (IArg)status);
% }
            goto leave;
        }

        /* process the message */
        queId = MessageQ_getReplyQueue(msg);
        procId = MessageQ_getProcId(queId);
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
        System_printf("Svr`tCore`_run: message received, procId=%d, "
                "cmd=%d \n", procId, msg->cmd);
% } else  {
        Log_print2(Diags_INFO, "Svr`tCore`_run: message received, procId=%d, "
                "cmd=%d", procId, msg->cmd);
% }

        switch (msg->cmd) {
            case SvrMsg_Cmd_START:
                msg->svrProcId = MultiProc_self();
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
                /* send message back */
                MessageQ_put(queId, (MessageQ_Msg)msg);
% }
                break;

            case SvrMsg_Cmd_PROCESS:
                break;

            case SvrMsg_Cmd_STOP:
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
                count++;
                /*
                 * When stop commands have been recieved from all procs
                 * terminate server
                 */
                if (count == numProcs) {
                    /* Free last message */
                    MessageQ_free((MessageQ_Msg)msg);
                    running = FALSE;
                }
                break;
% } else  {
                running = FALSE;
                break;
% }

            default:
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
                System_printf("Svr`tCore`_run: unknown message, cmd=%d \n",
                        (msg->cmd));
% } else  {
                Log_error1("Svr`tCore`_run: unknown message, cmd=%d",
                        (IArg)(msg->cmd));
% }
                break;
        }

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else  {
        /* send message back */
        MessageQ_put(queId, (MessageQ_Msg)msg);
% }

        /* decrement the done flag */
        if (Semaphore_getCount(Svr`tCore`_doneFlag) > 0) {
            Semaphore_pend(Svr`tCore`_doneFlag, BIOS_WAIT_FOREVER);
        }

    } /* while (running) */

leave:
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("<-- Svr`tCore`_run: %d \n", status);
% } else  {
    Log_print1(Diags_EXIT, "<-- Svr`tCore`_run: %d", (IArg)status);
% }
    return(status);
}

/*
 *  ======== Svr`tCore`_setup ========
 */
Int Svr`tCore`_setup(Void)
{
    Int                 status = 0;
    Error_Block         eb;
    Int                 count;
    Semaphore_Params    semParams;
    Registry_Result     result;
    MessageQ_Params     msgqParams;
    UInt16              procId;
    Char                cbuf[48];

    /* TODO enter gate */

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
    /* reference count the module usage */
    if (Mod_curInit >= 1) {
        goto leave;  /* already initialized */
    }

% }
    /* initialize module state */
    Error_init(&eb);
    Module.localMsgQue = NULL;

    /* initialize the done flag */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_COUNTING;
    count = MultiProc_getNumProcessors() * SvrMsg_NUM_MSGS;

    Svr`tCore`_doneFlag = Semaphore_create(count, &semParams, &eb);

    if (Error_check(&eb)) {
        status = -2;
        goto leave;
    }

    /* register with xdc.runtime to get a diags mask */
    result = Registry_addModule(&Registry_CURDESC, MODULE_NAME);

    if ((result != Registry_SUCCESS) && (result != Registry_ALREADY_ADDED)) {
        status = -1;
        goto leave;
    }

    /* enable some trace */
    Diags_setMask(MODULE_NAME"+FEX");
 %  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("--> Svr`tCore`_setup:\n");
% } else  {
    Log_print0(Diags_ENTRY, "--> Svr`tCore`_setup:");
% }

    /* compute the server queue name */
    procId = MultiProc_self();
    System_sprintf(cbuf, SvrMsg_SvrQueNameFmt, MultiProc_getName(procId));

    /* create local message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    Module.localMsgQue = MessageQ_create(cbuf, &msgqParams);

    if (Module.localMsgQue == NULL) {
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
        System_printf("Svr`tCore`_setup: message queue create failed\n");
% } else  {
        Log_error0("Svr`tCore`_setup: message queue create failed");
% }
        goto leave;
    }

%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("Svr`tCore`_setup: slave is ready\n");
% } else  {
    Log_print0(Diags_INFO,"Svr`tCore`_setup: slave is ready");
% }

leave:
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
% } else {
    /* success, increment reference count */
    if (status >= 0) {
        Mod_curInit++;
    }

    /* error handling */
    else {
        /* unregister diags mask */
        /* release resources */
    }
% }

    /* TODO leave gate */
%  if (this.platform.match(/^OMAPL138_linux_elf$/)) {
    System_printf("<-- Svr`tCore`_setup:\n");
% } else  {
    Log_print0(Diags_EXIT, "<-- Svr`tCore`_setup:");
% }
    return(status);
}

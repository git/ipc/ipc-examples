/*
 * Copyright (c) 2012-2018 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== SvrMsg.h ========
 *
 */

#ifndef SvrMsg__include
#define SvrMsg__include
#if defined (__cplusplus)
extern "C" {
#endif

#include <ti/ipc/MessageQ.h>

#define SvrMsg_NUM_MSGS 5       /* number of message to send each server */

#define SvrMsg_SvrQueNameFmt "%s_SvrMsgQ"   /* %s = proc name */

% if (this.platform.match(/^DRA7XX_bios_elf$/)) {
#define Dsp1_AppMsgQue "Dsp1_AppMsgQ"
#define Dsp2_AppMsgQue "Dsp2_AppMsgQ"
#define Eve1_AppMsgQue "Eve1_AppMsgQ"
#define Eve2_AppMsgQue "Eve2_AppMsgQ"
#define Eve3_AppMsgQue "Eve3_AppMsgQ"
#define Eve4_AppMsgQue "Eve4_AppMsgQ"
#define Ipu1_AppMsgQue "Ipu1_AppMsgQ"
#define Ipu1_0_AppMsgQue "Ipu1-0_AppMsgQ"
#define Ipu1_1_AppMsgQue "Ipu1-1_AppMsgQ"
#define Ipu2_AppMsgQue "Ipu2_AppMsgQ"
#define Host_AppMsgQue "Host_AppMsgQ"
% } else if (this.platform.match(/^AM572X_bios_elf$/)) {
#define Dsp1_AppMsgQue "Dsp1_AppMsgQ"
#define Dsp2_AppMsgQue "Dsp2_AppMsgQ"
#define Ipu1_AppMsgQue "Ipu1_AppMsgQ"
#define Ipu1_0_AppMsgQue "Ipu1-0_AppMsgQ"
#define Ipu1_1_AppMsgQue "Ipu1-1_AppMsgQ"
#define Ipu2_AppMsgQue "Ipu2_AppMsgQ"
#define Host_AppMsgQue "Host_AppMsgQ"
% } else if (this.platform.match(/^AM571X_bios_elf$/)) {
#define Dsp1_AppMsgQue "Dsp1_AppMsgQ"
#define Ipu1_AppMsgQue "Ipu1_AppMsgQ"
#define Ipu1_0_AppMsgQue "Ipu1-0_AppMsgQ"
#define Ipu1_1_AppMsgQue "Ipu1-1_AppMsgQ"
#define Ipu2_AppMsgQue "Ipu2_AppMsgQ"
#define Host_AppMsgQue "Host_AppMsgQ"
% } else if (this.platform.match(/^VAYUsim_bios_elf$/)) {
#define Dsp1_AppMsgQue "Dsp1_AppMsgQ"
#define Dsp2_AppMsgQue "Dsp2_AppMsgQ"
#define Eve1_AppMsgQue "Eve1_AppMsgQ"
#define Eve2_AppMsgQue "Eve2_AppMsgQ"
#define Eve3_AppMsgQue "Eve3_AppMsgQ"
#define Eve4_AppMsgQue "Eve4_AppMsgQ"
#define Ipu1_AppMsgQue "Ipu1_AppMsgQ"
#define Ipu1_AppMsgQue "Ipu1_0_AppMsgQ"
#define Ipu1_AppMsgQue "Ipu1_1_AppMsgQ"
% } else if (this.platform.match(/^TI814X_bios_elf$/)) {
#define Dsp_AppMsgQue   "Dsp_AppMsgQ"
#define Video_AppMsgQue "Video_AppMsgQ"
#define Host_AppMsgQue  "Host_AppMsgQ"
% } else if (this.platform.match(/^OMAPL138_linux_elf$/)) {
#define Dsp_AppMsgQue   "Dsp_AppMsgQ"
#define Host_AppMsgQue  "Host_AppMsgQ"
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
#define Dsp_AppMsgQue   "Dsp_AppMsgQ"
#define Video_AppMsgQue "Video_AppMsgQ"
#define Host_AppMsgQue  "Host_AppMsgQ"
#define Eve_AppMsgQue   "Eve_AppMsgQ"
% } else if (this.platform.match(/^C6472_bios_elf$/)) {
#define Core0_AppMsgQue  "Core0_AppMsgQ"
#define Core1_AppMsgQue  "Core1_AppMsgQ"
#define Core2_AppMsgQue  "Core2_AppMsgQ"
#define Core3_AppMsgQue  "Core3_AppMsgQ"
#define Core4_AppMsgQue  "Core4_AppMsgQ"
#define Core5_AppMsgQue  "Core5_AppMsgQ"
% } else if (this.platform.match(/^C6678_bios_elf$/)) {
#define Core0_AppMsgQue  "Core0_AppMsgQ"
#define Core1_AppMsgQue  "Core1_AppMsgQ"
#define Core2_AppMsgQue  "Core2_AppMsgQ"
#define Core3_AppMsgQue  "Core3_AppMsgQ"
#define Core4_AppMsgQue  "Core4_AppMsgQ"
#define Core5_AppMsgQue  "Core5_AppMsgQ"
#define Core6_AppMsgQue  "Core6_AppMsgQ"
#define Core7_AppMsgQue  "Core7_AppMsgQ"
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
#define Dsp1_AppMsgQue "Dsp1_AppMsgQ"
#define Dsp2_AppMsgQue "Dsp2_AppMsgQ"
#define Eve1_AppMsgQue "Eve1_AppMsgQ"
#define Ipu1_AppMsgQue "Ipu1_AppMsgQ"
#define Ipu1_0_AppMsgQue "Ipu1-0_AppMsgQ"
#define Ipu1_1_AppMsgQue "Ipu1-1_AppMsgQ"
% } else if (this.platform.match(/^TCI6636_bios_elf$/)) {
#define Core0_AppMsgQue  "Core0_AppMsgQ"
#define Core1_AppMsgQue  "Core1_AppMsgQ"
#define Core2_AppMsgQue  "Core2_AppMsgQ"
#define Core3_AppMsgQue  "Core3_AppMsgQ"
#define Core4_AppMsgQue  "Core4_AppMsgQ"
#define Core5_AppMsgQue  "Core5_AppMsgQ"
#define Core6_AppMsgQue  "Core6_AppMsgQ"
#define Core7_AppMsgQue  "Core7_AppMsgQ"
#define Host_AppMsgQue "Host_AppMsgQ"
% } else if (this.platform.match(/^(66AK2E|66AK2G)_bios_elf$/)) {
#define Core0_AppMsgQue  "Core0_AppMsgQ"
#define Host_AppMsgQue "Host_AppMsgQ"
% } else if (this.platform.match(/^TCI6630_bios_elf$/)) {
#define Core0_AppMsgQue  "Core0_AppMsgQ"
#define Core1_AppMsgQue  "Core1_AppMsgQ"
#define Core2_AppMsgQue  "Core2_AppMsgQ"
#define Core3_AppMsgQue  "Core3_AppMsgQ"
#define Host_AppMsgQue "Host_AppMsgQ"
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
#define R5f_0_AppMsgQue  "R5f-0_AppMsgQ"
#define R5f_1_AppMsgQue  "R5f-1_AppMsgQ"
#define Host_AppMsgQue "Host_AppMsgQ"
% } else {
%   throw new Error("unsupported platform: " + this.platform);
% }

#define SvrMsg_Cmd_START        1
#define SvrMsg_Cmd_STOP         2
#define SvrMsg_Cmd_PROCESS      3

typedef struct {
    MessageQ_MsgHeader  reserved;

    UInt32      cmd;
    UInt16      svrProcId;
    Char        buf[32];
} SvrMsg_Msg;


#if defined (__cplusplus)
}
#endif
#endif

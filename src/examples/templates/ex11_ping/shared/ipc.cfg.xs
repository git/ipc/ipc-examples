/*
 * Copyright (c) 2012-2018 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== ipc.cfg.xs ========
 *
 */
var Program = xdc.useModule('xdc.cfg.Program');
var cfgArgs = Program.build.cfgArgs;

/* configure processor names */
//var procNameAry = MultiProc.getDeviceProcNames();
% if (this.platform.match(/^(DRA7XX|AM572X)_bios_elf$/)) {
//var procNameAry = ["DSP1","DSP2","EVE1","EVE2","EVE3","EVE4","IPU1","HOST"];
% } else if (this.platform.match(/^AM572X_bios_elf$/)) {
//var procNameAry = ["DSP1", "DSP2", "IPU1", "HOST"];
% } else if (this.platform.match(/^AM571X_bios_elf$/)) {
//var procNameAry = ["DSP1", "IPU1", "HOST"];
% } else if (this.platform.match(/^VAYUsim_bios_elf$/)) {
//var procNameAry = ["DSP1", "DSP2", "EVE1", "EVE2", "EVE3", "EVE4", "IPU1"];
% } else if (this.platform.match(/^TI814X_bios_elf$/)) {
//var procNameAry = [ "DSP", "VIDEO-M3", "HOST" ];
% } else if (this.platform.match(/^C6A8149_bios_elf$/)) {
//var procNameAry = [ "DSP", "EVE", "VIDEO-M3", "HOST" ];
% } else if (this.platform.match(/^C6472_bios_elf$/)) {
//var procNameAry = [ "CORE0", "CORE1", "CORE2", "CORE3", "CORE4", "CORE5" ];
% } else if (this.platform.match(/^C6678_bios_elf$/)) {
//var procNameAry = [ "CORE0", "CORE1", "CORE2", "CORE3", "CORE4", "CORE5",
//        "CORE6", "CORE7" ];
% } else if (this.platform.match(/^TDA3XX_bios_elf$/)) {
//var procNameAry = ["DSP1","DSP2","IPU1","EVE1"];
% } else if (this.platform.match(/^TCI6636_bios_elf$/)) {
//var procNameAry = [ "HOST","CORE0", "CORE1", "CORE2", "CORE3", "CORE4", "CORE5",
//        "CORE6", "CORE7" ];
% } else if (this.platform.match(/^(66AK2E|66AK2G)_bios_elf$/)) {
//var procNameAry = [ "HOST","CORE0" ];
% } else if (this.platform.match(/^TCI6630_bios_elf$/)) {
//var procNameAry = [ "HOST","CORE0", "CORE1", "CORE2", "CORE3" ];
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
//var procNameAry = [ "HOST","R5F-0", "R5F-1" ];
% } else {
%     throw ("Unsupported platform: " + platform);
% }
var procNameAry = cfgArgs.procList.toUpperCase().split(/\s+/);

var MultiProc = xdc.useModule('ti.sdo.utils.MultiProc');
MultiProc.setConfig(Program.global.procName, procNameAry);

/* ipc configuration */
var Ipc = xdc.useModule('ti.sdo.ipc.Ipc');
Ipc.procSync = Ipc.ProcSync_ALL; /* synchronize all processors in Ipc_start */
Ipc.sr0MemorySetup = true;

% if (this.platform.match(/^C6A8149_bios_elf$/)) {
var Notify = xdc.module('ti.sdo.ipc.Notify');

/*
 * The following line uses a more optimized Notify driver
 */
Notify.SetupProxy = xdc.useModule('ti.sdo.ipc.family.c6a8149.NotifyCircSetup');

% }
/* shared region configuration */
var SharedRegion = xdc.useModule('ti.sdo.ipc.SharedRegion');

/* configure SharedRegion #0 (IPC) */
% if (this.platform.match(/^C6472_bios_elf$/)) {
var SR0Mem = Program.cpu.memoryMap["SL2RAM"];
% } else if (this.platform.match(/^AM65XX_bios_elf$/)) {
% } else {
var SR0Mem = Program.cpu.memoryMap["SR_0"];
% }

SharedRegion.setEntryMeta(0,
    new SharedRegion.MetaEntry({
        name:           "SR_0",
% if (this.platform.match(/AM65XX/)) {
        base_symbol:    "sr0_base",
        len_symbol:     "sr0_len",
% } else {
        base:           SR0Mem.base,
        len:            SR0Mem.len,
% }
% if (this.platform.match(/^(TCI6636|66AK2E|TCI6630|66AK2G)_bios_elf$/)) {
        ownerProcId:    1,
% } else {
        ownerProcId:    0,
% }
% if (this.platform.match(/^C6472_bios_elf$/)) {
        cacheLineSize:  64, /* SL2 memory has a cache line size of 64 */
% }
        isValid:        true,
% if (this.platform.match(/VAYU|DRA7XX|AM572X|AM571X|TDA3XX|AM65XX/)) {
        cacheEnable:    xdc.global.SR0_cacheEnable
% } else {
        cacheEnable:    true
% }
    })
);

/* reduce data memory usage */
var GateMP = xdc.useModule('ti.sdo.ipc.GateMP');
GateMP.maxRuntimeEntries = 4;
GateMP.RemoteCustom1Proxy = xdc.useModule('ti.sdo.ipc.gates.GateMPSupportNull');

/* reduce data memory usage */
var Notify = xdc.useModule('ti.sdo.ipc.Notify');
Notify.numEvents = 8;

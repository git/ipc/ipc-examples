#
#  Copyright (c) 2012-2015 Texas Instruments Incorporated - http://www.ti.com
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  *  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#  *  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#  *  Neither the name of Texas Instruments Incorporated nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
%%{
    // used to 'escape' the percent and backtick chars throughout
    var PCT = '%';
    var BKTK = '`';

    var platform = this.arguments[0];
    var core = this.arguments[1];
    var suffix = this.arguments[2];
    var target = this.arguments[3];

    var lcCore = core.toLowerCase();
    var platInst = "";
    var cgtools = target;

    if (platform.match(/^DRA7XX_bios_elf$/)) {
        platInst = "ti.platforms.evmDRA7XX:" + lcCore;
    } else {
        throw new Error("unsupported platform: " + platform);
    }

    ldlibs = "$(CGTOOLS)/lib/libc.a"
    if (target.match(/\.C66$/)) {
        cc = "bin/cl6x";
        lnk = "bin/cl6x -z";
    }
    else if (target.match(/\.ARP32(_far|)$/)) {
        cc = "bin/cl-arp32";
        lnk = "bin/cl-arp32 -z";
    }
    else if (target.match(/\.M4$/)) {
        cc = "bin/armcl";
        lnk = "bin/armcl -z";
    }
    else {
        throw new Error("unsupported target: " + target);
    }

%%}

#
#  ======== makefile ========
#

EXBASE = ..
include $(EXBASE)/products.mak

srcs = Hello`core`.c
objs = $(addprefix bin/$(PROFILE)/obj/,$(patsubst %.c,%.o`suffix`,$(srcs)))
CONFIG = bin/$(PROFILE)/configuro

-include $(addprefix bin/$(PROFILE)/obj/,$(patsubst %.c,%.o`suffix`.dep,$(srcs)))

.PRECIOUS: %/compiler.opt %/linker.cmd

all:
	$(MAKE) PROFILE=debug hello_`lcCore`.x
	$(MAKE) PROFILE=release hello_`lcCore`.x

hello_`lcCore`.x: bin/$(PROFILE)/hello_`lcCore`.x`suffix`
bin/$(PROFILE)/hello_`lcCore`.x`suffix`: $(objs) $(libs) $(CONFIG)/linker.cmd
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(LD) $(LDFLAGS) -o $@ $^ $(LDLIBS)

bin/$(PROFILE)/obj/%.o`suffix`: %.c $(CONFIG)/compiler.opt
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(CC) $(CPPFLAGS) $(CFLAGS) --output_file=$@ -fc $<

`PCT`/compiler.opt: `PCT`/linker.cmd ;
`PCT`/linker.cmd: `core`.cfg ../shared/config.bld
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(XDC_INSTALL_DIR)/xs --xdcpath="$(subst +,;,$(PKGPATH))" \
            xdc.tools.configuro -o $(CONFIG) \
            -t `target` \
            -c $(`cgtools`) \
            -p `platInst` \
            -b ../shared/config.bld -r release \
% if (target.match(/\.M4$/)) {
            --cfgArgs "{ \
                profile: \"$(PROFILE)\", \
                configBld: \"../shared/config.bld\" \
            }" `core`.cfg
% } else {
            --cfgArgs "{ \
                profile: \"$(PROFILE)\" \
            }" `core`.cfg
% }

install:
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	@$(MKDIR) $(EXEC_DIR)/debug
	$(CP) bin/debug/hello_`lcCore`.x`suffix` $(EXEC_DIR)/debug
% if (platform.match(/DRA7XX/) && lcCore.match(/eve|ipu1(?!-1)|ipu2/)) {
	$(CP) ex01_hello_`lcCore`.gel $(EXEC_DIR)/debug
% }
	@$(MKDIR) $(EXEC_DIR)/release
	$(CP) bin/release/hello_`lcCore`.x`suffix` $(EXEC_DIR)/release
% if (platform.match(/DRA7XX/) && lcCore.match(/eve|ipu1(?!-1)|ipu2/)) {
	$(CP) ex01_hello_`lcCore`.gel $(EXEC_DIR)/release
% }

install_rov:
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	@$(MKDIR) $(EXEC_DIR)/debug
	$(CP) bin/debug/configuro/package/cfg/`core`_p`suffix`.rov.xs $(EXEC_DIR)/debug
	@$(MKDIR) $(EXEC_DIR)/release
	$(CP) bin/release/configuro/package/cfg/`core`_p`suffix`.rov.xs $(EXEC_DIR)/release

help:
	@$(ECHO) "make                   # build executable"
	@$(ECHO) "make clean             # clean everything"

clean::
	$(RMDIR) bin

PKGPATH := $(BIOS_INSTALL_DIR)/packages
PKGPATH := $(PKGPATH)+$(IPC_INSTALL_DIR)/packages
PKGPATH := $(PKGPATH)+$(XDC_INSTALL_DIR)/packages

#  ======== install validation ========
ifeq (install,$(MAKECMDGOALS))
ifeq (,$(EXEC_DIR))
$(error must specify EXEC_DIR)
endif
endif

#  ======== toolchain macros ========
CGTOOLS = $(`cgtools`)

CC = $(CGTOOLS)/`cc` -c
%// unused
%// AR = $(CGTOOLS)/`ar`
LD = $(CGTOOLS)/`lnk`
%// unused
%// ST = $(CGTOOLS)/`strip`

CPPFLAGS =
CFLAGS = -qq -pdsw225 -ppd=$@.dep -ppa $(CCPROFILE_$(PROFILE)) -@$(CONFIG)/compiler.opt -I.

LDFLAGS = -w -q -c -m $(@D)/obj/$(@F).map
LDLIBS = -l `ldlibs`

CCPROFILE_debug = -D_DEBUG_=1 --symdebug:dwarf
CCPROFILE_release = -O2

#  ======== standard macros ========
ifneq (,$(wildcard $(XDC_INSTALL_DIR)/xdc.exe))
    # use these on Windows
    CP      = $(XDC_INSTALL_DIR)/bin/cp
    ECHO    = $(XDC_INSTALL_DIR)/bin/echo
    MKDIR   = $(XDC_INSTALL_DIR)/bin/mkdir -p
    RM      = $(XDC_INSTALL_DIR)/bin/rm -f
    RMDIR   = $(XDC_INSTALL_DIR)/bin/rm -rf
else
    # use these on Linux
    CP      = cp
    ECHO    = echo
    MKDIR   = mkdir -p
    RM      = rm -f
    RMDIR   = rm -rf
endif

#  ======== create output directories ========
ifneq (clean,$(MAKECMDGOALS))
ifneq (,$(PROFILE))
ifeq (,$(wildcard bin/$(PROFILE)/obj))
    $(shell $(MKDIR) -p bin/$(PROFILE)/obj)
endif
endif
endif

#
#  Copyright (c) 2013-2015 Texas Instruments Incorporated - http://www.ti.com
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  *  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#  *  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#  *  Neither the name of Texas Instruments Incorporated nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
%%{
    // used to 'escape' the percent and backtick chars throughout
    var PCT = '%';
    var BKTK = '`';

    var platform = this.arguments[0];
    var core = this.arguments[1];
    var suffix = this.arguments[2];
    var target = this.arguments[3];

    var lcCore = core.toLowerCase();
    var platInst = "";
    var cgtools = target;
    var cfgBldDir = "shared";  /* by default, may be overridden */

    if (platform.match(/^DRA7XX_(linux|qnx)_elf$/)) {
        platInst = "ti.platforms.evmDRA7XX:" + lcCore;
    } else if (platform.match(/^OMAP54XX_(linux|qnx)_elf$/)) {
        platInst = "ti.platforms.sdp5430:" + core;
    } else if (platform.match(/^OMAPL138_linux_elf$/)) {
        platInst = "ti.platforms.evmOMAPL138:" + lcCore;
	cfgBldDir = "dsp";
    } else {
        throw new Error("unsupported platform: " + platform);
    }
%%}

#
#  ======== makefile ========
#

EXBASE = ..
include $(EXBASE)/products.mak

srcs = main_ipu.c
objs = $(addprefix bin/$(PROFILE)/obj/,$(patsubst %.c,%.oem4,$(srcs)))
libs = ../machvis/lib/$(PROFILE)/libmachvis_ipu.aem4
CONFIG = bin/$(PROFILE)/configuro

PKGPATH := $(BIOS_INSTALL_DIR)/packages
PKGPATH := $(PKGPATH)+$(IPC_INSTALL_DIR)/packages
PKGPATH := $(PKGPATH)+$(XDC_INSTALL_DIR)/packages

-include $(addprefix bin/$(PROFILE)/obj/,$(patsubst %.c,%.oem4.dep,$(srcs)))

.PRECIOUS: %/compiler.opt %/linker.cmd

all: debug release

debug:
	$(MAKE) PROFILE=debug PROCLIST="$(PROCLIST)" server_ipu.x

release:
	$(MAKE) PROFILE=release PROCLIST="$(PROCLIST)" server_ipu.x

server_ipu.x: bin/$(PROFILE)/server_ipu.xem4
bin/$(PROFILE)/server_ipu.xem4: $(objs) $(libs) $(CONFIG)/linker.cmd
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(LD) $(LDFLAGS) -o $@ -u MachVis_startup $(objs) \
            $(addprefix -l ,$(libs)) $(CONFIG)/linker.cmd $(LDLIBS)

bin/$(PROFILE)/obj/%.oem4: %.c $(CONFIG)/compiler.opt
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(CC) $(CPPFLAGS) $(CFLAGS) --output_file=$@ -fc $<

`PCT`/linker.cmd `PCT`/compiler.opt: $(CONFIG)/.config ;
$(CONFIG)/.config: `core`.cfg ../shared/config.bld
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(XDC_INSTALL_DIR)/xs --xdcpath="$(subst +,;,$(PKGPATH))" \
            xdc.tools.configuro -o $(CONFIG) \
            -t `target` \
            -c $(`cgtools`) \
            -p `platInst` \
            -b ../shared/config.bld -r $(PROFILE) \
            --cfgArgs "{ configBld: \"../shared/config.bld\" }" \
            `core`.cfg
	@$(ECHO) "" > $@

install:
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	@$(MKDIR) $(EXEC_DIR)/debug
	$(CP) bin/debug/server_ipu.xem4 $(EXEC_DIR)/debug
	@$(MKDIR) $(EXEC_DIR)/release
	$(CP) bin/release/server_ipu.xem4 $(EXEC_DIR)/release

help:
	@$(ECHO) "make                   # build executable"
	@$(ECHO) "make clean             # clean everything"

clean::
	$(RMDIR) bin

#  ======== install validation ========
ifeq (install,$(MAKECMDGOALS))
ifeq (,$(EXEC_DIR))
$(error must specify EXEC_DIR)
endif
endif

#  ======== toolchain macros ========
CGTOOLS = $(ti.targets.arm.elf.M4)

CC = $(CGTOOLS)/bin/armcl -c
LD = $(CGTOOLS)/bin/armcl -z

CPPFLAGS =
CFLAGS = -qq -pdsw225 -ppd=$@.dep -ppa $(CCPROFILE_$(PROFILE)) -@$(CONFIG)/compiler.opt -I.

LDFLAGS = -w -q -c -m $(@D)/obj/$(@F).map
LDLIBS = -l $(CGTOOLS)/lib/rtsv7M4_T_le_eabi.lib

CCPROFILE_debug = -D_DEBUG_=1 --symdebug:dwarf
CCPROFILE_release = -O2

#  ======== standard macros ========
ifneq (,$(wildcard $(XDC_INSTALL_DIR)/xdc.exe))
    # use these on Windows
    CP      = $(XDC_INSTALL_DIR)/bin/cp
    ECHO    = $(XDC_INSTALL_DIR)/bin/echo
    MKDIR   = $(XDC_INSTALL_DIR)/bin/mkdir -p
    RM      = $(XDC_INSTALL_DIR)/bin/rm -f
    RMDIR   = $(XDC_INSTALL_DIR)/bin/rm -rf
else
    # use these on Linux
    CP      = cp
    ECHO    = echo
    MKDIR   = mkdir -p
    RM      = rm -f
    RMDIR   = rm -rf
endif

#  ======== create output directories ========
ifneq (clean,$(MAKECMDGOALS))
ifneq (,$(PROFILE))
ifeq (,$(wildcard bin/$(PROFILE)/obj))
    $(shell $(MKDIR) -p bin/$(PROFILE)/obj)
endif
endif
endif

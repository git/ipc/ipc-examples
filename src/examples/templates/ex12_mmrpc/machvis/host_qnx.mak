#
#  Copyright (c) 2013, Texas Instruments Incorporated
#  All rights reserved.
#
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
#
#  *  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
#
#  *  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#
#  *  Neither the name of Texas Instruments Incorporated nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
#
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#
#  ======== host.mak ========
#

EXBASE = ..
include $(EXBASE)/products.mak

.PHONY: lib

PKGPATH := $(BIOS_INSTALL_DIR)/packages
PKGPATH := $(PKGPATH)+$(IPC_INSTALL_DIR)/packages
PKGPATH := $(PKGPATH)+$(XDC_INSTALL_DIR)/packages
export PKGPATH

#
#  determine build host OS (Windows or Linux)
#
ifeq (,$(BUILDHOSTOS))
    ifeq (,$(findstring :,$(WINDIR)$(windir)$(COMSPEC)$(comspec)))
       BUILDHOSTOS := Linux
    else
       BUILDHOSTOS := Windows
    endif
endif

ifeq (Linux,$(BUILDHOSTOS))
QNX_PATH := $(QNX_INSTALL_DIR)/host/linux/x86/usr/bin
QNX_PATH := $(QNX_PATH):$(QNX_INSTALL_DIR)/host/linux/x86/bin
QNX_PATH := $(QNX_PATH):$(QNX_INSTALL_DIR)/host/linux/x86/sbin
QNX_PATH := $(QNX_PATH):$(QNX_INSTALL_DIR)/host/linux/x86/usr/sbin
QNX_PATH := $(QNX_PATH):$(QNX_INSTALL_DIR)/linux/x86/usr/photon/appbuilder
endif

export PATH

export QNX_CONFIGURATION    ?= $(QNX_INSTALL_DIR)/etc/qnx
export QNX_HOST             ?= $(QNX_INSTALL_DIR)/host/linux/x86
export QNX_TARGET           ?= $(QNX_INSTALL_DIR)/target/qnx6
export MAKEFLAGS            = -I$(QNX_INSTALL_DIR)/target/qnx6/usr/include
export LD_LIBRARY_PATH      ?= $(QNX_INSTALL_DIR)/host/linux/x86/usr/lib
export IPC_INSTALL_DIR

#
#  ======== rules ========
#
all: lib

lib: PATH:=$(QNX_PATH):$(PATH)
lib:
	@$(ECHO) "#"
	@$(ECHO) "# Making $@ ..."
	$(MAKE) -C Qnx

clean:: PATH:=$(QNX_PATH):$(PATH)
clean::
	$(MAKE) -C Qnx clean

#
#  ======== standard macros ========
#
ifneq (,$(wildcard $(XDC_INSTALL_DIR)/xdc.exe))
    # use these on Windows
    CP      = $(XDC_INSTALL_DIR)/bin/cp
    ECHO    = $(XDC_INSTALL_DIR)/bin/echo
    MKDIR   = $(XDC_INSTALL_DIR)/bin/mkdir -p
    RM      = $(XDC_INSTALL_DIR)/bin/rm -f
    RMDIR   = $(XDC_INSTALL_DIR)/bin/rm -rf
else
    # use these on Linux
    CP      = cp
    ECHO    = echo
    MKDIR   = mkdir -p
    RM      = rm -f
    RMDIR   = rm -rf
endif

#
#  ======== create output directories ========
#
ifneq (clean,$(MAKECMDGOALS))
ifneq (,$(PROFILE))
ifeq (,$(wildcard lib/$(PROFILE)/obj))
    $(shell $(MKDIR) -p lib/$(PROFILE)/obj)
endif
endif
endif

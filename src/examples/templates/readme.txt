IPC Examples Templates
============================


Example Categories
------------------------------------------------------------------------
 1 - 10: Fundamental examples to illustrate basic module use.
11 - 20: Fundamental examples to illustrate basic module use.
21 - 30: --------
31 - 40: Performance examples
41 - 50: Application specific examples
51 - 60: Application specific examples
61 - 70: Specialty examples
71 - 80: --------
81 - 90: --------
91 - 99: --------


Example List
------------------------------------------------------------------------
ex01_helloworld         The IPC hello world example
ex02_messageq           Use MessageQ module between host and slave.
ex11_ping               All-to-all core MessageQ example
ex12_mmrpc              MmRpc example between host and slave
ex13_notifypeer         Add peer using only notify driver

ex31_fastboot           Improvements for a fast boot time.
ex32_footprint          Improvements for a minimal code and data footprint.
ex33_umsg               Unidirectional message library.
ex34_radar              Example to illustrate umsg usage.
ex35_perf               Add performance instrumentation to ex34_radar example.

ex41_forwardmsg         Dual transport example that forwards received host
                        message to another slave core
ex42_radio              Radio receiver running on dsp. With UIA config.
ex43_server             Slave runs a server with application terminate support.
ex44_compute            Foundational architecture for processing node network.
ex45_host               Host-only IPC communiation.
ex46_graph              Interactive management of data-flow graph.
ex54_execgraph          Integrate the SA Execution Graph into executable

ex61_dspmmu             Build dsp executable with a virtual memory map.
ex62_videoammu          Build video executable with a virtual memory map.
ex63_vpssammu           Build vpss executable with a virtual memory map.
ex64_metal              Load and run a non-bios executable.
ex65_overlay            Use section overlays to run code from internal memory.
ex66_overlaydma         Copy sections to run address using dma.
ex67_buffer             Example that shows how to send buffers allocated by
                        a custom allocator using MessageQ
ex68_power              MessageQ example with power management enabled

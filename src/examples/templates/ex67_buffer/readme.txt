#
#  ======== readme.txt ========
#
buffer - Send buffer allocated with a custom allocator from client to
server and back


Overview
=========================================================================
This is a MessageQ-based example using the client/server pattern. It is a two
processor example, where a host processor communicates with a slave processor,
by sending pointers to buffers allocated using a custom allocation mechanism
% if (this.platform.match(/^DRA7XX_qnx_elf$/)) {
(shmemallocator) in the message payload.
% } else  {
in the message payload.
% }

The slave processor is the server. It will create a named message queue.
The server does not open any queues because it will extract the return
address from the message header. The server returns all messages to the
sender after inspecting and modifying the buffer. It does not access
the message pool.

The HOST processor is the client application. The client creates a named
message queue. The client also creates and manages the message
pool and the buffer pool. The client's return address, the physical address
of an initialized data buffer and its size are set in the message header for
each message before sending it to the server. The client then waits for a reply
message from the server. When it receives the reply it checks the data buffer
for the expected payload before sending another message down to the server.

Messages are sent from host to slave and back 14 times. Then the host
issues a shutdown message to the slave. The slave returns the message,
shuts itself down and reinitializes itself for future runs.


Build Instructions
=========================================================================

 1. Create a work folder on your file system.

    mkdir work

 2. Extract this example into your work folder.

    cd work
    unzip <...>/ex67_buffer.zip

 3. Setup the build environment. Edit products.mak and set the install paths
    as defined by your physical development area. Each example has its own
    products.mak file; you may also create/reuse a products.mak file in the
    parent directory which will be used by all examples.

    edit ex67_buffer/products.mak

    IPC_INSTALL_DIR  = <...>/ipc_m_mm_pp_bb
    BIOS_INSTALL_DIR = <...>/bios_m_mm_pp_bb
    XDC_INSTALL_DIR  = <...>/xdctools_m_mm_pp_bb

    The compilers ship with CCS. You can use them to build this example,
    or download them separately from TI.

    edit ex67_buffer/products.mak

    DEPOT = <...>/ccsv6/tools/compiler

    gnu.targets.arm.A15F           = $(DEPOT)/gcc_arm_none_eabi_m_m_p
    ti.targets.elf.C66             = $(DEPOT)/c6000_m_m_p
    ti.targets.arm.elf.M4          = $(DEPOT)/arm_m_m_p

 4. Build the example. This will build only debug versions of the executables.
    Edit the lower makefiles and uncomment the release goals to build both
    debug and release executables.

    cd ex67_buffer
    make

    By default, the example builds for host and dsp1. Look in the following
    folders for the executables.

    ex67_buffer/host/bin/debug/app_host.xa15fg
    ex67_buffer/dsp1/bin/debug/server_dsp1.xe66

 5. Issue the following commands to clean your example.

    cd ex67_buffer
    make clean


Build Configuration
=========================================================================
When building, you can choose which processors you wish to build for. Use these
instructions to specify which processor to build and to configure the host to
use the correct server.

 1. Specify which two processors to build. Edit the top-level makefile and
    modify the PROCLIST macro to specify which processors to build. The
    list of available processors in defined in the ALL macro. However,
    the client must always run on the HOST. For example, follow these
    steps to build for dsp1.

    edit ex67_buffer/makefile

    PROCLIST = dsp1 host

 2. Clean and build your example.

    cd ex67_buffer
    make clean
    make

% if (this.platform.match(/^DRA7XX_qnx_elf$/)) {
Run instructions
=========================================================================
 1. Launch memory allocator
    shmemallocator

 2. Launch IPC for the core of your choice, e.g. DSP1
    ipc DSP1 ex67_buffer/debug/server_dsp1.xe66

 3. Launch the application example to communicate with the core
    ex67_buffer/debug/app_host DSP1
% }

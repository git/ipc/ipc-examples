/*----------------------------------------------------------------------------*/
/* R5FLink.cmd                                                             */
/*                                                                            */
/* (c) Texas Instruments 2017-2018, All rights reserved.                           */
/*                                                                            */
%%{
    var platform = this.arguments[0];
    var core = this.arguments[1];
    var target = this.arguments[3];
    var lcCore = core.toLowerCase();
%%}

/* USER CODE BEGIN (0) */
/* USER CODE END */

/*----------------------------------------------------------------------------*/
/* Linker Settings                                                            */
--retain="*(.intvecs)"
--retain="*(.intvecsNew)"
/*  Memory Map
 *
 *  Virtual     Physical        Size            Comment
 *  ------------------------------------------------------------------------
 *  0000_0000   8000_0000   200_0000  (  32 MB) HOST (code, data)
 *              8200_0000    80_0000  (   8 MB) R5F-0 (code, data)
 *              8280_0000    80_0000  (   8 MB) R5F-1 (code, data)
 *              8300_0000   100_0000  (  16 MB) SR_0 (ipc)
 *              8400_0000   100_0000  (  16 MB) --------
 *
 */

MEMORY
{
    SR_0       (RW)  : ORIGIN =  0x83000000, LENGTH = 0x01000000
}

#define ATCM_START 0x00000000

-e __VECS_ENTRY_POINT

MEMORY{
    ATCM       (RWX) : origin=ATCM_START, length=0x00008000
    BTCM       (RWX) : origin=0x41010000, length=0x00008000
    RAM0       (RW)  : origin=0x41C00000, length=0x00080000
    MSMC_SRAM  (RWX) : origin=0x70000000, length=0x00200000
}

MEMORY{
% if (platform.match(/^AM65XX_bios_elf$/)
%       && target.match(/\.R5F$/)) {
% if (lcCore.match(/r5f-0/)) {
    EXT_R5F   (RW)  : origin=0x82000000, length=0x00800000
% } else if (lcCore.match(/r5f-1/)) {
    EXT_R5F   (RW)  : origin=0x82800000, length=0x00800000
% }
% }
}

/*----------------------------------------------------------------------------*/
/* Section Configuration                                                      */
SECTIONS{
    .vecs             : {
        __VECS_ENTRY_POINT = .;
    } > ATCM_START

    .init_text        : {
                          boot.*(.text)
                          *(.text:ti_sysbios_family_arm_MPU_*)
                          *(.text:ti_sysbios_family_arm_v7r_Cache_*)
                          *(.text:xdc_runtime_Startup*)
                        }  > ATCM

    .text    align(8) : {} > EXT_R5F
    .const   align(8) : {} > EXT_R5F
    .cinit   align(8) : {} > EXT_R5F
    .pinit   align(8) : {} > EXT_R5F
    .bss              : {} > EXT_R5F
    .data             : {} > EXT_R5F
    .sysmem           : {} > EXT_R5F
    .args             : {} > EXT_R5F
    .stack            : {} > EXT_R5F
    ti_sdo_ipc_init: load > EXT_R5F, type = NOINIT

}
/*----------------------------------------------------------------------------*/

sr0_base = START(SR_0);
sr0_len = END(SR_0)-START(SR_0);

/*
 * Copyright (c) 2012-2018 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
%%{
    var platform = this.arguments[0];
    var core = this.arguments[1];
    var suffix = this.arguments[2];
    var target = this.arguments[3];
    var procName = core.toUpperCase();
    var lcCore = core.toLowerCase();
%%}

/*
 *  ======== `core`.cfg ========
 *  Platform: `platform`
 *  Target: `target`
 */

/* root of the configuration object model */
var Program = xdc.useModule('xdc.cfg.Program');
var cfgArgs = Program.build.cfgArgs;
var RB = (cfgArgs.profile == "release" ? true : false);

% if (platform.match(/^(DRA7XX|AM572X|AM571X)_bios_elf$/) && target.match(/\.M4$/)) {
/* build arguments for this executable */
var configBld = xdc.loadCapsule(cfgArgs.configBld);

% }
/* application uses the following modules and packages */
xdc.useModule('xdc.runtime.Assert');
xdc.useModule('xdc.runtime.Diags');
xdc.useModule('xdc.runtime.Error');
xdc.useModule('xdc.runtime.Log');
xdc.useModule('xdc.runtime.Registry');

xdc.useModule('ti.sdo.utils.MultiProc');

xdc.useModule('ti.sysbios.knl.Task');

/*
 *  ======== IPC Configuration ========
 */
% if (platform.match(/(DRA7XX|AM572X|AM571X|TCI6636|66AK2E|TCI6630|66AK2G|AM65XX)/)) {
xdc.global.SR0_cacheEnable = false;
% } else {
%     throw new Error("unsupported platform: " + platform);
% }
xdc.global.procName = "`procName`";
var ipc_cfg = xdc.loadCapsule("../shared/ipc.cfg.xs");
% if (platform.match(/(DRA7XX|AM572X|AM571X|TCI6636|66AK2E|TCI6630|66AK2G)/) && target.match(/\.C66$/)) {

/* make SR_0 non-cacheable */
var Cache = xdc.useModule('ti.sysbios.family.c66.Cache');
Cache.setMarMeta(ipc_cfg.SR0Mem.base, ipc_cfg.SR0Mem.len, Cache.Mar_DISABLE);
% }
% if (platform.match(/^(DRA7XX|AM572X|AM571X)_bios_elf$/)
%       && lcCore.match(/ipu[12]/)) {

var Core = xdc.useModule('ti.sysbios.family.arm.ducati.Core');
%     if (core.match(/ipu1/i)) {
Core.ipuId = 1;
%     } else {
Core.ipuId = 2;
%     }
% }

/* select ipc libraries */
var Build = xdc.useModule('ti.sdo.ipc.Build');
Build.libType = (RB ? Build.LibType_NonInstrumented : Build.LibType_Debug);
Build.assertsEnabled = (RB ? false : true);
Build.logsEnabled = (RB ? false : true);

% if (platform.match(/^(DRA7XX|AM572X|AM571X)_bios_elf$/)
%       && target.match(/\.M4$/)) {
% if (lcCore.match(/ipu1(?!-1)|ipu2/)) {
/*
 *  ======== IPU/CORE0 Processor (primary core) ========
 */
% if (lcCore == "ipu1-0") {

/* must match hardware core id */
var Core = xdc.useModule('ti.sysbios.family.arm.ducati.Core');
Core.id = 0;
% }

/* enable the unicache */
var Cache = xdc.useModule('ti.sysbios.hal.unicache.Cache');
Cache.enableCache = true;

/* configure ammu (because unicache is enabled) */
var AMMU = xdc.useModule('ti.sysbios.hal.ammu.AMMU');

/* increase small page 0 to 16 KB */
var entry = AMMU.smallPages[0];
entry.pageEnabled = AMMU.Enable_YES;
entry.translationEnabled = AMMU.Enable_YES;
entry.logicalAddress = 0x00000000;
entry.translatedAddress = 0x55020000;
entry.size = AMMU.Small_16K;
entry.L1_cacheable = AMMU.CachePolicy_NON_CACHEABLE;

/* increase small page 1 to 16 KB to access timers (non-cacheable) */
var entry = AMMU.smallPages[1];
entry.pageEnabled = AMMU.Enable_YES;
entry.translationEnabled = AMMU.Enable_YES;
entry.logicalAddress = 0x40000000;
entry.translatedAddress = 0x55080000;
entry.size = AMMU.Small_16K;
entry.L1_cacheable = AMMU.CachePolicy_NON_CACHEABLE;
entry.volatileQualifier = AMMU.Volatile_FOLLOW;
entry.executeOnly = AMMU.Enable_NO;
entry.readOnly = AMMU.Enable_NO;
entry.prefetch = AMMU.Enable_NO;
entry.exclusion = AMMU.Enable_YES;
entry.L1_cacheable = AMMU.CachePolicy_NON_CACHEABLE;
entry.L1_posted = AMMU.PostedPolicy_NON_POSTED;
entry.L1_allocate = AMMU.AllocatePolicy_NON_ALLOCATE;
entry.L1_writePolicy = AMMU.WritePolicy_WRITE_THROUGH;

/* map program code/data memory into ammu (cacheable) */
var entry = AMMU.largePages[0];
entry.pageEnabled = AMMU.Enable_YES;
entry.translationEnabled = AMMU.Enable_NO;
entry.logicalAddress = configBld.`lcCore.replace(/-/,"_")`_ammu.prog.pa;
entry.size = AMMU[configBld.`lcCore.replace(/-/,"_")`_ammu.prog.size];
entry.L1_cacheable = AMMU[configBld.`lcCore.replace(/-/,"_")`_ammu.prog.cache];

/* map SR_0 ammu (non-cacheable) */
var entry = AMMU.largePages[1];
entry.pageEnabled = AMMU.Enable_YES;
entry.translationEnabled = AMMU.Enable_NO;
entry.logicalAddress = configBld.`lcCore.replace(/-/,"_")`_ammu.sr0.pa;
entry.size = AMMU[configBld.`lcCore.replace(/-/,"_")`_ammu.sr0.size];
entry.L1_cacheable = AMMU[configBld.`lcCore.replace(/-/,"_")`_ammu.sr0.cache];

/* map L3/L4 peripherals (mailbox, spinlock) into ammu (non-cacheable) */
var entry = AMMU.largePages[3];
entry.pageEnabled = AMMU.Enable_YES;
entry.translationEnabled = AMMU.Enable_YES;
entry.logicalAddress = 0x60000000;
entry.translatedAddress = 0x40000000;
entry.size = AMMU.Large_512M;
entry.L1_cacheable = AMMU.CachePolicy_NON_CACHEABLE;
entry.volatileQualifier = AMMU.Volatile_FOLLOW;
entry.executeOnly = AMMU.Enable_NO;
entry.readOnly = AMMU.Enable_NO;
entry.prefetch = AMMU.Enable_NO;
entry.exclusion = AMMU.Enable_YES;
entry.L1_cacheable = AMMU.CachePolicy_NON_CACHEABLE;
entry.L1_posted = AMMU.PostedPolicy_NON_POSTED;
entry.L1_allocate = AMMU.AllocatePolicy_NON_ALLOCATE;
entry.L1_writePolicy = AMMU.WritePolicy_WRITE_THROUGH;

% } else {
/*
 *  ======== IPU/CORE1 Processor (slave core) ========
 */

/* must match hardware core id */
var Core = xdc.useModule('ti.sysbios.family.arm.ducati.Core');
Core.id = 1;

% }
/* configure the interrupt cross-bar mmr base address */
var IntXbar = xdc.useModule('ti.sysbios.family.shared.vayu.IntXbar');
IntXbar.mmrBaseAddr = 0x6A002000;

/* configure hardware spin lock base address to match ammu mapping of L3/L4 */
var GateHWSpinlock = xdc.useModule('ti.sdo.ipc.gates.GateHWSpinlock');
GateHWSpinlock.baseAddr = 0x6A0F6800;

% }

% if (platform.match(/^AM65XX_bios_elf$/)
%       && target.match(/\.R5F$/)) {
%   if (lcCore.match(/r5f(?!-1)/)) {
/*
 *  ======== R5F/CORE0 Processor (primary core) ========
 */
%     if (lcCore == "r5f-0") {

/* must match hardware core id */
var Core = xdc.useModule('ti.sysbios.family.arm.v7r.keystone3.Core');
Core.id = 0;
%     }
%   } else {
/*
 *  ======== R5F/CORE1 Processor (slave core) ========
 */

/* must match hardware core id */
var Core = xdc.useModule('ti.sysbios.family.arm.v7r.keystone3.Core');
Core.id = 1;

%   }

var MPU = xdc.useModule('ti.sysbios.family.arm.MPU');
MPU.enableMPU = true;
MPU.enableBackgroundRegion = true;

var attrs = new MPU.RegionAttrs();
MPU.initRegionAttrsMeta(attrs);

attrs.enable = true;
attrs.bufferable = false;
attrs.cacheable = false;
attrs.shareable = true;
attrs.noExecute = true;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 0;
attrs.subregionDisableMask = 0;
MPU.setRegionMeta(0, 0x00000000, MPU.RegionSize_4G, attrs);

attrs.enable = true;
attrs.bufferable = true;
attrs.cacheable = true;
attrs.shareable = false;
attrs.noExecute = false;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 1;
attrs.subregionDisableMask = 0;
MPU.setRegionMeta(1, 0x00000000, MPU.RegionSize_32K, attrs);

attrs.enable = true;
attrs.bufferable = true;
attrs.cacheable = true;
attrs.shareable = false;
attrs.noExecute = false;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 1;
attrs.subregionDisableMask = 0;
MPU.setRegionMeta(2, 0x41000000, MPU.RegionSize_32K, attrs);

attrs.enable = true;
attrs.bufferable = true;
attrs.cacheable = true;
attrs.shareable = false;
attrs.noExecute = false;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 1;
attrs.subregionDisableMask = 0x0;
MPU.setRegionMeta(3, 0x41010000, MPU.RegionSize_32K, attrs);

attrs.enable = true;
attrs.bufferable = true;
attrs.cacheable = true;
attrs.shareable = false;
attrs.noExecute = false;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 1;
attrs.subregionDisableMask = 0;
MPU.setRegionMeta(4, 0x41C00000, MPU.RegionSize_512K, attrs);

attrs.enable = true;
attrs.bufferable = true;
attrs.cacheable = true;
attrs.shareable = false; /* NOTE: Setting it true will make it non-cacheable */
attrs.noExecute = false;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 1;
attrs.subregionDisableMask = 0;
MPU.setRegionMeta(5, 0x70000000, MPU.RegionSize_1M, attrs);


/* The following used for code/data */
attrs.enable = true;
attrs.bufferable = true;
/* TODO: Workaround: Currently cache operation not supported
    Need to be removed once BIOS supports cache operations */
attrs.cacheable = true;
attrs.shareable = false; /* NOTE: Setting it true will make it non-cacheable */
attrs.noExecute = false;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 1;
attrs.subregionDisableMask = 0;
%     if (lcCore == "r5f-0") {
MPU.setRegionMeta(6, 0x82000000, MPU.RegionSize_8M, attrs);
%     } else if (lcCore == "r5f-1") {
MPU.setRegionMeta(6, 0x82800000, MPU.RegionSize_8M, attrs);
%     }

attrs.enable = true;
attrs.bufferable = false;
attrs.cacheable = false;
attrs.shareable = false; /* NOTE: Setting it true will make it non-cacheable */
attrs.noExecute = true;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 1;
attrs.subregionDisableMask = 0;
MPU.setRegionMeta(7, 0x45880000, MPU.RegionSize_512K, attrs);

/* Used for Shared memory */
attrs.enable = true;
attrs.bufferable = true;
attrs.cacheable = true;
attrs.shareable = true;
attrs.noExecute = true;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 0;
attrs.subregionDisableMask = 0;

/* TODO: Currently disabling DDR and use MSMC for shared memory */
MPU.setRegionMeta(8, 0x83000000, MPU.RegionSize_16M, attrs);

attrs.enable = true;
attrs.bufferable = true;
attrs.cacheable = true;
attrs.shareable = true; /* NOTE: Setting it true will make it non-cacheable */
attrs.noExecute = true;
//attrs.accPerm = 1;          /* RW at PL1 */
attrs.accPerm = 0x3;          /* RW at PL1 & 2 */
attrs.tex = 0;
attrs.subregionDisableMask = 0;
//MPU.setRegionMeta(8, 0x70100000, MPU.RegionSize_1M, attrs);

/* configure hardware spin lock base address to match ammu mapping of L3/L4 */
var GateHWSpinlock = xdc.useModule('ti.sdo.ipc.gates.GateHWSpinlock');
/* TODO: Update HW spinlock base addr for K3 */
/* GateHWSpinlock.baseAddr = 0x6A0F6800; */

% }

/*
 *  ======== SYS/BIOS Configuration ========
 */

/* no rts heap */
Program.heap = 0;
Program.argSize = 100;  /* minimum size */
% if (lcCore.match(/eve/)) {
//Program.stack = 0x800; /* small stack needed to fit in DMEM */
% }
Program.stack = 0x1000;

/* create a default heap */
var HeapMem = xdc.useModule('ti.sysbios.heaps.HeapMem');
var heapMemParams = new HeapMem.Params();
% if (lcCore.match(/eve/)) {
//heapMemParams.size = 0x1400;  /* small heap needed to fit in DMEM */
% }
heapMemParams.size = 0x8000;

var Memory = xdc.useModule('xdc.runtime.Memory');
Memory.defaultHeapInstance = HeapMem.create(heapMemParams);

% if (platform.match(/^(DRA7XX|AM572X|AM571X|TCI6636|66AK2E|TCI6630|66AK2G)_bios_elf$/)) {
%     if (lcCore.match(/ipu1$|ipu2/)) {
/* configure SMP-enabled System module */
var SysMin = xdc.useModule('ti.sysbios.smp.SysMin');
%     } else {
/* configure System module */
var SysMin = xdc.useModule('xdc.runtime.SysMin');
%     }
%     if (lcCore.match(/eve/)) {
//SysMin.bufSize = 0x200;  /* small buffer needed to fit in DMEM */
%     }
% }
% else if (platform.match(/^AM65XX_bios_elf$/)) {
/* configure System module */
var SysMin = xdc.useModule('xdc.runtime.SysMin');
% }
% else {
%     throw new Error("unsupported platform: " + platform);
% }
SysMin.bufSize = 0x1000;
SysMin.flushAtExit = false;

var System = xdc.useModule('xdc.runtime.System');
System.SupportProxy = SysMin;
% if (platform.match(/^DRA7XX_bios_elf$/)
%       && lcCore.match(/eve/)) {

/* place text character table in external memory, too big for DMEM */
var Text_charTab = new prog.SectionSpec();
Text_charTab.loadSegment = "`procName`_PROG";
Program.sectMap[".const:xdc_runtime_Text_charTab__A"] = Text_charTab;

/* reduce size of idle stack, too big for DMEM */
var Task = xdc.useModule('ti.sysbios.knl.Task');
Task.idleTaskStackSize = 0x200;
% }
% if (platform.match(/^(DRA7XX|AM572X|AM571X)_bios_elf$/)
%       && lcCore.match(/dsp/)) {

/* Turn off the timer frequency check. The DSP timer does not run when
 * the host is halted because of the emulation suspend signal.
 */
var Timer = xdc.useModule('ti.sysbios.timers.dmtimer.Timer');
Timer.checkFrequency = false;
% }
% if (platform.match(/^(DRA7XX|AM572X|AM571X)_bios_elf$/)
%       && lcCore.match(/dsp/)) {
%     if (lcCore == "dsp1") {

/* allocate timers 5 and 6 to DSP1 */
var TimerSupport = xdc.useModule('ti.sysbios.family.shared.vayu.TimerSupport');
TimerSupport.availMask = 0x0030;
%     } else {

/* allocate timers 7 and 8 to DSP2 */
var TimerSupport = xdc.useModule('ti.sysbios.family.shared.vayu.TimerSupport');
TimerSupport.availMask = 0x00C0;
%     }
% }

var BIOS = xdc.useModule('ti.sysbios.BIOS');
BIOS.libType = (RB ? BIOS.LibType_NonInstrumented : BIOS.LibType_Instrumented);
// BIOS.libType = BIOS.LibType_Custom;
// BIOS.libType = BIOS.LibType_Debug;
% if (platform.match(/^(DRA7XX|AM572X|AM571X)_bios_elf$/)
%       && target.match(/\.M4$/)) {
%     if (lcCore.match(/ipu1-[01]/)) {

BIOS.smpEnabled = false;
%     } else {

BIOS.smpEnabled = true;
%     }
% }

%if (platform.match(/^AM65XX_bios_elf$/)) {

var Clock = xdc.useModule('ti.sysbios.knl.Clock');
% if (lcCore == "r5f-0") {
Clock.timerId = 0;
% } else {
Clock.timerId = 1;
% }
var Timer = xdc.module('ti.sysbios.timers.dmtimer.Timer');
Timer.checkFrequency = false; /* Disable frequency check */
for (var i = 0; i < 4; i++) {
    Timer.intFreqs[i].lo = 20000000; /* Set a high Timer frequency value
                                        as default may be too small and
                                        cause frquenct interrupts. */
    Timer.intFreqs[i].hi = 0;
}
% }
/*
 *  ======== Miscellaneous Configuration ========
 */

/* set default diags mask */
var Diags = xdc.useModule('xdc.runtime.Diags');
var Defaults = xdc.useModule('xdc.runtime.Defaults');

Defaults.common$.diags_ENTRY     = Diags.ALWAYS_OFF;
Defaults.common$.diags_EXIT      = Diags.ALWAYS_OFF;
Defaults.common$.diags_LIFECYCLE = Diags.ALWAYS_OFF;
Defaults.common$.diags_INTERNAL  = (RB ? Diags.ALWAYS_OFF : Diags.ALWAYS_ON);
Defaults.common$.diags_ASSERT    = (RB ? Diags.ALWAYS_OFF : Diags.ALWAYS_ON);
Defaults.common$.diags_STATUS    = Diags.RUNTIME_ON;
Defaults.common$.diags_USER1     = Diags.ALWAYS_OFF;
Defaults.common$.diags_USER2     = Diags.ALWAYS_OFF;
Defaults.common$.diags_USER3     = Diags.ALWAYS_OFF;
Defaults.common$.diags_USER4     = Diags.ALWAYS_OFF;
Defaults.common$.diags_USER5     = Diags.ALWAYS_OFF;
Defaults.common$.diags_USER6     = Diags.ALWAYS_OFF;
Defaults.common$.diags_INFO      = Diags.ALWAYS_OFF;
Defaults.common$.diags_ANALYSIS  = Diags.ALWAYS_OFF;

/* override diags mask for selected modules */
xdc.useModule('xdc.runtime.Main');
Diags.setMaskMeta(
    "xdc.runtime.Main",
    Diags.ENTRY | Diags.EXIT | Diags.INFO,
    Diags.RUNTIME_ON
);

var Registry = xdc.useModule('xdc.runtime.Registry');
Registry.common$.diags_ENTRY    = Diags.RUNTIME_OFF;
Registry.common$.diags_EXIT     = Diags.RUNTIME_OFF;
Registry.common$.diags_INFO     = Diags.RUNTIME_OFF;
Registry.common$.diags_ANALYSIS = Diags.RUNTIME_OFF;
Registry.common$.diags_USER1    = Diags.RUNTIME_OFF;

% if (platform.match(/^(DRA7XX|AM572X|AM571X|TCI6636|66AK2E|TCI6630|66AK2G)_bios_elf$/)) {
%     if (lcCore.match(/ipu1$|ipu2/)) {
/* create a SMP-enabled logger instance */
var LoggerBuf = xdc.useModule('ti.sysbios.smp.LoggerBuf');
%     } else {
/* create a logger instance */
var LoggerBuf = xdc.useModule('xdc.runtime.LoggerBuf');
%     }
% } else {
/* create a logger instance */
var LoggerBuf = xdc.useModule('xdc.runtime.LoggerBuf');
% }


var loggerBufP = new LoggerBuf.Params();
loggerBufP.numEntries = 256;  /* 256 entries = 8 KB of memory */
loggerBufP.bufType = LoggerBuf.BufType_FIXED;

var appLogger = LoggerBuf.create(loggerBufP);
appLogger.instance.name = "AppLog";
Defaults.common$.logger = appLogger;

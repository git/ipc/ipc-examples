/*
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== config.bld ========
 *
 */
var Build = xdc.useModule('xdc.bld.BuildEnvironment');

/*  Memory Map
 *
 *  Virtual     Physical        Size            Comment
 *  ------------------------------------------------------------------------
 *  0000_0000   8000_0000       1000  (   4 KB) --------
 *              8000_1000    3F_F000  (  ~4 MB) --------
 *  0000_0000   8040_0000       1000  (   4 KB) --------
 *              8040_1000    3F_F000  (  ~4 MB) --------
 *  0000_0000   8080_0000       1000  (   4 KB) --------
 *              8080_1000    3F_F000  (  ~4 MB) --------
 *  0000_0000   80C0_0000       1000  (   4 KB) --------
 *              80C0_1000    3F_F000  (  ~4 MB) --------
 *              8100_0000   100_0000  (  16 MB) --------
 *              8200_0000   100_0000  (  16 MB) --------
 *              8300_0000   100_0000  (  16 MB) --------
 *              8400_0000   100_0000  (  16 MB) --------
 *              8500_0000   100_0000  (  16 MB) --------
 *              8600_0000   100_0000  (  16 MB) IPU1 (code, data)       [1]
 *              8600_0000    80_0000  (   8 MB) IPU1-0 (code, data)     [1]
 *              8680_0000    80_0000  (   8 MB) IPI1-1 (code, data)     [1]
 *              8700_0000   100_0000  (  16 MB) --------
 *              8800_0000   100_0000  (  16 MB) --------
 *              8900_0000   100_0000  (  16 MB) --------
 *              8A00_0000   100_0000  (  16 MB) IPU2 (code, data)       [2]
 *              8B00_0000   100_0000  (  16 MB) HOST (code, data)
 *              8C00_0000   100_0000  (  16 MB) DSP1 (code, data)
 *              8D00_0000   100_0000  (  16 MB) --------
 *              8E00_0000   100_0000  (  16 MB) SR_0 (ipc)
 *              8F00_0000   100_0000  (  16 MB) --------
 *
 *  Notes
 *  ------------------------------------------------------------------------
 *  [1] IPU1 implies SMP mode; IPU1-0, IPU1-1 implies non-SMP mode. You
 *  cannot use both at the same time.
 *
 *  [2] IPU2 implies SMP mode.
 */

var SR_0 = {
    name: "SR_0", space: "data", access: "RW",
    base: 0x8E000000, len: 0x1000000,
    comment: "SR#0 Memory (16 MB)"
};

Build.platformTable["ti.platforms.evmAM571X:dsp1"] = {
    externalMemoryMap: [
        [ "DSP1_PROG", {
            name: "DSP1_PROG", space: "code/data", access: "RWX",
            base: 0x8C000000, len: 0x1000000,
            comment: "DSP1 Program Memory (16 MB)"
        }],
        [ "SR_0", SR_0 ]
    ],
    codeMemory:  "DSP1_PROG",
    dataMemory:  "DSP1_PROG",
    stackMemory: "DSP1_PROG",
    l1DMode: "32k",
    l1PMode: "32k",
    l2Mode: "128k"
};

Build.platformTable["ti.platforms.evmAM571X:ipu1"] = {
    externalMemoryMap: [
        [ "IPU1_PROG", {
            name: "IPU1_PROG", space: "code/data", access: "RWX",
            base: 0x86000000, len: 0x1000000,
            comment: "IPU1 Program Memory (16 MB)"
        }],
        [ "SR_0", SR_0 ]
    ],
    codeMemory:  "IPU1_PROG",
    dataMemory:  "IPU1_PROG",
    stackMemory: "IPU1_PROG"
};

var ipu1_ammu = {
    prog: { /* program memory */
        pa: 0x86000000,
        size: "Large_32M",
        cache: "CachePolicy_CACHEABLE"
    },
    sr0: { /* SR_0 data memory (non-cacheable) */
        pa: 0x8E000000,
        size: "Large_32M",
        cache: "CachePolicy_NON_CACHEABLE"
    }
};

Build.platformTable["ti.platforms.evmAM571X:ipu1-0"] = {
    externalMemoryMap: [
        [ "IPU1_0_PROG", {
            name: "IPU1_0_PROG", space: "code/data", access: "RWX",
            base: 0x86000000, len: 0x800000,
            comment: "IPU1-0 Program Memory (8 MB)"
        }],
        [ "SR_0", SR_0 ]
    ],
    codeMemory:  "IPU1_0_PROG",
    dataMemory:  "IPU1_0_PROG",
    stackMemory: "IPU1_0_PROG"
};

var ipu1_0_ammu = {
    prog: { /* program memory */
        pa: 0x86000000,
        size: "Large_32M",
        cache: "CachePolicy_CACHEABLE"
    },
    sr0: { /* SR_0 data memory (non-cacheable) */
        pa: 0x8E000000,
        size: "Large_32M",
        cache: "CachePolicy_NON_CACHEABLE"
    }
};

Build.platformTable["ti.platforms.evmAM571X:ipu1-1"] = {
    externalMemoryMap: [
        [ "IPU1_1_PROG", {
            name: "IPU1_1_PROG", space: "code/data", access: "RWX",
            base: 0x86800000, len: 0x800000,
            comment: "IPU1-1 Program Memory (8 MB)"
        }],
        [ "SR_0", SR_0 ]
    ],
    codeMemory:  "IPU1_1_PROG",
    dataMemory:  "IPU1_1_PROG",
    stackMemory: "IPU1_1_PROG"
};

Build.platformTable["ti.platforms.evmAM571X:ipu2"] = {
    externalMemoryMap: [
        [ "IPU2_PROG", {
            name: "IPU2_PROG", space: "code/data", access: "RWX",
            base: 0x8A000000, len: 0x1000000,
            comment: "IPU2 Program Memory (16 MB)"
        }],
        [ "SR_0", SR_0 ]
    ],
    codeMemory:  "IPU2_PROG",
    dataMemory:  "IPU2_PROG",
    stackMemory: "IPU2_PROG"
};

var ipu2_ammu = {
    prog: { /* program memory */
        pa: 0x8A000000,
        size: "Large_32M",
        cache: "CachePolicy_CACHEABLE"
    },
    sr0: { /* SR_0 data memory (non-cacheable) */
        pa: 0x8E000000,
        size: "Large_32M",
        cache: "CachePolicy_NON_CACHEABLE"
    }
};

Build.platformTable["ti.platforms.evmAM571X:host"] = {
    externalMemoryMap: [
        [ "HOST_PROG", {
            name: "HOST_PROG", space: "code/data", access: "RWX",
            base: 0x8B000000, len: 0x1000000,
            comment: "HOST Program Memory (16 MB)"
        }],
        [ "SR_0", SR_0 ]
    ],
    codeMemory:  "HOST_PROG",
    dataMemory:  "HOST_PROG",
    stackMemory: "HOST_PROG"
};

/*
 *  ======== ti.targets.elf.C66 ========
 */
var C66 = xdc.useModule('ti.targets.elf.C66');
C66.ccOpts.suffix += " -mi10 -mo -pdr -pden -pds=238 -pds=880 -pds1110 ";
Build.targets.$add(C66);

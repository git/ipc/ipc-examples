/*
 * Copyright (c) 2018 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <ti/sysbios/family/arm/v8a/Mmu.h>

Void InitMmu()
{
    Mmu_MapAttrs attrs;

    Mmu_initMapAttrs(&attrs);

//    attrs.attrIndx = Mmu_AttrIndx_MAIR0;
    attrs.attrIndx = 0;
    Mmu_map(0x01800000, 0x01800000, 0x00100000, &attrs); /* gicv3       */
    Mmu_map(0x02400000, 0x02400000, 0x000c0000, &attrs); /* dmtimer     */
    Mmu_map(0x02800000, 0x02800000, 0x00001000, &attrs); /* uart        */
    Mmu_map(0x2A430000, 0x2A430000, 0x00001000, &attrs); /* ctrcontrol0 */
    Mmu_map(0x30000000, 0x30000000, 0x02000000, &attrs); /* navss       */
    Mmu_map(0x32400000, 0x32400000, 0x00100000, &attrs); /* Secure proxy reg       */
    /* Secure proxy source target data reg       */
    Mmu_map(0x32C00000, 0x32C00000, 0x00100000, &attrs);

    attrs.attrIndx = 7;
    Mmu_map(0x80000000, 0x80000000, 0x03000000, &attrs); /* ddr         */
    Mmu_map(0x70000000, 0x70000000, 0x00200000, &attrs); /* msmc sram   */
    Mmu_map(0x83000000, 0x83000000, 0x04000000, &attrs); /* ddr         */

}

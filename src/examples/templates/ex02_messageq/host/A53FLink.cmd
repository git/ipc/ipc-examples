/*----------------------------------------------------------------------------*/
/* A53FLink.cmd                                                             */
/*                                                                            */
/* (c) Texas Instruments 2017-2018, All rights reserved.                           */
/*                                                                            */
/*  Memory Map
 *
 *  Virtual     Physical        Size            Comment
 *  ------------------------------------------------------------------------
 *  0000_0000   80000_0000   200_0000  (  32 MB) HOST (code, data)
 *              80200_0000    80_0000  (   8 MB) R5F-0 (code, data)
 *              80280_0000    80_0000  (   8 MB) R5F-1 (code, data)
 *              80300_0000   100_0000  (  16 MB) SR_0 (ipc)
 *              80300_0000   100_0000  (  16 MB) --------
 *
 */


MEMORY
{
    PSRAM0_RAM  (RX) : ORIGIN =  0x00000000, LENGTH = 0x00000400
/*    EXT_CODE   (RW)  : ORIGIN =  0x800000000, LENGTH = 0x00C00000
    EXTDATA    (RW)  : ORIGIN =  0x800C00000, LENGTH = 0x01000000
    EXT_HEAP   (RW)  : ORIGIN =  0x801C00000, LENGTH = 0x00400000
*/
    EXT_CODE   (RW)  : ORIGIN =  0x80000000, LENGTH = 0x00C00000
    EXTDATA    (RW)  : ORIGIN =  0x80C00000, LENGTH = 0x01000000
    EXT_HEAP   (RW)  : ORIGIN =  0x81C00000, LENGTH = 0x00400000

}

MEMORY
{
    SR_0       (RW)  : ORIGIN =  0x83000000, LENGTH = 0x01000000
}

/*----------------------------------------------------------------------------*/
/* Section Configuration                                                      */
SECTIONS{
/*    .vecs             : {} > VECTORS */

    .c_int00 : {
        *(.c_int00)
    } > EXT_CODE AT> EXT_CODE
    .stack (NOLOAD) : ALIGN(16) {
        _stack = .;
        __stack = .;
        KEEP(*(.stack))
    } > EXTDATA AT> EXTDATA
    .sr0 : ALIGN (4) {
        sr0_start = .;
	 . += 0x01000000;
        . = ALIGN (4);
        sr0_end = .;
    } > SR_0 AT> SR_0
   .text : {
        CREATE_OBJECT_SYMBOLS
        *(.text)
        *(.text.*)
        . = ALIGN(0x4);
        KEEP (*(.ctors))
        . = ALIGN(0x4);
        KEEP (*(.dtors))
        . = ALIGN(0x4);
        __init_array_start = .;
        KEEP (*(.init_array*))
        __init_array_end = .;
        *(.init)
        *(.fini*)
    } > EXT_CODE AT> EXT_CODE

    PROVIDE (__etext = .);
    PROVIDE (_etext = .);
    PROVIDE (etext = .);

    .rodata : {
        *(.rodata)
        *(.rodata*)
    } > EXT_CODE AT> EXT_CODE


   .data : ALIGN (4) {
        __data_load__ = LOADADDR (.data);
        __data_start__ = .;
        *(.data)
        *(.data*)
        . = ALIGN (4);
       *(.ti_sysbios_family_arm_v8a_Mmu_tableArray)
        __data_end__ = .;
    } > EXTDATA AT> EXTDATA
    .bss : {
        __bss_start__ = .;
        *(.shbss)
        *(.bss)
        *(.bss.*)
        *(COMMON)
        . = ALIGN (4);
        __bss_end__ = .;
    } > EXTDATA AT> EXTDATA

    .heap : {
        __heap_start__ = .;
        end = __heap_start__;
        _end = end;
        __end = end;
        KEEP(*(.heap))
        __heap_end__ = .;
        __HeapLimit = __heap_end__;
    } > EXT_HEAP AT> EXT_HEAP

    .note.gnu.build-id : {} >  EXT_CODE

}
/*----------------------------------------------------------------------------*/
sr0_base = ORIGIN(SR_0);
sr0_len = LENGTH(SR_0);

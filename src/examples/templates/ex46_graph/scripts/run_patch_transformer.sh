#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Error: must specify cluster baseId"
    exit -1
fi

baseId=$1
prog=transformerN

set -x
perl patchExec.pl $baseId ${prog}.xe66 ${prog}_p.xe66

#!/usr/bin/perl

#
# Copyright (c) 2015, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# patchExec.pl
#
# This script patches a DSP ELF executable with a cluster base id,
# at the location with the magic binary pattern
#     74efdb7760e9bf8961d98b7368d8f5b73506064a
# in the executable's .cinit records.
#
# Command line arguments:
#     $ARGV[0]: cluster base id
#     $ARGV[1]: input executable
#     $ARGV[2]: path to patched output executable
#

use strict;
use warnings;

# Set the input and output file names
my $execFile = $ARGV[1];
my $patchedFile = $ARGV[2];

my $sections = 0;
my $skip = 0;
my $fileOff;
my $sizeCInit;
my $b;

my $usage = "Usage: perl patchExec.pl <cluster base id> <input executable>" .
 " <patched executable>\n\n";

# Verify arguments
die "Wrong number of command-line arguments.\n" . $usage
    unless (@ARGV == 3);
die "Invalid cluster base id.\n" . $usage unless ($ARGV[0] >= 0);
die "Cannot read from input file.\n" . $usage unless -r $ARGV[1];

# Parse the objdump
my @resultLines = split("\n", `objdump -h $ARGV[1]`);
die "Error when running objdump.\n" unless (@resultLines != 0);

foreach (@resultLines) {
    if (index($_, "Sections:") != -1) {      #Look for sections header
        $sections = 1;
    }
    elsif ($sections eq 1) {
        $skip = $skip + 1;
        if (($skip % 2) eq 0) {
            my @cols = split;                # split into columns
            if ($cols[1] eq ".cinit") {
                $fileOff = hex($cols[5]);    # file offset of .cinit
                $sizeCInit = hex($cols[2]);  # size of .cinit
            }
        }
    }
}

open(IN, "< $execFile") or die "Cannot open input file\n" . $usage;
binmode(IN);
open(OUT, "> $patchedFile") or die "Cannot open output file\n" . $usage;
binmode(OUT);

#skip over everything before .cinit
read(IN, $b, $fileOff);
print(OUT $b);

#
# Look for magic value of 9807fc203ee72e6857da95ecd619feed55f050e6
# in .cinit and modify it with base cluster id, We ignore the first
# and last byte in the magic value in case they have been encoded
# using RLE, and store the cluster id in the 3rd and 4th byte.
#
read(IN, $b, $sizeCInit);
my $hex = unpack('H*', $b);         #unpack binary into string
my $id = sprintf("%04x", $ARGV[0]); #convert id into 16-bit hex string

#convert cluster id to little endian
my @chars = reverse(split("", $id));
$id = reverse(@chars[0..1]) . reverse(@chars[2..3]);

my $newString = "07" . $id . "3ee72e6857da95ecd619feed55f050";
my $magic = "07fc203ee72e6857da95ecd619feed55f050";
$hex =~ s/$magic/$newString/g;
die "Magic pattern not found" unless scalar @+ != 0;
die "Too many patterns found" unless scalar @+ == 1;
my $out = pack('H*', $hex); #pack string into binary
print(OUT $out);

#copy the rest of the file
my $filesize = -s $execFile;
read(IN, $b, $filesize - $sizeCInit - $fileOff);
print(OUT $b);

close(IN);
close(OUT);

#TODO: remove temporary files

# Exit script
exit;

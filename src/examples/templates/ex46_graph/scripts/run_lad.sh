#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Error: must specify cluster baseId"
    exit -1
fi

baseId=$1

set -x
./lad_tci6638 -s PAIR -n 4608 -b $baseId -r 8 -l log.txt

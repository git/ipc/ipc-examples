/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Cmd.h ========
 */

#ifndef Cmd__include
#define Cmd__include

#include <sys/queue.h>
#include "../shared/SysCfg.h"

#if defined (__cplusplus)
extern "C" {
#endif

/* commands */
#define Cmd_CONNECT     0xC02   /* source node only, MessageQ_openQueueId   */
#define Cmd_DISCONNECT  0xC04   /* source node only, MessageQ_close         */
#define Cmd_EXIT        0xC05   /* manager only, exit program               */
#define Cmd_HELP        0xC06   /* command object only, list all commands   */
#define Cmd_INFO        0xC07
#define Cmd_LAUNCH      0xC08   /* manager only, attach host programs       */
#define Cmd_PAUSE       0xC09   /* producer only, stop data pump            */
#define Cmd_RUN         0xC0A   /* producer only, start data pump           */
#define Cmd_SHUTDOWN    0xC0B   /* one-way message to all nodes             */
#define Cmd_STATUS      0xC0C   /* deferred command, ping all nodes         */

/* arguments */
#define Cmd_BASEID      0xA01
#define Cmd_COMBINER    0xA02
#define Cmd_CONSUMER    0xA03
#define Cmd_CLUSTER     0xA04
#define Cmd_PRODUCER    0xA05
#define Cmd_START       0xA06
#define Cmd_STOP        0xA07
#define Cmd_TRANSFORMER 0xA08

#define Cmd_HOST        0       /* token value matches clusterId */
#define Cmd_DSP1        1
#define Cmd_DSP2        2
#define Cmd_DSP3        3
#define Cmd_DSP4        4
#define Cmd_DSP5        5
#define Cmd_DSP6        6
#define Cmd_DSP7        7
#define Cmd_DSP8        8

/* command object */
typedef struct Cmd_Command {
    int cmd;                    /* command token */
    int reply;                  /* send reply message */
    int deferredCount;          /* number of replies needed to complete */
    int status;                 /* command result status */

    union {
        struct connect {
            char source;        /* node name */
            char sink;          /* node name */
            void *progSource;   /* source node program context */
            void *progSink;     /* sink node program context */
        } connect;

        struct disconnect {
            char source;        /* node name */
            char sink;          /* node name */
            void *progSource;   /* deferred command */
            void *progSink;     /* deferred command */
        } disconnect;

        struct info {
            int item;
            int baseId;
            int size;
            char names[10][16];
        } info;

        struct launch {
            int program;
            int processor;      /* IPC cluster ID */
            char name[32];
        } launch;

        struct pause {
            char node;          /* must be a producer node */
            void *producer;     /* deferred command */
        } pause;

        struct run {
            char node;          /* must be a producer node */
        } run;

        struct shutdown {
            int program;        /* dsp only: IPC cluster ID */
            char node;          /* only for producer and consumer */
            void *msg;          /* message allocated in command prologue */
            void *prog;         /* program object */
        } shutdown;

    } args;

    LIST_ENTRY(Cmd_Command) link;
} Cmd_Command;

/* reserved message queue ID */
#define Cmd_MSGID SysCfg_Cmd_MSGID

int  Cmd_help(char *buf, int size, int seek);
void Cmd_parse(char *buf, Cmd_Command *cmd);


#if defined (__cplusplus)
}
#endif
#endif

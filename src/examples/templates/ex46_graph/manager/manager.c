/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== manager.c ========
 */

/* standard headers */
#include <fcntl.h>
#include <pthread.h>            /* pthread_create, pthread_join */
#include <stdio.h>              /* fopen, fclose, fileno, fprintf */
#include <unistd.h>             /* fcntl */

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>

/* example header files */
#include <ex45_host/transport/hlos/TransportPMQ.h>
#include "../shared/SysCfg.h"
#include "ClusterMgr.h"
#include "Console.h"

/* log helpers */
#define CMLOG "/tmp/cmlog.txt"
#define LOG fprintf(cmlog,
#define LOGF ); fflush(cmlog);
FILE *cmlog = NULL;


/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    int status = 0;
    pthread_t cmThr;            /* cluster manager */
    int cmArg;
    int cmStatus = 0;
    pthread_t conThr;           /* console */
    int conArg;
    int conStatus = 0;
    int fdlog;
    int flags;
    ITransport_Handle transport;
    INetworkTransport_Handle transNetwork;
    TransportPMQ_Handle transPMQ;

    /* open log file */
    cmlog = fopen(CMLOG, "w");

    if (cmlog == NULL) {
        fprintf(stderr, "Error: unable to open log file, %s\n", CMLOG);
        status = -1;
        goto leave;
    }

    /* set the close on exec flag for the log file */
    fdlog = fileno(cmlog);
    flags = fcntl(fdlog, F_GETFD);

    if (flags == -1) {
        fprintf(stderr, "Error: unable to read log flags\n");
        status = -1;
        goto leave;
    }

    status = fcntl(fdlog, F_SETFD, flags | FD_CLOEXEC);

    if (status == -1) {
        fprintf(stderr, "Error: unable to write log flags\n");
        status = -1;
        goto leave;
    }

    /* configure the transport factory */
    Ipc_transportConfig(&TransportRpmsg_Factory);

    /* initialize IPC */
    LOG "main: IPC init\n" LOGF
    status = Ipc_start();

    if (status < 0) {
        fprintf(stderr, "Error: IPC init failed, error=%d\n", status);
        goto leave;
    }

    /* setup the TransportPMQ module */
    LOG "main: TransportPMQ setup\n" LOGF
    status = TransportPMQ_setup();

    if (status < 0) {
        status = -1;
        fprintf(stderr, "Error: TransportPMQ setup failed\n");
        goto leave;
    }
    transPMQ = TransportPMQ_handle();

    /* register transport with MessageQ */
    transNetwork = TransportPMQ_upCast(transPMQ);
    transport = INetworkTransport_upCast(transNetwork);
    MessageQ_registerTransportId(SysCfg_TRANSPORT_PMQ_ID, transport);

    /* IPC is now ready */
    LOG "main: IPC ready, status=%d\n", status LOGF

    /* start the manager thread */
    status = pthread_create(&cmThr, NULL, &ClusterMgr_threadFxn, &cmArg);

    if (status < 0) {
        fprintf(stderr, "Error: cluster manager failed\n");
        goto leave;
    }

    /* start the console thread */
    status = pthread_create(&conThr, NULL, &Console_threadFxn, &conArg);

    if (status < 0) {
        fprintf(stderr, "Error: console failed\n");
        goto leave;
    }

    /* wait for manager thread to shutdown */
    pthread_join(cmThr, (void **)(&cmStatus));

    /* wait for console thread to shutdown */
    pthread_join(conThr, (void **)(&conStatus));

    status = ((cmStatus < 0) || (conStatus < 0)) ? -1 : 0;

    /* finalize the secondary transport */
    MessageQ_unregisterTransportId(SysCfg_TRANSPORT_PMQ_ID);
    TransportPMQ_destroy();

    /* finalize IPC */
    Ipc_stop();

leave:
    LOG "main: cmStatus=%d\n", cmStatus LOGF
    LOG "main: conStatus=%d\n", conStatus LOGF
    LOG "main: <-- status=%d\n", status LOGF
    fclose(cmlog);

    return (status);
}

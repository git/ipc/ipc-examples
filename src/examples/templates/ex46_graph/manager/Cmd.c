/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Cmd.c ========
 */

/* Standard headers */
#include <stdio.h>              /* printf, sscanf */
#include <string.h>             /* strcpy, strcmp, strlen */

/* example header files */
#include "Cmd.h"

#define FALSE 0
#define TRUE 1

/* log helpers */
extern FILE *cmlog;
#define LOG fprintf(cmlog,
#define LOGF ); fflush(cmlog);

/* parser data structures */
#define Lexeme_LEN 32

typedef struct {
    char lexeme[Lexeme_LEN];
    int value;
} Token;

typedef struct {
    Token *tab;
    int size;
} PTable;

/* private functions */
int TokenInt(PTable *table, char *word, int *tokerr);


/* commands */
Token Cmd_tokens[] = {
    {   "connect",      Cmd_CONNECT     },
    {   "disconnect",   Cmd_DISCONNECT  },
    {   "exit",         Cmd_EXIT        },
    {   "help",         Cmd_HELP        },
    {   "info",         Cmd_INFO        },
    {   "launch",       Cmd_LAUNCH      },
    {   "pause",        Cmd_PAUSE       },
    {   "run",          Cmd_RUN         },
    {   "shutdown",     Cmd_SHUTDOWN    },
    {   "status",       Cmd_STATUS      }
};
PTable CmdTab = {
    Cmd_tokens,
    sizeof(Cmd_tokens) / sizeof(Token)
};

/* cmd: info */
Token Info_tokens[] = {
    {   "baseId",       Cmd_BASEID      },
    {   "cluster",      Cmd_CLUSTER     }
};
PTable InfoTab = {
    Info_tokens,
    sizeof(Info_tokens) / sizeof(Token)
};

/* cmd: launch */
Token Launch_tokens[] = {
    { "combiner",       Cmd_COMBINER    },
    { "consumer",       Cmd_CONSUMER    },
    { "producer",       Cmd_PRODUCER    },
    { "transformer",    Cmd_TRANSFORMER }
};
PTable LaunchTab = {
    Launch_tokens,
    sizeof(Launch_tokens) / sizeof(Token)
};

/* cmd: shutdown */
Token Node_tokens[] = {
    { "dsp1",           Cmd_DSP1        },
    { "dsp2",           Cmd_DSP2        },
    { "dsp3",           Cmd_DSP3        },
    { "dsp4",           Cmd_DSP4        },
    { "dsp5",           Cmd_DSP5        },
    { "dsp6",           Cmd_DSP6        },
    { "dsp7",           Cmd_DSP7        },
    { "dsp8",           Cmd_DSP8        }
};
PTable NodeTab = {
    Node_tokens,
    sizeof(Node_tokens) / sizeof(Token)
};

/* processor names */
Token Processor_tokens[] = {
    { "host", Cmd_HOST },
    { "dsp1", Cmd_DSP1 },
    { "dsp2", Cmd_DSP2 },
    { "dsp3", Cmd_DSP3 },
    { "dsp4", Cmd_DSP4 },
    { "dsp5", Cmd_DSP5 },
    { "dsp6", Cmd_DSP6 },
    { "dsp7", Cmd_DSP7 },
    { "dsp8", Cmd_DSP8 }
};
PTable ProcessorTab = {
    Processor_tokens,
    sizeof(Processor_tokens) / sizeof(Token)
};

/* help text */
char Cmd_helpTextBuf[] =
    "NAME\n"
    "    connect - create a data path between two nodes\n"
    "    disconnect - delete a data path\n"
    "    exit - shutdown the cluster manager\n"
    "    help - print this message\n"
    "    info - display information about objects\n"
    "    launch - start a program\n"
    "    pause - stop data flow\n"
    "    run - start data flow\n"
    "    shutdown - terminate a program\n"
    "    status - display system state\n"
    "\n"
    "SYNOPSIS\n"
    "    connect sourceNode sinkNode\n"
    "    disconnect sourceNode sinkNode\n"
    "    exit\n"
    "    help\n"
    "    info baseId | cluster\n"
    "    launch producer|consumer\n"
    "    launch transformer|combiner dsp1|dsp2|...|dsp8\n"
    "    pause nodeX\n"
    "    run nodeX\n"
    "    shutdown nodeX\n"
    "    shutdown dsp1|dsp2|...|dsp8\n"
    "    status\n"
    "\n"
    "OPTIONS\n"
    "    nodeX - specify node name (e.g. 'A')\n"
    "    sourceNode, sinkNode - specify node name (e.g. 'A')\n"
    "\n"
;  /* statement terminator, don't delete */

int Cmd_helpTextLen = sizeof(Cmd_helpTextBuf);


/*
 *  ======== Cmd_help ========
 */
int  Cmd_help(char *buf, int size, int seek)
{
    char *cp;
    int nbytes;

    /* reserve space for string terminator */
    size -= 1;

    /* set current pointer into source buffer */
    cp = Cmd_helpTextBuf + seek;

    /* compute transfer size */
    nbytes = Cmd_helpTextLen - seek;
    nbytes = nbytes > size ? size : nbytes;

    /* copy help text to destination buffer, add string terminator */
    strncpy(buf, cp, nbytes);
    buf[nbytes] = '\0';

    return (nbytes);
}

/*
 *  ======== Cmd_parse ========
 */
void Cmd_parse(char *buf, Cmd_Command *cmd)
{
    int num;
    int tokerr = FALSE;
    char word[32];
    char arg1[32];
    char arg2[32];

    /* assume success */
    cmd->status = 0;

    /* extract first word */
    num = sscanf(buf, "%s", word);

    if (num == 0) {
        /* sscan error */
        cmd->cmd = -1;
        tokerr = TRUE;
        goto leave;
    }

    /* tokenize the command */
    cmd->cmd = TokenInt(&CmdTab, word, &tokerr);

    switch (cmd->cmd) {

        case Cmd_CONNECT:
        {
            struct connect *connect = &cmd->args.connect;
            num = sscanf(buf, "%*s %s %s", &arg1[0], &arg2[0]);
            if (num != 2) goto lexerr;
            connect->source = arg1[0];
            connect->sink = arg2[0];
        } break;

        case Cmd_DISCONNECT:
        {
            struct disconnect *disconnect = &cmd->args.disconnect;
            num = sscanf(buf, "%*s %s %s", &arg1[0], &arg2[0]);
            if (num != 2) goto lexerr;
            disconnect->source = arg1[0];
            disconnect->sink = arg2[0];
        } break;

        case Cmd_EXIT:
            break;

        case Cmd_HELP:
            break;

        case Cmd_INFO:
            num = sscanf(buf, "%*s %s", word);
            if (num != 1) goto lexerr;
            cmd->args.info.item = TokenInt(&InfoTab, word, &tokerr);
            break;

        case Cmd_LAUNCH:
        {
            struct launch *launch = &cmd->args.launch;
            num = sscanf(buf, "%*s %s", word);
            if (num != 1) goto lexerr;
            launch->program = TokenInt(&LaunchTab, word, &tokerr);
            if (!tokerr) {
                strcpy(launch->name, word);
            }
            if ((launch->program == Cmd_TRANSFORMER)
                || (launch->program == Cmd_COMBINER)) {
                num = sscanf(buf, "%*s %*s %s", word);
                if (num != 1) goto lexerr;
                launch->processor = TokenInt(&ProcessorTab, word, &tokerr);
            }
        } break;

        case Cmd_PAUSE:
        {
            struct pause *pause = &cmd->args.pause;
            char node;
            num = sscanf(buf, "%*s %c", &node);
            if (num != 1) goto lexerr;
            pause->node = node;
        } break;

        case Cmd_RUN:
        {
            struct run *run = &cmd->args.run;
            char node;
            num = sscanf(buf, "%*s %c", &node);
            if (num != 1) goto lexerr;
            run->node = node;
        } break;

        case Cmd_SHUTDOWN:
        {
            struct shutdown *shutdown = &cmd->args.shutdown;
            num = sscanf(buf, "%*s %s", word);
            if (num != 1) goto lexerr;

            if (strlen(word) == 1) {
                shutdown->node = word[0];
                shutdown->program = -1;
            }
            else {
                shutdown->program = TokenInt(&NodeTab, word, &tokerr);
                if (tokerr) goto lexerr;
            }
        } break;

        case Cmd_STATUS:
            break;

        default:
            printf("Cmd_parser: Error: command not implemented\n");
            break;
    }

    if ((cmd->cmd == -1) || tokerr) {
lexerr:
        printf("Error: invalid argument\n");
        cmd->cmd = -1;
        cmd->status = -1;
        goto leave;
    }

leave:
    return;
}

/*
 *  ======== TokenInt ========
 */
int TokenInt(PTable *table, char *word, int *tokerr)
{
    int i;

    /* if in error state, do nothing */
    if (*tokerr) {
        return (-1);
    }

    /* search parse table for given word */
    for (i = 0; i < table->size; i++) {
        if (strcmp(word, table->tab[i].lexeme) == 0) {
            return (table->tab[i].value);
        }
    }

    /* word not found, enter error state */
    *tokerr = TRUE;
    return (-1);
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== ClusterMgr.c ========
 */

/* Standard headers */
#include <assert.h>             /* assert */
#include <dirent.h>             /* opendir */
#include <errno.h>              /* errno, sys_errlist[] */
#include <stdio.h>              /* fprintf, sprintf */
#include <stdlib.h>             /* system, malloc, free, atoi */
#include <string.h>             /* strcpy, strdup, strtok */
#include <unistd.h>             /* usleep, fork */
#include <sys/queue.h>          /* LIST macros */
#include <sys/types.h>          /* opendir, wait, WEXITSTATUS */
#include <sys/wait.h>           /* wait, WEXITSTATUS */

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* example header files */
#include "../combiner/CombinerN.h"
#include "../consumer/Consumer.h"
#include "../transformer/TransformerN.h"
#include "../producer/Producer.h"
#include "../shared/ClusterMgr_shared.h"
#include "../shared/Graph.h"
#include "../shared/SysCfg.h"
#include "ClusterMgr.h"
#include "Cmd.h"

#define HEAPID          0
#define DSPCOUNT        8               /* Keystone 2 Hawking has 8 DSPs */
#define BUFSZ           64

#define PRODUCER_QUEUES         0x0E    /* reserved queues: 1, 2, 3 */
#define CONSUMER_QUEUES         0x70    /* reserved queues: 4, 5, 6 */
#define NUM_RESERVED_QUEUES     8       /* number of reserved queues */

#define SR_ZERO_OWNER   "CORE0"
#define MAXARGS 15

/* program type */
typedef enum {
    ClusterMgr_COMBINER,
    ClusterMgr_CONSUMER,
    ClusterMgr_PRODUCER,
    ClusterMgr_TRANSFORMER,
    ClusterMgr_UNKNOWN
} ProgType;

/* program context */
typedef struct ProgCtx {
    Char                node;           /* node name */
    Char                dstNode;        /* destination node */
    ProgType            type;           /* program type */
    pid_t               pid;            /* pid if Linux program */
    MessageQ_QueueId    cmdQue;         /* command message queue */
    MessageQ_QueueIndex dataQueIdx;     /* data queue index */
    UInt8               transId;        /* transport ID, manager to node */
    UInt16              procId;         /* processor ID */
    Bool                attached;       /* ipc attach state */
    Bool                dataOn;         /* data flow state (producer only) */
    LIST_ENTRY(ProgCtx) link;           /* list link */
} ProgCtx;

LIST_HEAD(ListHead, ProgCtx);
LIST_HEAD(DclHead, Cmd_Command);

/* module structure */
typedef struct {
    Char                naNode;         /* next available node */
    Bool                run;            /* main loop run flag */
    UInt8               prodQueMask;    /* producer available queues */
    UInt8               consQueMask;    /* consumer available queues */
    MessageQ_Handle     queue;          /* inbound messages */
    struct ListHead     consumer;       /* list of consumer programs */
    struct ListHead     producer;       /* list of producer programs */
    ProgCtx            *dsp[DSPCOUNT];  /* dsp context array */
    int                 dfrCmdReady;    /* deferred command ready count */
    struct DclHead      dfrCmdList;     /* deferred command list */
} ClusterMgr_Module;

/* private functions */
static Cmd_Command *ClusterMgr_completeCommand(Cmd_Command *cmd);
static void ClusterMgr_destroy(void);
static int ClusterMgr_exec(void);
static Cmd_Command *ClusterMgr_processCommand(Cmd_Command *cmd);
static int ClusterMgr_processGraph(Graph_Msg *msg);
static int ClusterMgr_setup(void);
static pid_t ClusterMgr_system(char *cmd, bool wait);

//static int Prog_dataState(ProgCtx *prog);
static ProgCtx *Prog_findNode(struct ListHead *head, Char node);
static ProgCtx *Prog_findAllNode(Char node);
static ProgCtx *Prog_findDspNode(Char node);

static Cmd_Command *Prog_findCmd(struct DclHead *head, UInt dcid);
static Cmd_Command *Prog_getReadyCmd(struct DclHead *head);

static Void List_insert_alpha(struct ListHead *list, ProgCtx *item);

/* command functions */
static Cmd_Command *doConnectCmd(Cmd_Command *cmd);
static Cmd_Command *doDisconnectCmd(Cmd_Command *cmd);
static Cmd_Command *doInfoCmd(Cmd_Command *cmd);
static Cmd_Command *doLaunchCmd(Cmd_Command *cmd);
static Cmd_Command *doPauseCmd(Cmd_Command *cmd);
static Cmd_Command *doRunCmd(Cmd_Command *cmd);
static Cmd_Command *doShutdownCmd(Cmd_Command *cmd);
static Cmd_Command *doStatusCmd(Cmd_Command *cmd);

static Cmd_Command *completeConnectCmd(Cmd_Command *cmd);
static Cmd_Command *completeDisconnectCmd(Cmd_Command *cmd);
static Cmd_Command *completePauseCmd(Cmd_Command *cmd);
static Cmd_Command *completeShutdownCmd(Cmd_Command *cmd);
static Cmd_Command *completeStatusCmd(Cmd_Command *cmd);

static void sendAttachCmd(ProgCtx *prog, UInt16 procId, UInt dcid);
static void sendConnectCmd(ProgCtx *progSource, ProgCtx *progSink);
static void sendDetachCmd(ProgCtx *prog, UInt16 procId, UInt dcid);
static void sendDisconnectCmd(ProgCtx *progSrc, ProgCtx *progSink, UInt dcid);

/* log helpers */
extern FILE *cmlog;
#define LOG fprintf(cmlog,
#define LOGF ); fflush(cmlog);

/* private data */
static ClusterMgr_Module ClusterMgr_mod = {
    .naNode         = 'A',
    .run            = FALSE,
    .prodQueMask    = PRODUCER_QUEUES,
    .consQueMask    = CONSUMER_QUEUES,
    .queue          = NULL,
    .dfrCmdReady    = 0
};

static char CM_combiner[] = "combiner";
static char CM_transformer[] = "transformer";


/*
 *  ======== ClusterMgr_completeCommand ========
 */
static Cmd_Command *ClusterMgr_completeCommand(Cmd_Command *cmd)
{

    switch (cmd->cmd) {
        case Cmd_CONNECT:
            cmd = completeConnectCmd(cmd);
            break;

        case Cmd_DISCONNECT:
            cmd = completeDisconnectCmd(cmd);
            break;

        case Cmd_PAUSE:
            cmd = completePauseCmd(cmd);
            break;

        case Cmd_SHUTDOWN:
            cmd = completeShutdownCmd(cmd);
            break;

        case Cmd_STATUS:
            cmd = completeStatusCmd(cmd);
            break;
    }

    return (cmd);
}

/*
 *  ======== ClusterMgr_destroy ========
 */
void ClusterMgr_destroy(void)
{
    LOG "ClusterMgr_destroy: -->\n" LOGF

    /* delete the message queue */
    MessageQ_delete(&ClusterMgr_mod.queue);

    LOG "ClusterMgr_destroy: <--\n" LOGF
}

/*
 *  ======== ClusterMgr_exec ========
 */
int ClusterMgr_exec(void)
{
    int status = 0;
    MessageQ_Msg mqMsg;
    Cmd_Command *cmd;
    Graph_Msg *graph;
    MessageQ_QueueId qid;
    Bool reply;

    typedef struct {
        MessageQ_MsgHeader reserved;
        Cmd_Command cmd;
    } CommandMsg;

#define CmdOffset ((Int)&((CommandMsg *)0)->cmd)
#define MessageQ_Msg_Addr(cmd) (MessageQ_Msg)((Char *)cmd - CmdOffset)

    LOG "ClusterMgr_exec: -->\n" LOGF
    ClusterMgr_mod.run = TRUE;

    /* main loop */
    while (ClusterMgr_mod.run) {

        /* wait for inbound message */
        status = MessageQ_get(ClusterMgr_mod.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            ClusterMgr_mod.run = FALSE;
            status = 0;
            goto leave;
        }
        else if (status < 0) {
            fprintf(stderr, "Error: ClusterMgr_exec: message receive failed, "
                    "error=%d\n", status);
            goto leave;
        }

        /* decode the message type, invoke process method */
        switch (MessageQ_getMsgId(mqMsg)) {

            case Cmd_MSGID:
                cmd = &((CommandMsg *)mqMsg)->cmd;
                cmd = ClusterMgr_processCommand(cmd);

                if (cmd != NULL) {
                    reply = cmd->reply;
                    cmd->reply = FALSE;
                }
                else {
                    mqMsg = NULL;
                }
                break;

            case Graph_MSGID:
                graph = (Graph_Msg *)mqMsg;
                ClusterMgr_processGraph(graph);
                reply = graph->reply;
                graph->reply = FALSE;
                break;
        }

        do {
            if (mqMsg != NULL) {
                if (reply) {
                    /* return the message to sender */
                    qid = MessageQ_getReplyQueue(mqMsg);
                    status = MessageQ_put(qid, mqMsg);

                    if (status < 0) {
                        fprintf(stderr, "Error: message send failed\n");
                        status = -2;
                        goto leave;
                    }
                }
                else {
                    MessageQ_free(mqMsg);
                }
            }
            mqMsg = NULL;

            /* process all deferred commands which are ready for completion */
            if (ClusterMgr_mod.dfrCmdReady > 0) {
                ClusterMgr_mod.dfrCmdReady--;

                cmd = Prog_getReadyCmd(&ClusterMgr_mod.dfrCmdList);
                cmd = ClusterMgr_completeCommand(cmd);

                if (cmd != NULL) {
                    reply = cmd->reply;
                    cmd->reply = FALSE;
                    mqMsg = MessageQ_Msg_Addr(cmd);
                }
                else {
                    mqMsg = NULL;
                }
            }
        } while (mqMsg != NULL);
    } /* main loop */

leave:
    LOG "ClusterMgr_exec: <-- status=%d\n", status LOGF

    return (status);
}

/*
 *  ======== ClusterMgr_processCommand ========
 */
static Cmd_Command *ClusterMgr_processCommand(Cmd_Command *cmd)
{

    switch (cmd->cmd) {

        case Cmd_CONNECT:
            cmd = doConnectCmd(cmd);
            break;

        case Cmd_DISCONNECT:
            cmd = doDisconnectCmd(cmd);
            break;

        case Cmd_EXIT:
            ClusterMgr_mod.run = FALSE;
            break;

        case Cmd_INFO:
            cmd = doInfoCmd(cmd);
            break;

        case Cmd_LAUNCH:
            cmd = doLaunchCmd(cmd);
            break;

        case Cmd_PAUSE:
            cmd = doPauseCmd(cmd);
            break;

        case Cmd_RUN:
            cmd = doRunCmd(cmd);
            break;

        case Cmd_SHUTDOWN:
            cmd = doShutdownCmd(cmd);
            break;

        case Cmd_STATUS:
            cmd = doStatusCmd(cmd);
            break;
    }

    return (cmd);
}

/*
 *  ======== ClusterMgr_processGraph ========
 */
int ClusterMgr_processGraph(Graph_Msg *msg)
{
    int status = 0;
    Cmd_Command *cmd;
    ProgCtx *prog;

    switch (msg->cmd) {

        case Graph_Cmd_ATTACH:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: ATTACH reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF

                /* search the deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);
                    assert(cmd != NULL);

                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_DETACH:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: DETACH reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF

                /* search the deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);
                    assert(cmd != NULL);

                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_DATAQUERY:
            /* acknowledgement message */
            if (msg->ack) {
                prog = (ProgCtx *)(msg->uarg1);

                if (msg->status >= 0) {
                    LOG "ClusterMgr: DATAQUERY success (reply from %c)\n",
                            prog->node LOGF

                    /* store query result */
                    prog->dataOn = (msg->arg1 > 0 ? TRUE : FALSE);
                }
                else {
                    LOG "ClusterMgr: DATAQUERY failure (reply from %c)\n",
                            prog->node LOGF
                    /* TODO dataOn should be tri-state */
                }

                /* search the  deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);

                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_FLUSH:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: FLUSH reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_INFO: {
            Char node = (Char)(msg->arg1);
            UInt blockSN = msg->uarg1;
            LOG "ClusterMgr: producer %c, block: %5d\n", node, blockSN LOGF
            break;
        }

        case Graph_Cmd_INPUTCONNECT:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: INPUT-CONNECT reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF

                /* TODO - How to handle an error? */

                /* search the deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);
                    assert(cmd != NULL);

                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_INPUTDISCONNECT:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: input-disconnect reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF

                /* search the deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);
                    assert(cmd != NULL);

                    /* decrement deferred count but leave command on list */
                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_OUTPUTCONNECT:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: OUTPUT-CONNECT reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF

                /* TODO - How to handle an error? */

                /* search the deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);
                    assert(cmd != NULL);

                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_OUTPUTDISCONNECT:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: output-disconnect reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF

                /* search the deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);
                    assert(cmd != NULL);

                    /* decrement deferred count but leave command on list */
                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_PAUSE:
            /* acknowledgement message */
            if (msg->ack) {
                LOG "ClusterMgr: PAUSE reply message, status %s\n",
                        msg->status < 0 ? "failure" : "success" LOGF

                /* search the deferred command list for given ID */
                if (msg->dcid != 0) {
                    cmd = Prog_findCmd(&ClusterMgr_mod.dfrCmdList, msg->dcid);
                    assert(cmd != NULL);

                    /* decrement deferred count but leave command on list */
                    if (--cmd->deferredCount == 0) {
                        /* this command is ready for completion */
                        ClusterMgr_mod.dfrCmdReady++;
                    }
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

        case Graph_Cmd_RUN:
            /* acknowledgement message */
            if (msg->ack) {
                prog = (ProgCtx *)(msg->uarg1);

                if (msg->status >= 0) {
                    LOG "ClusterMgr: RUN success (reply from %c)\n",
                            prog->node LOGF

                    /* store query result */
                    prog->dataOn = TRUE;
                }
                else {
                    LOG "ClusterMgr: RUN failure (reply from %c)\n",
                            prog->node LOGF
                    prog->dataOn = FALSE;
                }
            }
            else {
                fprintf(stderr, "Error: unexpected command message\n");
                status = -1;
            }
            break;

    }

    return (status);
}

/*
 *  ======== ClusterMgr_setup ========
 */
int ClusterMgr_setup(void)
{
    int status = 0;
    int i;
    MessageQ_Params msgqParams;

    LOG "ClusterMgr_setup: -->\n" LOGF

    /* initialize module state */
    ClusterMgr_mod.naNode = 'A';
    ClusterMgr_mod.run = FALSE;
    ClusterMgr_mod.queue = NULL;
    ClusterMgr_mod.dfrCmdReady = 0;
    LIST_INIT(&ClusterMgr_mod.consumer);
    LIST_INIT(&ClusterMgr_mod.producer);
    LIST_INIT(&ClusterMgr_mod.dfrCmdList);

    for (i = 0; i < DSPCOUNT; i++) {
        ClusterMgr_mod.dsp[i] = NULL;
    }

    /* create message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    ClusterMgr_mod.queue = MessageQ_create(ClusterMgr_CMDQUE, &msgqParams);

    if (ClusterMgr_mod.queue == NULL) {
        status = -2;
        fprintf(stderr, "Error: message queue create failed\n");
        goto leave;
    }

leave:
    LOG "ClusterMgr_setup: <-- status=%d\n", status LOGF
    return (status);
}

/*
 *  ======== ClusterMgr_system ========
 */
static pid_t ClusterMgr_system(char *cmd, bool wait)
{
    int status = 0;
    int wstat;
    pid_t pid;
    char *cp, *savecp;
    int argc;
    char cbuf[BUFSZ];
    char *argv[MAXARGS];

    /* argument validation */
    if ((strlen(cmd) + 1) > BUFSZ) {
        LOG "CM_system: Error: command too long: %s\n", cmd LOGF
        status = -2;
        goto leave;
    }

    /* parse command string into argument array */
    cp = strcpy(cbuf, cmd);

    for (argc = 0; argc < MAXARGS; argc++, cp = NULL) {
        argv[argc] = strtok_r(cp, " ", &savecp);
        if (argv[argc] == NULL) {
            break;
        }
    }

    if (argc == MAXARGS) {
        LOG "CM_system: Error: too many arguments: %s\n", cmd LOGF
        status = -3;
        goto leave;
    }

    /* fork a new process */
    pid = fork();

    if (pid == -1) {
        LOG "CM_system: fork error: %s\n", sys_errlist[errno] LOGF
        status = -1;
        goto leave;
    }

    /* parent */
    else if (pid > 0) {

        if (wait) {
            /* wait on child to exit */
            waitpid(pid, &wstat, 0);
            wstat = WEXITSTATUS(wstat);

            if ((wstat < 0) || (wstat >= 127)) {
                LOG "CM_system: waitpid error: %d\n", wstat LOGF
            }

            pid = 0;
        }
    }

    /* child */
    else {
        /* overlay a new executable */
        execvp(argv[0], argv);
        LOG "CM_system: execvp error: %s\n", sys_errlist[errno] LOGF
        status = -1;
    }

leave:
    return (status == 0 ? pid : status);
}

/*
 *  ======== ClusterMgr_threadFxn ========
 */
void *ClusterMgr_threadFxn(void *arg)
{
    int status = 0;

    LOG "ClusterMgr_threadFxn: -->\n" LOGF

    /* setup phase */
    status = ClusterMgr_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = ClusterMgr_exec();

leave:
    /* shutdown phase */
    ClusterMgr_destroy();

    LOG "ClusterMgr_threadFxn: <-- status=%d\n", status LOGF
    return ((Void *)status);
}

/*
 *  ======== Prog_findNode ========
 */
static ProgCtx *Prog_findNode(struct ListHead *head, Char node)
{
    ProgCtx *prog;

    /* search the given list */
    for (prog = head->lh_first; prog != NULL; prog = prog->link.le_next) {
        if (prog->node == node) {
            return (prog);
        }
    }

    return (NULL);
}

/*
 *  ======== Prog_findAllNode ========
 */
static ProgCtx *Prog_findAllNode(Char node)
{
    ProgCtx *prog;

    /* search the producer list */
    prog = Prog_findNode(&ClusterMgr_mod.producer, node);
    if (prog != NULL) return (prog);

    /* search the consumer list */
    prog = Prog_findNode(&ClusterMgr_mod.consumer, node);
    if (prog != NULL) return (prog);

    /* search the dsp array */
    prog = Prog_findDspNode(node);
    return (prog);
}

/*
 *  ======== Prog_findDspNode ========
 */
static ProgCtx *Prog_findDspNode(Char node)
{
    int i;

    /* search the dsp array */
    for (i = 0; i < DSPCOUNT; i++) {

        /* skip unused entries */
        if (ClusterMgr_mod.dsp[i] == NULL) {
            continue;
        }

        if (ClusterMgr_mod.dsp[i]->node == node) {
            return (ClusterMgr_mod.dsp[i]);
        }
    }

    return (NULL);
}

/*
 *  ======== Prog_findCmd ========
 */
static Cmd_Command *Prog_findCmd(struct DclHead *head, UInt dcid)
{
    Cmd_Command *cmd;

    /* search the given list */
    for (cmd = head->lh_first; cmd != NULL; cmd = cmd->link.le_next) {
        if ((UInt)cmd == dcid) {
            return (cmd);
        }
    }

    /* should never get here */
    assert(FALSE);
}

/*
 *  ======== Prog_getReadyCmd ========
 */
static Cmd_Command *Prog_getReadyCmd(struct DclHead *head)
{
    Cmd_Command *cmd;

    /* search the given list for a command which is ready for completion */
    for (cmd = head->lh_first; cmd != NULL; cmd = cmd->link.le_next) {
        if (cmd->deferredCount == 0) {
            LIST_REMOVE(cmd, link);
            return (cmd);
        }
    }

    return (NULL);
}

/*
 *  ======== List_insert_alpha ========
 *  Insert in alphabetical order using node name
 */
static Void List_insert_alpha(struct ListHead *list, ProgCtx *newItem)
{
    ProgCtx *item;


    item = list->lh_first;

    /* if list is empty or first item comes after new item, insert */
    if ((item == NULL) || (item->node > newItem->node)) {
        LIST_INSERT_HEAD(list, newItem, link);
    }
    else {
        /* seach list for insertion point */
        do {
            /* if last item in list, insert */
            if (item->link.le_next == NULL) {
                LIST_INSERT_AFTER(item, newItem, link);
                break;
            }
            /* if following item comes after new item, insert */
            else if (item->link.le_next->node > newItem->node) {
                LIST_INSERT_AFTER(item, newItem, link);
                break;
            }
            else {
                item = item->link.le_next;
            }
        } while (item != NULL);
    }
}

/*
 *  ======== doConnectCmd ========
 */
static Cmd_Command *doConnectCmd(Cmd_Command *cmd)
{
    struct connect *connect;
    ProgCtx *progSource;
    ProgCtx *progSink;

    /* clear the deferred count */
    cmd->deferredCount = 0;

    /* setup a convenience alias */
    connect = &cmd->args.connect;

    /* find program object for data-source */
    progSource = Prog_findAllNode(connect->source);

    if (progSource == NULL) {
        fprintf(stderr, "Error: node not found: %c\n", connect->source);
        goto leave;
    }

    /* find program object for data-sink */
    progSink = Prog_findAllNode(connect->sink);

    if (progSink == NULL) {
        fprintf(stderr, "Error: node not found: %c\n", connect->sink);
        goto leave;
    }

    /* a producer can never be a data-sink endpoint */
    if (progSink->type == ClusterMgr_PRODUCER) {
        fprintf(stderr, "Error: producer cannot be data-sink endpoint\n");
        goto leave;
    }

    /* track data-sink endpoint */
    progSource->dstNode = progSink->node;

    /*  If either endpoint is at least one of HOST or SR_ZERO_OWNER,
     *  then the attach has already been done. Just send the connect
     *  command.
     */
    if ((progSource->procId == MultiProc_getId("HOST"))
        || (progSource->procId == MultiProc_getId(SR_ZERO_OWNER))
        || (progSink->procId == MultiProc_getId("HOST"))
        || (progSink->procId == MultiProc_getId(SR_ZERO_OWNER))) {

        /* send the connect messages */
        LOG "doConnectCmd: send output-connect message to source: %c\n",
                connect->source LOGF
        LOG "doConnectCmd: send input-connect message to sink: %c\n",
                connect->sink LOGF
        sendConnectCmd(progSource, progSink);
    }
    else {
        /*  This is a data-only connection, which implies the two endpoint
         *  have not yet attached to each other. Send each endpoint an attach
         *  command and defer the completion of the connect command.
         */
        cmd->deferredCount++;
        sendAttachCmd(progSource, progSink->procId, (UInt)cmd);
        cmd->deferredCount++;
        sendAttachCmd(progSink, progSource->procId, (UInt)cmd);

        /* save the program context pointers for use in command epiloque */
        connect->progSource = (void *)progSource;
        connect->progSink = (void *)progSink;

        /* this command must be deferred until we hear back from both nodes */
        LIST_INSERT_HEAD(&ClusterMgr_mod.dfrCmdList, cmd, link);
        cmd = NULL;

        LOG "doConnectCmd: attach messages sent, command deferred\n" LOGF
    }

leave:
    return (cmd);
}

/*
 *  ======== completeConnectCmd ========
 */
static Cmd_Command *completeConnectCmd(Cmd_Command *cmd)
{
    struct connect *connect;
    ProgCtx *progSource;
    ProgCtx *progSink;

    /* setup aliases */
    connect = &cmd->args.connect;
    progSource = (ProgCtx *)connect->progSource;
    progSink = (ProgCtx *)connect->progSink;

    /* send the connect messages */
    LOG "doConnectCmd: send output-connect message to source: %c\n",
            progSource->node LOGF
    LOG "doConnectCmd: send input-connect message to sink: %c\n",
            progSink->node LOGF
    sendConnectCmd(progSource, progSink);

    return (cmd);
}

/*
 *  ======== doDisconnectCmd ========
 */
static Cmd_Command *doDisconnectCmd(Cmd_Command *cmd)
{
    struct disconnect *disconnect;
    ProgCtx *progSource;
    ProgCtx *progSink;

    /* clear the deferred count */
    cmd->deferredCount = 0;

    /* setup a convenience alias */
    disconnect = &cmd->args.disconnect;

    /* find program object for data-source */
    progSource = Prog_findAllNode(disconnect->source);

    if (progSource == NULL) {
        fprintf(stderr, "Error: node not found: %c\n", disconnect->source);
        goto leave;
    }

    /* find program object for data-sink */
    progSink = Prog_findAllNode(disconnect->sink);

    if (progSink == NULL) {
        fprintf(stderr, "Error: node not found: %c\n", disconnect->sink);
        goto leave;
    }

    /* validate connection information */
    if (progSource->dstNode != progSink->node) {
        fprintf(stderr, "Error: connection %c %c does not exist\n",
                disconnect->source, disconnect->sink);
        goto leave;
    }

    /* remove connection tracking information */
    progSource->dstNode = '-';

    /*  If either endpoint is at least one of HOST or SR_ZERO_OWNER,
     *  then to not instruct the endpoints to detach. Just send the
     *  disconnect command.
     */
    if ((progSource->procId == MultiProc_getId("HOST"))
        || (progSource->procId == MultiProc_getId(SR_ZERO_OWNER))
        || (progSink->procId == MultiProc_getId("HOST"))
        || (progSink->procId == MultiProc_getId(SR_ZERO_OWNER))) {

        /* send disconnect message to source */
        LOG "doDisconnectCmd: send output-disconnect message to source: %c\n",
                disconnect->source LOGF
        LOG "doDisconnectCmd: send input-disconnect message to sink: %c\n",
                disconnect->sink LOGF
        sendDisconnectCmd(progSource, progSink, 0);
    }
    else {
        /*  This is a data-only connection, which implies the two
         *  endpoints must detach from each other after diconnecting.
         *  Send each node the disconnect command but defer the detach
         *  commands to ensure the disconnect has completed.
         */
        LOG "doDisconnectCmd: send output-disconnect message to source: %c\n",
                disconnect->source LOGF
        LOG "doDisconnectCmd: send input-disconnect message to sink: %c\n",
                disconnect->sink LOGF
        sendDisconnectCmd(progSource, progSink, (UInt)cmd);

        /* save program context pointers for use in command epilogue */
        cmd->deferredCount += 2;
        disconnect->progSource = (void *)progSource;
        disconnect->progSink = (void *)progSink;

        /* put command on deferred list */
        LIST_INSERT_HEAD(&ClusterMgr_mod.dfrCmdList, cmd, link);
        cmd = NULL;

        LOG "doDisconnectCmd: disconnect messages sent, command deferred\n" LOGF
    }

leave:
    return (cmd);
}

/*
 *  ======== completeDisconnectCmd ========
 */
static Cmd_Command *completeDisconnectCmd(Cmd_Command *cmd)
{
    struct disconnect *disconnect;
    ProgCtx *progSource;
    ProgCtx *progSink;

    /* setup aliases */
    disconnect = &cmd->args.disconnect;
    progSource = (ProgCtx *)disconnect->progSource;
    progSink = (ProgCtx *)disconnect->progSink;

    /* send detach commands */
    sendDetachCmd(progSource, progSink->procId, 0);
    sendDetachCmd(progSink, progSource->procId, 0);

    LOG "completeDisconnectCmd: detach messages sent\n" LOGF

    return (cmd);
}

/*
 *  ======== doInfoCmd ========
 */
static Cmd_Command *doInfoCmd(Cmd_Command *cmd)
{
    struct info *info;
    int i;
    int procId;
    String name;

    info = &cmd->args.info;

    if (info->item == Cmd_BASEID) {
        info->baseId = MultiProc_getBaseIdOfCluster();
    }
    else if (info->item == Cmd_CLUSTER) {
        info->size = MultiProc_getNumProcsInCluster();
        info->baseId = MultiProc_getBaseIdOfCluster();

        for (i = 0; i < info->size; i++) {
            procId = info->baseId + i;
            name = MultiProc_getName(procId);
            strcpy(info->names[i], name);
        }
    }

    return (cmd);
}

/*
 *  ======== doLaunchCmd ========
 */
static Cmd_Command *doLaunchCmd(Cmd_Command *cmd)
{
    int status = 0;
    pid_t pid;
    ProgCtx *prog = NULL;
    struct launch *launch;
    char scmd[BUFSZ];
    char qname[BUFSZ];
    MessageQ_QueueId qid;
    int mpmId;
    UInt16 procId;
    int i;
    UInt8 bit;
    Bool dspAttach = FALSE;
    Bool hostAttach = FALSE;
    ProgCtx *host;
    ProgCtx *dsp;
    ProgCtx *srzero;

    launch = &cmd->args.launch;

    /* allocate a new program context object */
    prog = malloc(sizeof(ProgCtx));
    prog->node = '-';
    prog->dstNode = '-';
    prog->type = ClusterMgr_UNKNOWN;
    prog->pid = 0;
    prog->cmdQue = MessageQ_INVALIDMESSAGEQ;
    prog->transId = 0;
    prog->procId = MultiProc_INVALIDID;
    prog->attached = FALSE;
    prog->dataOn = FALSE;

    /* assign next available node name */
    prog->node = ClusterMgr_mod.naNode++;

    switch (launch->program) {
        case Cmd_CONSUMER:
            /* compute the command message queue name */
            snprintf(qname, BUFSZ, Consumer_CMDQUE_FMT, prog->node);

            /* determine the next available reserved message queue index */
            prog->dataQueIdx = -1;

            for (i = 0; i < NUM_RESERVED_QUEUES; i++) {
                bit = 1 << i;
                if (bit & ClusterMgr_mod.consQueMask) {
                    prog->dataQueIdx = i;
                    ClusterMgr_mod.consQueMask &= ~bit; /* clear the bit */
                    break;
                }
            }

            if (prog->dataQueIdx < 0) {
                fprintf(stderr, "Error: no available message queues\n");
                goto leave;
            }

            /* compute the launch command, launch program */
            snprintf(scmd, BUFSZ, Consumer_LAUNCH_FMT, prog->node,
                    prog->dataQueIdx);

            pid = ClusterMgr_system(scmd, FALSE);

            if (pid < 0) {
                fprintf(stderr, "Error: system call failed: %s\n", scmd);
                goto leave;
            }

            /* save child process ID */
            prog->pid = pid;
            break;

        case Cmd_COMBINER:
            /* for dsp, reserved queues are pre-determined */
            prog->dataQueIdx = SysCfg_DATA_RQUE;

            /* mpmcl names are base-zero, must subtract 1 from clusterId */
            mpmId = launch->processor - 1;

            /* load the dsp */
            snprintf(scmd, BUFSZ, CombinerN_LOAD_FMT, mpmId);
            status = ClusterMgr_system(scmd, TRUE);

            if (status < 0) {
                fprintf(stderr, "Error: system call failed: %s\n", scmd);
                goto leave;
            }

            /* run the dsp */
            snprintf(scmd, BUFSZ, CombinerN_RUN_FMT, mpmId);
            status = ClusterMgr_system(scmd, TRUE);

            if (status < 0) {
                fprintf(stderr, "Error: system call failed: %s\n", scmd);
                goto leave;
            }

            /* TODO: wait to allow rpmsg driver to setup */
            usleep(300000); /* 0.3 second */

            procId = MultiProc_getBaseIdOfCluster() + launch->processor;

            /* send attach command to SR-Zero owner */
            if (launch->processor != 1) {
                srzero = ClusterMgr_mod.dsp[0];
                sendAttachCmd(srzero, procId, 0);
            }

            /* attach to dsp */
            status = Ipc_attach(procId);

            if (status < 0) {
                fprintf(stderr, "Error: Ipc_attach(%d) failed\n", procId);
                goto leave;
            }

            /* compute name of combiner control message queue */
            snprintf(qname, BUFSZ, CombinerN_CMDQUE_FMT, procId);
            break;

        case Cmd_PRODUCER:
            /* compute the message queue name */
            snprintf(qname, BUFSZ, Producer_CMDQUE_FMT, prog->node);

            /* determine the next available reserved message queue index */
            prog->dataQueIdx = -1;

            for (i = 0; i < NUM_RESERVED_QUEUES; i++) {
                bit = 1 << i;
                if (bit & ClusterMgr_mod.prodQueMask) {
                    prog->dataQueIdx = i;
                    ClusterMgr_mod.prodQueMask &= ~bit; /* clear the bit */
                    break;
                }
            }

            if (prog->dataQueIdx < 0) {
                fprintf(stderr, "Error: no available message queues\n");
                goto leave;
            }

            /* compute the launch command, launch program */
            snprintf(scmd, BUFSZ, Producer_LAUNCH_FMT, prog->node,
                    prog->dataQueIdx);

            pid = ClusterMgr_system(scmd, FALSE);

            if (pid < 0) {
                fprintf(stderr, "Error: system call failed: %s\n", scmd);
                goto leave;
            }

            /* save child process ID */
            prog->pid = pid;
            break;

        case Cmd_TRANSFORMER:
            /* for dsp, reserved queues are pre-determined */
            prog->dataQueIdx = SysCfg_DATA_RQUE;

            /* mpmcl names are base-zero, must subtract 1 from clusterId */
            mpmId = launch->processor - 1;

            /* load the dsp */
            snprintf(scmd, BUFSZ, TransformerN_LOAD_FMT, mpmId);
            status = ClusterMgr_system(scmd, TRUE);

            if (status < 0) {
                fprintf(stderr, "Error: system call failed: %s\n", scmd);
                goto leave;
            }

            /* run the dsp */
            snprintf(scmd, BUFSZ, TransformerN_RUN_FMT, mpmId);
            status = ClusterMgr_system(scmd, TRUE);

            if (status < 0) {
                fprintf(stderr, "Error: system call failed: %s\n", scmd);
                goto leave;
            }

            /* TODO: wait to allow rpmsg driver to setup */
            usleep(300000); /* 0.3 second */

            procId = MultiProc_getBaseIdOfCluster() + launch->processor;

            /* send attach command to SR-Zero owner */
            if (launch->processor != 1) {
                srzero = ClusterMgr_mod.dsp[0];
                sendAttachCmd(srzero, procId, 0);
            }

            /* attach to dsp */
            status = Ipc_attach(procId);

            if (status < 0) {
                fprintf(stderr, "Error: Ipc_attach(%d) failed\n", procId);
                goto leave;
            }

            /* compute name of transformer control message queue */
            snprintf(qname, BUFSZ, TransformerN_CMDQUE_FMT, procId);
            break;
    }

    /* TODO: wait to allow new program to create message queue */
    usleep(400000); /* 0.4 second */

    /* open the destination message queue */
    do {
        status = MessageQ_open(qname, &qid);

        if (status == MessageQ_E_NOTFOUND) {
            usleep(60000); /* 60 milliseconds */
        }
    } while (status == MessageQ_E_NOTFOUND);

    if (status < 0) {
        fprintf(stderr, "Error: cannot open queue, name=%s error=%d\n",
                qname, status);
        goto leave;
    }

    /* update local database with connection details */
    switch (launch->program) {
        case Cmd_COMBINER:
            prog->type = ClusterMgr_COMBINER;
            prog->cmdQue = qid;
            prog->transId = 0; /* use primary transport (i.e. default) */
            prog->procId = procId;
            prog->attached = TRUE;
            ClusterMgr_mod.dsp[mpmId] = prog;
            dspAttach = TRUE;
            break;

        case Cmd_CONSUMER:
            prog->type = ClusterMgr_CONSUMER;
            prog->cmdQue = qid;
            prog->transId = SysCfg_TRANSPORT_PMQ_ID; /* posix message queue */
            prog->procId = MultiProc_self();
            prog->attached = FALSE;
            List_insert_alpha(&ClusterMgr_mod.consumer, prog);
            hostAttach = TRUE;
            break;

        case Cmd_PRODUCER:
            prog->type = ClusterMgr_PRODUCER;
            prog->cmdQue = qid;
            prog->transId = SysCfg_TRANSPORT_PMQ_ID; /* posix message queue */
            prog->procId = MultiProc_self();
            prog->attached = FALSE;
            List_insert_alpha(&ClusterMgr_mod.producer, prog);
            hostAttach = TRUE;
            break;

        case Cmd_TRANSFORMER:
            prog->type = ClusterMgr_TRANSFORMER;
            prog->cmdQue = qid;
            prog->transId = 0; /* use primary transport (i.e. default) */
            prog->procId = procId;
            prog->attached = TRUE;
            ClusterMgr_mod.dsp[mpmId] = prog;
            dspAttach = TRUE;
            break;
    }

#if 0
    /*  Optional: In the current implementation, the SR-Zero owner has
     *  an idle function which attempts to attach to all remaining DSP
     *  processors. An althernative would be to remove the idle function
     *  and send an explicit attach message to the SR-Zero owner from here.
     */
    if (dspAttach) {
        /* send SR-Zero owner attach message */
    }
#endif

    /* all host programs must attach to new dsp */
    if (dspAttach) {

        /* walk list of producers */
        for (host = ClusterMgr_mod.producer.lh_first;
            host != NULL; host = host->link.le_next) {
            sendAttachCmd(host, prog->procId, 0);
        }

        /* walk list of consumers */
        for (host = ClusterMgr_mod.consumer.lh_first;
            host != NULL; host = host->link.le_next) {
            sendAttachCmd(host, prog->procId, 0);
        }
    }

    /* new host program must attach to all running dsp processors */
    if (hostAttach) {
        for (i = 0; i < DSPCOUNT; i++) {
            dsp = ClusterMgr_mod.dsp[i];
            if (dsp != NULL) {
                sendAttachCmd(prog, dsp->procId, 0);
            }
        }
    }

    LOG "doLaunchCmd: queueId = 0x%08x\n", (unsigned int)qid LOGF

leave:
    return (cmd);
}

/*
 *  ======== doPauseCmd ========
 */
static Cmd_Command *doPauseCmd(Cmd_Command *cmd)
{
    int status = 0;
    struct pause *pause = &cmd->args.pause;
    ProgCtx *producer;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    /* search the producer list for the give node */
    producer = Prog_findNode(&ClusterMgr_mod.producer, pause->node);

    if (producer == NULL) {
        fprintf(stderr, "Error: node not found: %c\n", pause->node);
        status = -1;
        goto leave;
    }

    /* send message to producer */
    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, producer->transId);

    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_PAUSE;
    msg->reply = TRUE;
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);
    msg->ack = FALSE;
    msg->dcid = (UInt)cmd; /* store the deferred command ID */

    status = MessageQ_put(producer->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }
    LOG "doPauseCmd: message sent to producer: %c\n", pause->node LOGF

    /* add command to deferred command list */
    cmd->deferredCount = 1;
    pause->producer = (void *)producer;
    LIST_INSERT_HEAD(&ClusterMgr_mod.dfrCmdList, cmd, link);

leave:
    return (status < 0 ? cmd : NULL);
}

/*
 *  ======== completePauseCmd ========
 */
static Cmd_Command *completePauseCmd(Cmd_Command *cmd)
{
    int status;
    struct pause *pause;
    ProgCtx *prog;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;
    UInt16 procId;
    UInt16 queueIndex;

    pause = &cmd->args.pause;
    prog = (ProgCtx *)pause->producer;

    /* send each node in data flow a flush command */
    while (prog->dstNode != '-') {

        /* save source connection information */
        procId = prog->procId;
        queueIndex = prog->dataQueIdx;

        /* find next downstream node */
        prog = Prog_findAllNode(prog->dstNode);
        assert(prog != NULL);

        /* flush message */
        mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

        if (mqMsg == NULL) {
            fprintf(stderr, "Error: message alloc failed\n");
            goto leave;
        }

        MessageQ_setMsgId(mqMsg, Graph_MSGID);
        MessageQ_setTransportId(mqMsg, prog->transId);

        msg = (Graph_Msg *)mqMsg;
        msg->cmd = Graph_Cmd_FLUSH;
        msg->dcid = 0;
        msg->reply = TRUE;
        MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);
        msg->ack = FALSE;
        msg->arg1 = procId;
        msg->arg2 = queueIndex;

        status = MessageQ_put(prog->cmdQue, mqMsg);

        if (status < 0) {
            fprintf(stderr, "Error: message send failed\n");
            goto leave;
        }
        LOG "completePauseCmd: flush message sent to node %c\n", prog->node LOGF
    }

leave:
    return (cmd);
}

/*
 *  ======== doRunCmd ========
 */
static Cmd_Command *doRunCmd(Cmd_Command *cmd)
{
    int status;
    struct run *run = &cmd->args.run;
    ProgCtx *producer;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    /* search the producer list for the give node */
    producer = Prog_findNode(&ClusterMgr_mod.producer, run->node);

    if (producer == NULL) {
        fprintf(stderr, "Error: node not found: %c\n", run->node);
        goto leave;
    }

    /* send message to producer */
    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, producer->transId);

    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_RUN;
    msg->dcid = 0;
    msg->reply = TRUE;
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);
    msg->ack = FALSE;
    msg->uarg1 = (UInt)producer;

    status = MessageQ_put(producer->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }
    LOG "doRunCmd: message sent to producer: %c\n", run->node LOGF

leave:
    return (cmd);
}

/*
 *  ======== doShutdownCmd ========
 */
static Cmd_Command *doShutdownCmd(Cmd_Command *cmd)
{
    int status = 0;
    struct shutdown *shutdown;
    MessageQ_Msg mqMsg = NULL;
    struct ProgCtx *prog;
    Bool shutdownDSP = FALSE;
    Bool shutdownHOST = FALSE;
    int i;
    ProgCtx *host;
    ProgCtx *dsp;

    /* clear the deferred count */
    cmd->deferredCount = 0;

    /* setup a convenience alias */
    shutdown = &cmd->args.shutdown;
    shutdown->msg = NULL;
    shutdown->prog = NULL;

    switch (shutdown->program) {
        case Cmd_DSP1:
        case Cmd_DSP2:
        case Cmd_DSP3:
        case Cmd_DSP4:
        case Cmd_DSP5:
        case Cmd_DSP6:
        case Cmd_DSP7:
        case Cmd_DSP8: {
            int index = shutdown->program - 1;
            prog = ClusterMgr_mod.dsp[index];
            ClusterMgr_mod.dsp[index] = NULL;
            if (prog == NULL) {
                fprintf(stderr, "Error: dsp in reset\n");
                status = -1;
                goto leave;
            }
            shutdown->prog = (void *)prog;
            shutdownDSP = TRUE;
            break;
        }

        default:
            prog = Prog_findNode(&ClusterMgr_mod.consumer, shutdown->node);
            if (prog != NULL) {
                shutdown->program = Cmd_CONSUMER;
                shutdown->prog = (void *)prog;
            }

            prog = Prog_findNode(&ClusterMgr_mod.producer, shutdown->node);
            if (prog != NULL) {
                shutdown->program = Cmd_PRODUCER;
                shutdown->prog = (void *)prog;
            }

            if (shutdown->prog == NULL) {
                fprintf(stderr, "Error: node %c not found\n", shutdown->node);
                status = -2;
                goto leave;
            }

            prog = (struct ProgCtx *)(shutdown->prog);
            LIST_REMOVE(prog, link);
            shutdownHOST = TRUE;
            break;
    }

    /* allocate a graph message */
    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        status = -1;
        goto leave;
    }

    /* store message pointer in command object */
    shutdown->msg = (void *)mqMsg;

    /* identify the message type */
    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, prog->transId);

    /* target host program must detach from all running dsp processors */
    if (shutdownHOST) {
        for (i = 0; i < DSPCOUNT; i++) {
            dsp = ClusterMgr_mod.dsp[i];
            if (dsp != NULL) {
                sendDetachCmd(prog, dsp->procId, (UInt)cmd);
                cmd->deferredCount++;
            }
        }
    }

    /* all host programs must detach from target dsp */
    if (shutdownDSP) {

        /* walk list of producers */
        for (host = ClusterMgr_mod.producer.lh_first;
            host != NULL; host = host->link.le_next) {
            sendDetachCmd(host, prog->procId, (UInt)cmd);
            cmd->deferredCount++;
        }

        /* walk list of consumers */
        for (host = ClusterMgr_mod.consumer.lh_first;
            host != NULL; host = host->link.le_next) {
            sendDetachCmd(host, prog->procId, (UInt)cmd);
            cmd->deferredCount++;
        }
    }

    /* add command to deferred command list */
    LIST_INSERT_HEAD(&ClusterMgr_mod.dfrCmdList, cmd, link);

    /* if no sub-command, then this command is ready for completion */
    if (cmd->deferredCount == 0) {
        ClusterMgr_mod.dfrCmdReady++;
    }

leave:
    if (status < 0) {
        if (mqMsg != NULL) {
            MessageQ_free(mqMsg);
            mqMsg = NULL;
        }
    }

    return (status < 0 ? cmd : NULL);
}

/*
 *  ======== completeShutdownCmd ========
 */
static Cmd_Command *completeShutdownCmd(Cmd_Command *cmd)
{
    int status = 0;
    int wstat;
    struct shutdown *shutdown;
    struct ProgCtx *prog;
    Graph_Msg *msg;
    MessageQ_Msg mqMsg;
    MessageQ_QueueId *pqid;
    char scmd[BUFSZ];

    /* setup aliases */
    shutdown = &cmd->args.shutdown;
    prog = (struct ProgCtx *)(shutdown->prog);
    mqMsg = (MessageQ_Msg)shutdown->msg;

    /* fill in message payload */
    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_SHUTDOWN;
    msg->reply = FALSE;

    /* send the message */
    pqid = &prog->cmdQue;
    status = MessageQ_put(*pqid, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        status = -2;
        goto leave;
    }
    LOG "completeShutdownCmd: shutdown message sent, qid=0x%x\n", *pqid LOGF

    /* close the remote message queue */
    MessageQ_close(pqid);

    switch (shutdown->program) {
        case Cmd_CONSUMER:
            /* reclaim the reserved message queue index */
            ClusterMgr_mod.consQueMask |= (1 << prog->dataQueIdx);

            /* wait until child has terminated */
            waitpid(prog->pid, &wstat, 0);
            wstat = WEXITSTATUS(wstat);

            if ((wstat < 0) || (wstat >= 127)) {
                LOG "CM_system: waitpid error: %d\n", wstat LOGF
            }
            break;

        case Cmd_PRODUCER:
            /* reclaim the reserved message queue index */
            ClusterMgr_mod.prodQueMask |= (1 << prog->dataQueIdx);

            /* wait until child has terminated */
            waitpid(prog->pid, &wstat, 0);
            wstat = WEXITSTATUS(wstat);

            if ((wstat < 0) || (wstat >= 127)) {
                LOG "CM_system: waitpid error: %d\n", wstat LOGF
            }
            break;

        case Cmd_DSP1:
        case Cmd_DSP2:
        case Cmd_DSP3:
        case Cmd_DSP4:
        case Cmd_DSP5:
        case Cmd_DSP6:
        case Cmd_DSP7:
        case Cmd_DSP8: {
            /* the dsp processor must be placed into reset */
            UInt16 procId;
            int dspId;

            /* detach from the dsp */
            procId = MultiProc_getBaseIdOfCluster() + shutdown->program;

            status = Ipc_detach(procId);

            if (status < 0) {
                fprintf(stderr, "Error: Ipc_detach(%d) failed\n", procId);
                goto leave;
            }

            dspId = shutdown->program - 1;
            prog->attached = FALSE;

            /* wait to let dsp go to idle task */
            usleep(300000); /* 0.3 sec */

            /* construct the system command */
            snprintf(scmd, BUFSZ, TransformerN_RESET_FMT, dspId);
            status = ClusterMgr_system(scmd, TRUE);

            if (status < 0) {
                fprintf(stderr, "Error: system call failed: %s\n", scmd);
                goto leave;
            }
            break;
        }
    }

    /* free the program context object */
    free(prog);

leave:
    return (cmd);
}

/*
 *  ======== doStatusCmd ========
 */
static Cmd_Command *doStatusCmd(Cmd_Command *cmd)
{
    int status;
    ProgCtx *prog;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    /* clear the deferred count */
    cmd->deferredCount = 0;

    /* send each producer a data query message */
    for (prog = ClusterMgr_mod.producer.lh_first;
        prog != NULL; prog = prog->link.le_next) {

        mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

        if (mqMsg == NULL) {
            fprintf(stderr, "Error: message alloc failed\n");
            goto leave;
        }

        /* fill in message payload */
        MessageQ_setMsgId(mqMsg, Graph_MSGID);
        MessageQ_setTransportId(mqMsg, prog->transId);

        msg = (Graph_Msg *)mqMsg;
        msg->cmd = Graph_Cmd_DATAQUERY;
        MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);
        msg->reply = TRUE;
        msg->ack = FALSE;
        msg->uarg1 = (UInt)prog;
        msg->dcid = (UInt)cmd; /* store the deferred command ID */

        /* send the message */
        status = MessageQ_put(prog->cmdQue, mqMsg);

        if (status < 0) {
            fprintf(stderr, "Error: message send failed\n");
            goto leave;
        }
        LOG "doStatusCmd: sent data query, node=%c\n", prog->node LOGF

        cmd->deferredCount++;
    }

leave:
    /* add command to deferred command list */
    LIST_INSERT_HEAD(&ClusterMgr_mod.dfrCmdList, cmd, link);

    /* if no sub-command, then this command is ready for completion */
    if (cmd->deferredCount == 0) {
        ClusterMgr_mod.dfrCmdReady++;
    }

    return (NULL);
}

/*
 *  ======== completeStatusCmd ========
 */
static Cmd_Command *completeStatusCmd(Cmd_Command *cmd)
{
    ProgCtx *prog;
    int i;
    char *type;


    printf("\nProgram Status:\n");
    printf("----------------------------------------\n");

    for (prog = ClusterMgr_mod.producer.lh_first;
        prog != NULL; prog = prog->link.le_next) {
        printf(" %-2c %-14s %-4s: cqid=0x%08x %-4s\n", prog->node, "producer",
                "host", prog->cmdQue, (prog->dataOn ? "on" : "off"));
    }

    for (prog = ClusterMgr_mod.consumer.lh_first;
        prog != NULL; prog = prog->link.le_next) {
        printf(" %-2c %-14s %-4s: cqid=0x%08x\n", prog->node, "consumer",
                "host", prog->cmdQue);
    }

    for (i = 0; i < DSPCOUNT; i++) {
        prog = ClusterMgr_mod.dsp[i];

        if (prog == NULL) {
            printf(" %-2c %-14s dsp%d: cqid=0x%08x\n", '-', "--------",
                    i + 1, MessageQ_INVALIDMESSAGEQ);
        }
        else {
            switch (prog->type) {
                case ClusterMgr_COMBINER:
                    type = CM_combiner;
                    break;
                case ClusterMgr_TRANSFORMER:
                    type = CM_transformer;
                    break;
                default:
                    /* TODO: report error */
                    break;
            }
            printf(" %-2c %-14s dsp%d: cqid=0x%08x\n", prog->node, type,
                    i + 1, prog->cmdQue);
        }
    }

    printf("\nConnections:\n");
    printf("----------------------------------------\n");
    for (prog = ClusterMgr_mod.producer.lh_first;
        prog != NULL; prog = prog->link.le_next) {

        if (prog->dstNode != '-') {
            printf(" %-2c --> %-2c\n", prog->node, prog->dstNode);
        }
    }

    for (i = 0; i < DSPCOUNT; i++) {
        prog = ClusterMgr_mod.dsp[i];

        if ((prog != NULL) && (prog->dstNode != '-')) {
            printf(" %-2c --> %-2c\n", prog->node, prog->dstNode);
        }
    }

    printf("\n");

    return (cmd);
}

/*
 *  ======== sendAttachCmd ========
 */
static void sendAttachCmd(ProgCtx *prog, UInt16 procId, UInt dcid)
{
    int status;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    /* message delivery details */
    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, prog->transId);
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);

    /* command details */
    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_ATTACH;
    msg->uarg1 = procId;
    msg->dcid = dcid; /* store the deferred command ID */
    msg->reply = TRUE;
    msg->ack = FALSE;

    /* send message */
    status = MessageQ_put(prog->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }
    LOG "attach message sent to node %c, procId %d\n", prog->node, procId LOGF

leave:
    return;
}

/*
 *  ======== sendDetachCmd ========
 */
static void sendDetachCmd(ProgCtx *prog, UInt16 procId, UInt dcid)
{
    int status;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    /* message delivery details */
    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, prog->transId);
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);

    /* command details */
    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_DETACH;
    msg->uarg1 = procId;
    msg->dcid = dcid; /* store the deferred command ID */
    msg->reply = TRUE;
    msg->ack = FALSE;

    /* send message */
    status = MessageQ_put(prog->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }
    LOG "detach message sent to node %c, procId %d\n", prog->node, procId LOGF

leave:
    return;
}

/*
 *  ======== sendConnectCmd ========
 */
static void sendConnectCmd(ProgCtx *progSource, ProgCtx *progSink)
{
    int status;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    /* allocate graph message to send output-connect command to source node */
    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, progSource->transId);
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);

    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_OUTPUTCONNECT;
    msg->uarg1 = progSink->procId;
    msg->uarg2 = progSink->dataQueIdx;
    msg->dcid = 0;
    msg->reply = TRUE;
    msg->ack = FALSE;

    /* send message to source node */
    status = MessageQ_put(progSource->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }

    /* allocate graph message to send input-connect command to sink node */
    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, progSink->transId);
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);

    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_INPUTCONNECT;
    msg->uarg1 = progSource->procId;
    msg->uarg2 = progSource->dataQueIdx;
    msg->dcid = 0;
    msg->reply = TRUE;
    msg->ack = FALSE;

    /* send message to source */
    status = MessageQ_put(progSink->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }

leave:
    return;
}

/*
 *  ======== sendDisconnectCmd ========
 */
static void sendDisconnectCmd(ProgCtx *progSource, ProgCtx *progSink, UInt dcid)
{
    int status;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    /* graph message to send output-disconnect command to source node */
    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, progSource->transId);
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);

    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_OUTPUTDISCONNECT;
    msg->uarg1 = progSink->procId;
    msg->uarg2 = progSink->dataQueIdx;
    msg->dcid = dcid; /* store the deferred command ID */
    msg->reply = TRUE;
    msg->ack = FALSE;

    /* send message to source node */
    status = MessageQ_put(progSource->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }

    /* allocate graph message to send input-disconnect command to sink node */
    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        fprintf(stderr, "Error: message alloc failed\n");
        goto leave;
    }

    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, progSink->transId);
    MessageQ_setReplyQueue(ClusterMgr_mod.queue, mqMsg);

    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_INPUTDISCONNECT;
    msg->uarg1 = progSource->procId;
    msg->uarg2 = progSource->dataQueIdx;
    msg->dcid = dcid; /* store the deferred command ID */
    msg->reply = TRUE;
    msg->ack = FALSE;

    /* send message to source */
    status = MessageQ_put(progSink->cmdQue, mqMsg);

    if (status < 0) {
        fprintf(stderr, "Error: message send failed\n");
        goto leave;
    }

leave:
    return;
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Console.c ========
 */

/* standard headers */
#include <stdio.h>              /* printf, fprintf, getline */
#include <string.h>             /* strcpy */
#include <unistd.h>             /* usleep */

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>

/* example header files */
#include <ex45_host/transport/hlos/TransportPMQ.h>
#include "../shared/ClusterMgr_shared.h"
#include "Cmd.h"

#define HEAPID 0

/* log helpers */
extern FILE *cmlog;
#define LOG fprintf(cmlog,
#define LOGF ); fflush(cmlog);

/* private functions */
void doBaseId(Cmd_Command *cmd);
void doCluster(Cmd_Command *cmd);

/*
 *  ======== Console_threadFxn ========
 */
void *Console_threadFxn(void *arg)
{
    int status = 0;
    Bool run = TRUE;
    char *buf = NULL;
    size_t nbytes;
    MessageQ_Handle queue;
    MessageQ_QueueId cmQue;
    Cmd_Command *cmd;
    MessageQ_Msg mqMsg;
    char text[64];
    int seek;

    typedef struct {
        MessageQ_MsgHeader reserved;
        Cmd_Command cmd;
    } CommandMsg;

    LOG "Console_threadFxn: -->\n" LOGF

    /* create an anonymous message queue (for reply messages) */
    queue = MessageQ_create(NULL, NULL);

    if (queue == NULL) {
        fprintf(stderr, "Error: cannot create message queues\n");
        goto leave;
    }

    /* open the cluster manager message queue */
    do {
        status = MessageQ_open(ClusterMgr_CMDQUE, &cmQue);

        if (status == MessageQ_E_NOTFOUND) {
            usleep(60000); /* 60 milliseconds */
        }
    } while (status == MessageQ_E_NOTFOUND);

    if (status < 0) {
        fprintf(stderr, "Error: cannot open cluster manager queue, name=%s "
                "error=%d\n", ClusterMgr_CMDQUE, status);
        goto leave;
    }

    /* main loop */
    while (run) {

        /* read the next input string */
        printf("[cm]> ");
        nbytes = getline(&buf, &nbytes, stdin);

        if (nbytes == 0) {
            fprintf(stderr, "Error: Console_threadFxn: getline failed\n");
            status = -1;
            goto leave;
        }
        else if ((nbytes == 1) && (strcmp(buf, "\n") == 0)) {
            continue;
        }

        LOG "Console_threadFxn: input: %s", buf LOGF

        /* allocate message to contain command object */
        mqMsg = MessageQ_alloc(HEAPID, sizeof(CommandMsg));

        /* set return address, identify message type */
        MessageQ_setReplyQueue(queue, mqMsg);
        MessageQ_setMsgId(mqMsg, Cmd_MSGID);

        /* all command messages should come back here */
        cmd = &((CommandMsg *)mqMsg)->cmd;
        cmd->reply = TRUE;

        /* parse input string into the command object */
        Cmd_parse(buf, cmd);

        if (cmd->cmd == -1) {
            /* bad input, try again */
            MessageQ_free(mqMsg);
            continue;
        }

        /* check for exit command */
        if (cmd->cmd == Cmd_EXIT) {
            run = FALSE;
        }

        /* get help text and print to user */
        else if (cmd->cmd == Cmd_HELP) {
            seek = 0;
            do {
                nbytes = Cmd_help(text, sizeof(text), seek);

                seek += nbytes;
                if (nbytes > 0) {
                    printf("%s", text);
                }
            } while (nbytes > 0);

            /* don't send this command to manager */
            MessageQ_free(mqMsg);
            continue;
        }

        /* send command to manager thread */
        status = MessageQ_put(cmQue, mqMsg);

        if (status < 0) {
            fprintf(stderr, "Error: message send failed\n");
            status = -2;
            goto leave;
        }

        /* wait for reply message */
        status = MessageQ_get(queue, &mqMsg, MessageQ_FOREVER);

        if (status < 0) {
            fprintf(stderr, "Error:  message receive failed\n");
            goto leave;
        }

        /* process the reply message */
        cmd = &((CommandMsg *)mqMsg)->cmd;

        if (cmd->status < 0) {
            printf("Error: invalid input\n");
        }
        else if (cmd->cmd == Cmd_INFO) {
            if (cmd->args.info.item == Cmd_BASEID) {
                doBaseId(cmd);
            }
            else if (cmd->args.info.item == Cmd_CLUSTER) {
                doCluster(cmd);
            }
        }

        MessageQ_free(mqMsg);
    }

leave:
    LOG "Console_threadFxn: <-- status=%d\n", status LOGF
    return ((Void *)status);
}

/*
 *  ======== doBaseId ========
 */
void doBaseId(Cmd_Command *cmd)
{
    int baseId;

    baseId = cmd->args.info.baseId;
    printf("baseId = %d\n", baseId);
}

/*
 *  ======== doCluster ========
 */
void doCluster(Cmd_Command *cmd)
{
    struct info *info;
    int i;
    int procId;
    String name;

    info = &cmd->args.info;

    printf("cluster size = %d\n", info->size);

    for (i = 0; i < info->size; i++) {
        procId = info->baseId + i;
        name = info->names[i];
        printf("ProcId %d = \"%s\"\n", procId, name);
    }
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Sensor.c ========
 */

/* Standard headers */
#include <assert.h>             /* assert */
#include <errno.h>
#include <pthread.h>            /* mutex */
#include <stdio.h>
#include <stdlib.h>             /* malloc */
#include <unistd.h>
#include <sys/eventfd.h>        /* event object */
#include <sys/queue.h>          /* LIST macros */

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* example header files */
#include "Sensor.h"
#include "../shared/ClusterMgr_shared.h"
#include "../shared/Data.h"
#include "../shared/Graph.h"
#include "../shared/SysCfg.h"

#define HEAPID  0

#define Event_DONE      (1 << 0)
#define Event_SHUTDOWN  (1 << 1)
#define Event_START     (1 << 2)


TAILQ_HEAD(DataList, Sensor_BufDesc);

/* module structure */
typedef struct {
    Bool                ready;          /* module is ready for use */
    Int                 refCount;       /* reference count */
    pthread_mutex_t     gate;           /* module gate */
    Bool                run;            /* main loop control */
    Bool                dataOn;         /* data flow flag */
    /* TODO make this into a LIST object */
    struct DataList     empty;          /* list of empty buffers */
    struct DataList     full;           /* queue of full buffers */
    int                 availCount;     /* counting semaphore (fd) */
    int                 waitEvent;      /* event object */
} Sensor_Module;

/* private functions */
static int Sensor_exec(void);

/* private data */
static Sensor_Module Sensor_mod = {
    .ready      = FALSE,
    .refCount   = 0,
    .gate       = PTHREAD_MUTEX_INITIALIZER,
    .run        = FALSE,
    .dataOn     = FALSE,
    .availCount = -1,
    .waitEvent = -1
};


/*
 *  ======== Sensor_threadFxn ========
 */
void *Sensor_threadFxn(void *arg)
{
    int status = 0;

    /* setup phase */
    status = Sensor_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Sensor_exec();

leave:
    /* shutdown phase */
    Sensor_destroy();

    return ((void *)status);
}

/*
 *  ======== Sensor_done ========
 */
void Sensor_done(void)
{
    uint64_t value = Event_DONE;

    write(Sensor_mod.waitEvent, &value, sizeof(value));
}

/*
 *  ======== Sensor_isReady ========
 */
int Sensor_isReady(void)
{
    return (Sensor_mod.ready ? 1 : 0);
}

/*
 *  ======== Sensor_issue ========
 */
void Sensor_issue(Sensor_BufDesc *desc)
{
    pthread_mutex_lock(&Sensor_mod.gate);
    TAILQ_INSERT_HEAD(&Sensor_mod.empty, desc, link);
    pthread_mutex_unlock(&Sensor_mod.gate);
}

/*
 *  ======== Sensor_recall ========
 */
void Sensor_recall(Sensor_BufDesc **pdesc)
{
    /* same work as reclaim, but different semantics */
    Sensor_reclaim(pdesc);
}

/*
 *  ======== Sensor_reclaim ========
 */
void Sensor_reclaim(Sensor_BufDesc **pdesc)
{
    uint64_t count;
    Sensor_BufDesc *desc;

    /* wait on counting semaphore until there is an available buffer */
    read(Sensor_mod.availCount, &count, sizeof(count));

    pthread_mutex_lock(&Sensor_mod.gate);

    /* remove next full buffer */
    desc = Sensor_mod.full.tqh_first;
    TAILQ_REMOVE(&Sensor_mod.full, desc, link);

    pthread_mutex_unlock(&Sensor_mod.gate);

    /* return buffer */
    *pdesc = desc;
}

/*
 *  ======== Sensor_shutdown ========
 */
void Sensor_shutdown(void)
{
    uint64_t value = Event_SHUTDOWN;

    Sensor_mod.run = FALSE;
    write(Sensor_mod.waitEvent, &value, sizeof(value));
}

/*
 *  ======== Sensor_start ========
 */
void Sensor_start(void)
{
    uint64_t value = Event_START;

    Sensor_mod.dataOn = TRUE;
    write(Sensor_mod.waitEvent, &value, sizeof(value));
}

/*
 *  ======== Sensor_stop ========
 */
void Sensor_stop(void)
{
    Sensor_mod.dataOn = FALSE;
}

/*
 *  ======== Sensor_destroy ========
 */
void Sensor_destroy(void)
{
    pthread_mutex_lock(&Sensor_mod.gate);

    /* ensure only last thread performs stop procedure */
    if (--Sensor_mod.refCount > 0) {
        goto leave;
    }

    /* close the event objects */
    close(Sensor_mod.waitEvent);
    close(Sensor_mod.availCount);

leave:
    pthread_mutex_unlock(&Sensor_mod.gate);
    return;
}

/*
 *  ======== Sensor_exec ========
 */
static int Sensor_exec(void)
{
    int status = 0;
    int emptyBuf;
    int i;
    uint64_t value;
    Sensor_BufDesc *desc;

    /* wait here until the application buffers have been submitted */
    read(Sensor_mod.waitEvent, &value, sizeof(value));

    /* never got started, shutdown requested */
    if (value & Event_SHUTDOWN) {
        goto transfer;
    }

    /* prime the pipeline */
    pthread_mutex_lock(&Sensor_mod.gate);
    desc = Sensor_mod.empty.tqh_first;
    TAILQ_REMOVE(&Sensor_mod.empty, desc, link);
    pthread_mutex_unlock(&Sensor_mod.gate);

    Sensor_mod.run = TRUE;

    /* ==== main loop ==== */
    while (Sensor_mod.run) {

        /* wait for start command */
        if (!Sensor_mod.dataOn) {
            read(Sensor_mod.waitEvent, &value, sizeof(value));

            if (value & Event_SHUTDOWN) {
                /* todo */
            }
            continue;
        }

        /*  ==== simulate the time it takes to collect the data ==== */
        usleep(200000);  /* 0.2 seconds */

        for (i = 0; i < desc->size; i++) {
            desc->buffer[i] = 0x10002000;
        }
        desc->count = i;

        /* enter the data gate */
        pthread_mutex_lock(&Sensor_mod.gate);

        /*  ==== place the sensor buffer on full list ====
         *
         *  If the buffer came from the empty list, then set flag
         *  to increment available count.
         */
        TAILQ_INSERT_TAIL(&Sensor_mod.full, desc, link);

        /*  ==== acquire a new buffer to fill ====
         *
         *  Take from the empty buffer list if available. Otherwise,
         *  recycle an unused buffer from the full list. The buffer
         *  at the head of the queue is the oldest.
         */
        if ((desc = Sensor_mod.empty.tqh_first) != NULL) {
            TAILQ_REMOVE(&Sensor_mod.empty, desc, link);
            emptyBuf = TRUE;
        }
        else {
            desc = Sensor_mod.full.tqh_first;
            TAILQ_REMOVE(&Sensor_mod.full, desc, link);
            emptyBuf = FALSE;
        }

        /* leave the data gate */
        pthread_mutex_unlock(&Sensor_mod.gate);

        /* increment the semaphore if buffer came from empty list */
        if (emptyBuf) {
            value = 1;
            write(Sensor_mod.availCount, &value, sizeof(value));
        }
    }

    pthread_mutex_lock(&Sensor_mod.gate);

    /* put unused buffer onto full list */
    desc->count = 0;
    TAILQ_INSERT_HEAD(&Sensor_mod.full, desc, link);
    value = 1;
    write(Sensor_mod.availCount, &value, sizeof(value));
    pthread_mutex_unlock(&Sensor_mod.gate);

transfer:
    pthread_mutex_lock(&Sensor_mod.gate);

    /* transfer all remaining buffers from the empty list to the full list */
    while ((desc = Sensor_mod.empty.tqh_first) != NULL) {
        desc->count = 0;
        TAILQ_REMOVE(&Sensor_mod.empty, desc, link);
        TAILQ_INSERT_HEAD(&Sensor_mod.full, desc, link);
        value = 1;
        write(Sensor_mod.availCount, &value, sizeof(value));
    }

    pthread_mutex_unlock(&Sensor_mod.gate);

    /* wait until the client is done, needs to reclaim buffers */
    read(Sensor_mod.waitEvent, &value, sizeof(value));

    return (status);
}

/*
 *  ======== Sensor_setup ========
 */
int Sensor_setup(void)
{
    int status = 0;

    pthread_mutex_lock(&Sensor_mod.gate);

    /* ensure only first thread performs setup procedure */
    if (Sensor_mod.refCount >= 1) {
        Sensor_mod.refCount++;
        status = 1;  /* already setup */
        goto leave;
    }

    /* initialize module state */
    TAILQ_INIT(&Sensor_mod.empty);
    TAILQ_INIT(&Sensor_mod.full);

    /* create counting semaphore, initial value = 0 */
    Sensor_mod.availCount = eventfd(0, EFD_SEMAPHORE);

    if (Sensor_mod.availCount == -1) {
        perror("Sensor_setup: create eventfd failed");
        status = -1;
        goto leave;
    }

    /* create event object, initial value = un-signaled  */
    Sensor_mod.waitEvent = eventfd(0, 0);

    if (Sensor_mod.waitEvent == -1) {
        perror("Sensor_setup: create eventfd failed");
        status = -1;
        goto leave;
    }

    /* getting here means we have successfully started */
    Sensor_mod.refCount++;

    /* module is ready */
    Sensor_mod.ready = TRUE;

leave:
    pthread_mutex_unlock(&Sensor_mod.gate);
    return (status);
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Sensor.h ========
 */

#ifndef Sensor__include
#define Sensor__include

#include <sys/queue.h>

#if defined (__cplusplus)
extern "C" {
#endif


typedef struct Sensor_BufDesc {
    int                 size;           /* size of buffer (in elements) */
    int                 count;          /* number of elements in buffer */
    unsigned int       *buffer;         /* buffer address */
    TAILQ_ENTRY(Sensor_BufDesc) link;
} Sensor_BufDesc;


void *Sensor_threadFxn(void *arg);
int  Sensor_setup(void);
void Sensor_destroy(void);

void Sensor_done(void);
int  Sensor_isReady(void);
void Sensor_issue(Sensor_BufDesc *desc);
void Sensor_recall(Sensor_BufDesc **desc);
void Sensor_reclaim(Sensor_BufDesc **desc);
void Sensor_shutdown(void);
void Sensor_start(void);
void Sensor_stop(void);


#if defined (__cplusplus)
}
#endif
#endif

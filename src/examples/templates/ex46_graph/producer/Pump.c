/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Pump.c ========
 */

/* Standard headers */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/eventfd.h>                /* eventfd */
#include <sys/stat.h>
#include <sys/types.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>

/* example header files */
#include "Pump.h"
#include "Sensor.h"
#include "Producer.h"
#include "../shared/ClusterMgr_shared.h"
#include "../shared/Graph.h"
#include "../shared/Data.h"
#include "../shared/SysCfg.h"


#define HEAPID  0

#define SENSOR_POOLSZ   3               /* sensor message pool, min = 3 */
#define PIPELINE_POOLSZ 1               /* message pipeline pool, min = 1 */
#define DESC_POOLSZ     (SENSOR_POOLSZ + PIPELINE_POOLSZ)

/* data sink context */
typedef struct {
    UInt16              procId;
    MessageQ_QueueIndex qidx;           /* queue index value */
    MessageQ_QueueId    dstQue;         /* data-sink message queue */
    Int                 transId;        /* message transport ID */
} DataSink;

TAILQ_HEAD(DescList, Sensor_BufDesc);

/* module structure */
typedef struct {
    Char                node;           /* node name */
    Bool                run;            /* main loop run flag */
    Bool                dataOn;         /* data flowing flag */
    Bool                okayToSignal;   /* indicates eventfd is created */
    Bool                ipcReady;       /* IPC ready flag */
    int                 waitEvent;
    MessageQ_QueueIndex queIndex;       /* reserved queue index */
    MessageQ_Handle     queue;          /* return message queue */
    MessageQ_QueueId    cmQue;          /* cluster manager message queue */
    struct DescList     descList;       /* list of available descriptors */
    DataSink            sink;           /* data-sink object */
    unsigned int        dataBlockSN;    /* data block serial number */
} Pump_Module;

/* private functions */
static void Pump_destroy(void);
static int Pump_exec(void);
static void Pump_sendInfoMsg(void);
static int Pump_setup(Pump_Arg *arg);

static Sensor_BufDesc   bufDescPool[DESC_POOLSZ];

/* private data */
static Pump_Module Pump_mod = {
    .node           = '-',
    .run            = FALSE,
    .dataOn         = FALSE,
    .okayToSignal   = FALSE,
    .ipcReady       = FALSE,
    .waitEvent      = -1,
    .queue          = NULL,
    .cmQue          = MessageQ_INVALIDMESSAGEQ,
    .sink.procId    = MultiProc_INVALIDID,
    .sink.qidx      = MessageQ_INVALIDMESSAGEQ,
    .sink.dstQue    = MessageQ_INVALIDMESSAGEQ,
    .sink.transId   = 0,
    .dataBlockSN    = 0
};

/*
 *  ======== Pump_threadFxn ========
 */
void *Pump_threadFxn(void *param)
{
    int status = 0;
    Pump_Arg *arg = (Pump_Arg *)param;

    /* setup phase */
    status = Pump_setup(arg);

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Pump_exec();

leave:
    /* shutdown phase */
    Pump_destroy();

    return ((void *)status);
}

/*
 *  ======== Pump_closeDataSink ========
 */
int Pump_closeDataSink(void)
{
    int status = 0;

    MessageQ_close(&Pump_mod.sink.dstQue);

    Pump_mod.sink.procId = MultiProc_INVALIDID;
    Pump_mod.sink.qidx = MessageQ_INVALIDMESSAGEQ;
    Pump_mod.sink.transId = 0;

    return (status);
}

/*
 *  ======== Pump_go ========
 */
void Pump_go(void)
{
    Pump_mod.dataOn = TRUE;
    Sensor_start();
}

/*
 *  ======== Pump_ipcDone ========
 */
int Pump_ipcDone(void)
{
    return (Pump_mod.ipcReady ? 0 : 1);
}

/*
 *  ======== Pump_ipcIsReady ========
 *  Inform this module that IPC is ready
 *
 *  This function is called from another execution context. Must
 *  be careful to ensure local resources have been created.
 */
void Pump_ipcIsReady(void)
{
    uint64_t value = 1;

    Pump_mod.ipcReady = TRUE;

    /* event might not yet exist, must check if okay to signal */
    if (Pump_mod.okayToSignal) {
        write(Pump_mod.waitEvent, &value, sizeof(value));
    }
}

/*
 *  ======== Pump_openDataSink ========
 */
int Pump_openDataSink(UInt16 procId, UInt16 queueIndex)
{
    int status = 0;

    Pump_mod.sink.procId = procId;
    Pump_mod.sink.qidx = queueIndex;

    /* determine which transport to use */
    if (procId == MultiProc_getId("HOST")) {
        Pump_mod.sink.transId = SysCfg_TRANSPORT_PMQ_ID;
    }
    else {
        Pump_mod.sink.transId = 0;
    }

    /* open the destination message queue (reserved ) */
    Pump_mod.sink.dstQue = MessageQ_openQueueId(queueIndex, procId);

    return (status);
}

/*
 *  ======== Pump_shutdown ========
 */
void Pump_shutdown(void)
{
    Pump_mod.run = FALSE;
}

/*
 *  ======== Pump_stop ========
 */
void Pump_stop(void)
{
    Sensor_stop();
    Pump_mod.dataOn = FALSE;
}

/*
 *  ======== Pump_destroy ========
 */
static void Pump_destroy(void)
{
    int status;
    int i;
    MessageQ_Msg mqMsg;
    Sensor_BufDesc *bufDesc;
    Data_Buffer *dataBuf;

    /* reclaim messages from the pipeline */
    for (i = 1; i <= PIPELINE_POOLSZ; i++) {
        status = MessageQ_get(Pump_mod.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            Pump_mod.run = FALSE;
            status = 0;
            goto leave;
        }
        else if (status < 0) {
            fprintf(stderr, "Error: Pump_exec: message receive failed, "
                    "error=%d\n", status);
            goto leave;
        }
        MessageQ_free(mqMsg);
    }

    /* reclaim messages from the sensor buffer pool */
    for (i = 1; i <= SENSOR_POOLSZ; i++) {

        /* recall buffer from sensor */
        Sensor_recall(&bufDesc);

        /* unbind descriptor from buffer */
        dataBuf = Data_DBADDR(bufDesc->buffer);
        mqMsg = Data_MQADDR(dataBuf);
        bufDesc->buffer = NULL;
        bufDesc->size = 0;
        bufDesc->count = 0;

        /* put buffer descriptor back onto the available list */
        TAILQ_INSERT_HEAD(&Pump_mod.descList, bufDesc, link);

        /* free the buffer */
        MessageQ_free(mqMsg);
    }

    /* tell sensor module we are done with it */
    Sensor_done();

    /* delete the message queue */
    MessageQ_delete(&Pump_mod.queue);

    /* done using IPC */
    Pump_mod.ipcReady = FALSE;

    /* close wait event */
    close(Pump_mod.waitEvent);

leave:
    return;
}

/*
 *  ======== Pump_exec ========
 */
static int Pump_exec(void)
{
    Int status;
    MessageQ_Msg mqMsg;
    Data_Buffer *dataBuf;
    Sensor_BufDesc *bufDesc;

    /* start the sensor running */
    Pump_mod.run = TRUE;

    /* main loop */
    while (Pump_mod.run) {

        /* wait for go command */
        if (!Pump_mod.dataOn) {
            usleep(500000);  /* 0.5 seconds */
            continue;
        }

        /* ensure we have a valid connection */
        if (Pump_mod.sink.dstQue == MessageQ_INVALIDMESSAGEQ) {
            /* TODO raise error */
            continue;
        }

        /* wait for return message */
        status = MessageQ_get(Pump_mod.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            Pump_mod.run = FALSE;
            status = 0;
            goto leave;
        }
        else if (status < 0) {
            fprintf(stderr, "Error: Pump_exec: message receive failed, "
                    "error=%d\n", status);
            goto leave;
        }

        /* get next available buffer descriptor */
        bufDesc = Pump_mod.descList.tqh_first;
        TAILQ_REMOVE(&Pump_mod.descList, bufDesc, link);

        /* bind descriptor to empty buffer */
        dataBuf = &((Data_Msg *)mqMsg)->data;
        bufDesc->buffer = dataBuf->buffer;
        bufDesc->size = Data_BUFSZ;
        bufDesc->count = 0;

        /* submit empty buffer to sensor */
        Sensor_issue(bufDesc);

        /* request full buffer from sensor module */
        Sensor_reclaim(&bufDesc);
        Pump_mod.dataBlockSN++;

        /* update data buffer with number of elements returned */
        dataBuf = Data_DBADDR(bufDesc->buffer);
        dataBuf->count = bufDesc->count;

        /* unbind descriptor from buffer */
        bufDesc->buffer = NULL;
        bufDesc->size = 0;
        bufDesc->count = 0;

        /* put buffer descriptor back onto the available list */
        TAILQ_INSERT_HEAD(&Pump_mod.descList, bufDesc, link);

        /* send data message to destination queue for processing */
        mqMsg = Data_MQADDR(dataBuf);
        MessageQ_setMsgId(mqMsg, Data_MSGID);
        MessageQ_setTransportId(mqMsg, Pump_mod.sink.transId);
        MessageQ_setReplyQueue(Pump_mod.queue, mqMsg);

        status = MessageQ_put(Pump_mod.sink.dstQue, mqMsg);

        if (status < 0) {
            fprintf(stderr, "Error: message send failed\n");
            /* TODO */
            continue;
        }

        /* send info message to cluster manager */
        if ((Pump_mod.dataBlockSN % 10) == 0) {
            Pump_sendInfoMsg();
        }
    }

    /* shutdown the sensor */
    Sensor_shutdown();

leave:
    return (status);
}

/*
 *  ======== Pump_sendInfoMsg ========
 */
static void Pump_sendInfoMsg(void)
{
    int status;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    mqMsg = MessageQ_alloc(HEAPID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        /* TODO */
        goto leave;
    }

    MessageQ_setMsgId(mqMsg, Graph_MSGID);
    MessageQ_setTransportId(mqMsg, SysCfg_TRANSPORT_PMQ_ID);

    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_INFO;
    msg->dcid = 0;
    msg->reply = FALSE;
    msg->arg1 = (Int)Pump_mod.node;
    msg->uarg1 = Pump_mod.dataBlockSN;

    status = MessageQ_put(Pump_mod.cmQue, mqMsg);

    if (status < 0) {
        /* TODO */
        goto leave;
    }

leave:
    return;
}

/*
 *  ======== Pump_setup ========
 */
static int Pump_setup(Pump_Arg *arg)
{
    int status = 0;
    int i;
    Data_Buffer *data;
    uint64_t value;
    Sensor_BufDesc *bufDesc;
    MessageQ_Msg mqMsg;
    MessageQ_QueueId qid;
    MessageQ_Params msgqParams;

    /* ensure dependencies are ready */
    while (!Sensor_isReady()) {
        usleep(60000); /* 60 milliseconds */
    }

    /* initialize module state */
    Pump_mod.node = arg->node;
    Pump_mod.queIndex = arg->queIdx;

    /* put buffer descriptors onto the available list */
    for (i = 0; i < DESC_POOLSZ; i++) {
        bufDesc = &bufDescPool[i];
        TAILQ_INSERT_HEAD(&Pump_mod.descList, bufDesc, link);
    }

    /* create event object */
    Pump_mod.waitEvent = eventfd(0, 0);

    if (Pump_mod.waitEvent == -1) {
        perror("Pump_setup: create eventfd failed");
        status = -1;
        goto leave;
    }

    Pump_mod.okayToSignal = TRUE;

    /* wait here until IPC is ready */
    if (!Pump_mod.ipcReady) {
        read(Pump_mod.waitEvent, &value, sizeof(value));
    }

    /* create reserved message queue (return messages) */
    MessageQ_Params_init(&msgqParams);
    msgqParams.queueIndex = Pump_mod.queIndex;

    Pump_mod.queue = MessageQ_create(NULL, &msgqParams);

    if (Pump_mod.queue == NULL) {
        status = -2;
        fprintf(stderr, "Error: message queue create failed\n");
        goto leave;
    }

    /* open the cluster manager message queue */
    do {
        status = MessageQ_open(ClusterMgr_CMDQUE, &Pump_mod.cmQue);

        if (status == MessageQ_E_NOTFOUND) {
            usleep(60000); /* 60 milliseconds */
        }
    } while (status == MessageQ_E_NOTFOUND);

    if (status < 0) {
        fprintf(stderr, "Error: cannot open cluster manager queue, name=%s "
                "error=%d\n", ClusterMgr_CMDQUE, status);
        goto leave;
    }

    /* allocate messages for the sensor buffer pool */
    for (i = 1; i <= SENSOR_POOLSZ; i++) {
        mqMsg = MessageQ_alloc(HEAPID, sizeof(Data_Msg));

        if (mqMsg == NULL) {
            fprintf(stderr, "Error: message alloc failed\n");
            status = -1;
            goto leave;
        }

        /* get next available buffer descriptor */
        bufDesc = Pump_mod.descList.tqh_first;
        TAILQ_REMOVE(&Pump_mod.descList, bufDesc, link);

        /* bind descriptor to empty buffer */
        data = &((Data_Msg *)mqMsg)->data;
        bufDesc->size = Data_BUFSZ;
        bufDesc->buffer = data->buffer;
        bufDesc->count = 0;  /* empty buffer */

        /* submit empty buffer to sensor */
        Sensor_issue(bufDesc);
    }

    /* allocate messages to prime the pipeline */
    qid = MessageQ_getQueueId(Pump_mod.queue);

    for (i = 1; i <= PIPELINE_POOLSZ; i++) {
        mqMsg = MessageQ_alloc(HEAPID, sizeof(Data_Msg));

        if (mqMsg == NULL) {
            fprintf(stderr, "Error: message alloc failed\n");
            status = -1;
            goto leave;
        }

        MessageQ_put(qid, mqMsg);
    }

leave:
    return (status);
}

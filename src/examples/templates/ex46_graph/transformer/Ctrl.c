/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Ctrl.c ========
 */

/* this define must precede inclusion of any xdc header file */
#define Registry_CURDESC Ctrl__Desc
#define MODULE_NAME "Ctrl"

/* xdctools header files */
#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/System.h>

/* package header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/ipc/HeapBufMP.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/SharedRegion.h>
#include <ti/ipc/ipcmgr/IpcMgr.h>

/* example header files */
#include "Ctrl.h"
#include "Xfm.h"
#include "../shared/Graph.h"
#include "../shared/SysCfg.h"

#define NAMESZ 48


/* module structure */
typedef struct {
    Bool                run;            /* main loop run flag */
    UInt16              srZeroProc;     /* SR-Zero owner processor ID */
    MessageQ_QueueId    srZeroQue;      /* SR-Zero owner control queue */
    MessageQ_Handle     queue;          /* inbound messages */
    HeapBufMP_Handle    heap;           /* control message pool */
} Ctrl_Module;

/* private functions */
static void Ctrl_destroy(void);
static void Ctrl_detachSRZero(void);
static int Ctrl_exec(void);
static int Ctrl_setup(void);

/* private data */
Registry_Desc Registry_CURDESC;

static Ctrl_Module Ctrl_mod = {
    FALSE,                      /* run */
    MultiProc_INVALIDID,        /* srZeroProcId */
    NULL,                       /* srZeroQue */
    NULL,                       /* queue */
    NULL                        /* heap */
};


/*
 *  ======== Ctrl_taskFxn ========
 */
Void Ctrl_taskFxn(UArg arg0, UArg arg1)
{
    int status = 0;

    Log_print0(Diags_INFO, "Ctrl_taskFxn: -->");

    /* setup phase */
    status = Ctrl_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Ctrl_exec();

leave:
    /* shutdown phase */
    Ctrl_destroy();

    Log_print1(Diags_INFO, "Ctrl_threadFxn: <-- status=%d", (IArg)status);
}

/*
 *  ======== Ctrl_destroy ========
 */
static void Ctrl_destroy(void)
{
    int status = 0;

    /* wait until the xfm module is finished using IPC */
    while (!Xfm_ipcDone()) {
        Task_sleep(100);  /* 0.1 seconds */
    }

    /* if not SR-Zero owner, close its message queue */
    if (MultiProc_self() != Ctrl_mod.srZeroProc) {
        MessageQ_close(&Ctrl_mod.srZeroQue);
    }

    /* delete my own message queue */
    MessageQ_delete(&Ctrl_mod.queue);

    /* unregister the message heap */
    MessageQ_unregisterHeap(SysCfg_CtrlMsg_HeapID);

    /* if SR-Zero owner, delete the message heap */
    if (MultiProc_self() == Ctrl_mod.srZeroProc) {
        HeapBufMP_delete(&Ctrl_mod.heap);
    }
    else {
        /* else, close the message heap */
        HeapBufMP_close(&Ctrl_mod.heap);
    }

    /*  If not SR-Zero owner, must detach now.
     *
     *  We have already aranged for SR-Zero to be in the Ipc_detach
     *  loop (see Ctrl_detachSRZero). All IPC resources have now been
     *  release, so we finally enter the detach loop here.
     */
    if (MultiProc_self() != Ctrl_mod.srZeroProc) {
        do {
            status = Ipc_detach(Ctrl_mod.srZeroProc);

            if (status == Ipc_E_NOTREADY) {
                Task_sleep(1);
            }
        } while (status == Ipc_E_NOTREADY);
    }

    /* finalize IPC */
    Ipc_stop();

    /* disable log events */
    Log_print1(Diags_INFO, "Ctrl_delete: <-- status=%d", (IArg)status);
    Diags_setMask(MODULE_NAME"-FEX");

    /*
     * Note that there isn't a Registry_removeModule() yet:
     *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=315448
     *
     * ... but this is where we'd call it.
     */
}

/*
 *  ======== Ctrl_detachSRZero ========
 */
static void Ctrl_detachSRZero(void)
{
    int status;
    MessageQ_Msg mqMsg;
    Graph_Msg *msg;

    /* allocate a message queue message to contain the graph message */
    mqMsg = MessageQ_alloc(SysCfg_CtrlMsg_HeapID, sizeof(Graph_Msg));

    if (mqMsg == NULL) {
        Log_error0("message alloc failed");
        goto leave;
    }

    /* identify the message type */
    MessageQ_setMsgId(mqMsg, Graph_MSGID);

    /* fill in message payload */
    msg = (Graph_Msg *)mqMsg;
    msg->cmd = Graph_Cmd_DETACH;
    msg->uarg1 = MultiProc_self();
    msg->reply = FALSE;

    /* send the message */
    status = MessageQ_put(Ctrl_mod.srZeroQue, mqMsg);

    if (status < 0) {
        Log_error0("message send failed");
        goto leave;
    }

    /*  SR-Zero owner will now go into Ipc_detach loop. However, before
     *  this processor can detach, it must first release all IPC resources.
     *  This is done in Ctrl_destory.
     */

    Log_print1(Diags_INFO, "Ctrl_detachSRZero: IPC detached from proc=%d",
            Ctrl_mod.srZeroProc);

leave:
    return;
}

/*
 *  ======== Ctrl_exec ========
 */
static int Ctrl_exec(void)
{
    int status = 0;
    MessageQ_Msg mqMsg;
    MessageQ_QueueId qid;
    Graph_Msg *msg;

    Log_print0(Diags_INFO, "Ctrl_exec: -->");
    Ctrl_mod.run = TRUE;

    /* main loop */
    while (Ctrl_mod.run) {

        /* wait for inbound message */
        Log_print0(Diags_INFO, "waiting for message");
        status = MessageQ_get(Ctrl_mod.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            Ctrl_mod.run = FALSE;
            status = 0;
            goto leave;
        }
        else if (status < 0) {
            Log_error1("Ctrl_exec: message get error=%d", status);
            goto leave;
        }

        /* process the message */
        msg = (Graph_Msg *)mqMsg;
        Log_print1(Diags_INFO, "message received, cmd=%d", msg->cmd);

        switch (msg->cmd) {

            case Graph_Cmd_ATTACH:
                /* dsp attach to dsp */
                do {
                    status = Ipc_attach(msg->uarg1);

                    if (status == Ipc_E_NOTREADY) {
                        Task_sleep(1);
                    }
                } while (status == Ipc_E_NOTREADY);

                if (status < 0) {
                    Log_error1("Ipc_attach failed, error=%d", status);
                    status = -1;
                    goto leave;
                }

                Log_print1(Diags_INFO, "IPC attached proc=%d", msg->uarg1);
                break;

            case Graph_Cmd_DETACH:
                do {
                    status = Ipc_detach(msg->uarg1);

                    if (status == Ipc_E_NOTREADY) {
                        Task_sleep(1);
                    }
                } while (status == Ipc_E_NOTREADY);

                if (status < 0) {
                    Log_error1("Ipc_detach failed, error=%d", status);
                    status = -1;
                    goto leave;
                }

                Log_print1(Diags_INFO, "IPC detached proc=%d", msg->uarg1);
                break;

            case Graph_Cmd_FLUSH:
                msg->status = Graph_S_SUCCESS; /* nothing to do */
                break;

            case Graph_Cmd_INPUTCONNECT:
                msg->status = Graph_S_SUCCESS;
                break;

            case Graph_Cmd_INPUTDISCONNECT:
                msg->status = Graph_S_SUCCESS;
                break;

            case Graph_Cmd_OUTPUTCONNECT:
                Log_print2(Diags_INFO, "connect output to proc=%d qidx=%d",
                        msg->uarg1, msg->uarg2);
                Xfm_connect(msg->uarg1, msg->uarg2);
                msg->status = Graph_S_SUCCESS;
                break;

            case Graph_Cmd_OUTPUTDISCONNECT:
                Log_print2(Diags_INFO, "disconnect output proc=%d qidx=%d",
                        msg->uarg1, msg->uarg2);
                Xfm_disconnect(msg->uarg1, msg->uarg2);
                msg->status = Graph_S_SUCCESS;
                break;

            case Graph_Cmd_SHUTDOWN:
                Log_print0(Diags_INFO, "shutdown message received");
                Xfm_shutdown();
                Ctrl_mod.run = FALSE;  /* stop the idle function */

                /* if SR-Zero owner, give idle function a chance to unwind */
                /* TODO - this should be a do-while loop */
                if (MultiProc_self() == Ctrl_mod.srZeroProc) {
                    Task_sleep(1);
                }
                else {
                    /* send detach request to SR-Zero owner */
                    Ctrl_detachSRZero();
                }
                break;

            default:
                Log_error1("unknown command, cmd=%d", (IArg)msg->cmd);
                break;
        }

        if (msg->reply) {
            /* return the message to sender */
            msg->reply = FALSE;
            msg->ack = TRUE;
            qid = MessageQ_getReplyQueue(mqMsg);

            status = MessageQ_put(qid, mqMsg);

            if (status < 0) {
                Log_error0("Ctrl_exec: message send failed");
                status = -2;
                goto leave;
            }
        }
        else {
            /* free message here */
            MessageQ_free(mqMsg);
        }

    } /* while (run) */

leave:
    Log_print1(Diags_INFO, "Ctrl_exec: <-- %d", (IArg)status);
    return (status);
}

/*
 *  ======== Ctrl_setup ========
 */
static int Ctrl_setup(void)
{
    Int status = 0;
    Error_Block eb;
    Char cbuf[NAMESZ];
    Bool ready;
    Registry_Result result;
    MessageQ_Params msgqParams;
    SharedRegion_Entry entry;
    HeapBufMP_Params heapParams;

    Error_init(&eb);

    /* register with xdc.runtime to get a diags mask */
    result = Registry_addModule(&Registry_CURDESC, MODULE_NAME);

    if ((result != Registry_SUCCESS) && (result != Registry_ALREADY_ADDED)) {
        status = -1;
        goto leave;
    }

    /* enable some trace */
    Diags_setMask(MODULE_NAME"+F");
    Log_print0(Diags_INFO, "Ctrl_setup: -->");

    /* setup IPC */
    Log_print0(Diags_INFO, "initializing IPC");

    /* setup TransportRpmsg for host communication */
    IpcMgr_ipcStartup();

    /* setup IPC for dsp to dsp communication */
    status = Ipc_start();

    if (status < 0) {
        Log_error0("Ipc_start failed");
        goto leave;
    }

    /* get SR-Zero information */
    SharedRegion_getEntry(0, &entry);
    Ctrl_mod.srZeroProc = entry.ownerProcId;

    /* if this processor is not owner, attach to SR-Zero owner */
    if (MultiProc_self() != Ctrl_mod.srZeroProc) {
        Log_print1(Diags_INFO, "attach to proc=%d", Ctrl_mod.srZeroProc);

        do {
            status = Ipc_attach(Ctrl_mod.srZeroProc);

            if (status == Ipc_E_NOTREADY) {
                Task_sleep(1);
            }
        } while (status == Ipc_E_NOTREADY);
    }

    System_snprintf(cbuf, sizeof(cbuf), SysCfg_CtrlHeapNameFmt,
            Ctrl_mod.srZeroProc);

    /* if SR-Zero owner, create the message heap (control messages) */
    if (MultiProc_self() == Ctrl_mod.srZeroProc) {
        HeapBufMP_Params_init(&heapParams);
        heapParams.name = cbuf;
        heapParams.regionId = 0;
        heapParams.blockSize = Graph_HEAPBLOCKSIZE;
        heapParams.numBlocks = SysCfg_CtrlHeapNumBlocks;

        Ctrl_mod.heap = HeapBufMP_create(&heapParams);

        if (Ctrl_mod.heap == NULL) {
            Log_error0("heap create failed");
            status = -3;
            goto leave;
        }
    }
    else {
        /* else, open the message heap */
        status = HeapBufMP_open(cbuf, &Ctrl_mod.heap);

        if (status < 0) {
            Log_error0("heap create failed");
            status = -3;
            goto leave;
        }
    }

    /* register heap with MessageQ */
    status = MessageQ_registerHeap(Ctrl_mod.heap, SysCfg_CtrlMsg_HeapID);

    if (status < 0) {
        Log_error0("heap already exists");
        status = -4;
        goto leave;
    }

    Log_print0(Diags_INFO, "IPC startup complete");

    /* compute the message queue name */
    System_snprintf(cbuf, sizeof(cbuf), Ctrl_QueNameFmt, MultiProc_self());

    /* create message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    Ctrl_mod.queue = MessageQ_create(cbuf, &msgqParams);

    if (Ctrl_mod.queue == NULL) {
        Log_error0("message queue create failed");
        goto leave;
    }

    /* if not SR-Zero owner, open its message queue (control messages) */
    if (MultiProc_self() != Ctrl_mod.srZeroProc) {
        System_snprintf(cbuf, sizeof(cbuf), Ctrl_QueNameFmt,
                Ctrl_mod.srZeroProc);

        do {
            status = MessageQ_open(cbuf, &Ctrl_mod.srZeroQue);

            if (status == MessageQ_E_NOTFOUND) {
                Task_sleep(5);
            }
        } while (status == MessageQ_E_NOTFOUND);

        if (status < 0) {
            Log_error0("message queue open failed");
        }
    }

    /* inform other modules that IPC is ready */
    Xfm_ipcIsReady();

    /* busy wait until other modules are ready */
    do {
        ready = TRUE;

        if (!Xfm_isReady()) {
            ready = FALSE;
        }

        /*  check other modules here
         *
         *  if (!<Mod>_isReady()) {
         *      ready = FALSE;
         *  }
         */

        if (!ready) {
            Task_sleep(10);
        }

    } while (!ready);

    Log_print0(Diags_INFO, "peer modules are ready");

leave:
    Log_print1(Diags_INFO, "Ctrl_setup: <-- %d", (IArg)status);
    return (status);
}

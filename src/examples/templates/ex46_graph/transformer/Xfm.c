/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Xfm.c ========
 */

/* this define must precede inclusion of any xdc header file */
#define Registry_CURDESC Xfm__Desc
#define MODULE_NAME "Xfm"

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/SharedRegion.h>

/* local header files */
#include "Xfm.h"
#include "../shared/Data.h"
#include "../shared/SysCfg.h"

#define MESSAGE_POOLSZ  4               /* message pool size */
#define PIPELINE_POOLSZ 2               /* message pipeline pool, min = 2 */
#define DESC_POOLSZ     MESSAGE_POOLSZ

typedef struct {
    Queue_Elem  link;
    Data_Msg   *msg;
} Xfm_Desc;

typedef struct {
    MessageQ_QueueIndex queueIndex;     /* reserved queue index */
    MessageQ_Handle     queue;          /* data queue, inbound messages */
    MessageQ_QueueId    myQID;          /* used to identify my messages */
} Xfm_Input;

typedef struct {
    UInt16              procId;         /* destination node processor ID    */
    UInt16              queueIndex;     /* destination queue index          */
    Int                 transId;        /* message transport ID             */
    MessageQ_QueueId    queue;          /* destination data queue           */
} Xfm_OutputNode;

/* module structure */
typedef struct {
    Bool                run;            /* main loop run flag               */
    volatile Bool       ipcReady;       /* IPC ready flag                   */
    Semaphore_Handle    ipcSem;         /* IPC synchronizing object         */
    Queue_Handle        descriptors;
    Queue_Handle        inputQue;
    Queue_Handle        outputQue;
    Ptr                 heapMem;        /* heap memory from SR-Zero         */
    SizeT               heapSize;
    HeapBuf_Handle      msgHeap;        /* message pool                     */
    Xfm_Input           input;          /* input end-point */
    Xfm_OutputNode      output;
    Semaphore_Struct    ipcSemObj;
    Queue_Struct        descQueObj;
    Queue_Struct        inputQueObj;
    Queue_Struct        outputQueObj;
} Xfm_Module;

/* private functions */
static void Xfm_destroy(void);
static int  Xfm_exec(void);
static int  Xfm_setup(void);
static void Xfm_transform(Data_Buffer *input, Data_Buffer *output);

/* private data */
Registry_Desc           Registry_CURDESC;
//static Int              Mod_curInit = 0;

static Xfm_Desc descPool[DESC_POOLSZ];

static Xfm_Module Xfm_mod = {
    FALSE,                          /* run                      */
    FALSE,                          /* ipcReady                 */
    NULL,                           /* ipcSem                   */
    NULL,                           /* descriptors              */
    NULL,                           /* inputQue                 */
    NULL,                           /* outputQue                */
    NULL,                           /* heapMem                  */
    0,                              /* heapSize                 */
    NULL,                           /* msgHeap                  */
    { SysCfg_DATA_RQUE,             /* input.queueIndex         */
      NULL,                         /* input.queue              */
      MessageQ_INVALIDMESSAGEQ },   /* input.myQID              */
    { MultiProc_INVALIDID,          /* output.procId            */
      MessageQ_INVALIDMESSAGEQ,     /* output.queueIndex        */
      -1,                           /* output.transId           */
      MessageQ_INVALIDMESSAGEQ }    /* output.queue             */
};


/*
 *  ======== Xfm_taskFxn ========
 */
Void Xfm_taskFxn(UArg arg0, UArg arg1)
{
    Int status = 0;

    Log_print0(Diags_INFO, "Xfm_taskFxn: -->");

    /* setup phase */
    status = Xfm_setup();

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Xfm_exec();

leave:
    /* shutdown phase */
    Xfm_destroy();

    Log_print1(Diags_INFO, "Xfm_taskFxn: <-- %d", (IArg)status);
}

/*
 *  ======== Xfm_connect ========
 */
void Xfm_connect(UInt16 procId, UInt16 queueIndex)
{
    /* save connection information */
    Xfm_mod.output.procId = procId;
    Xfm_mod.output.queueIndex = queueIndex;
    Xfm_mod.output.transId = 0;

    /* open the destination message queue (reserved ) */
    Xfm_mod.output.queue = MessageQ_openQueueId(queueIndex, procId);
}

/*
 *  ======== Xfm_disconnect ========
 */
void Xfm_disconnect(UInt16 procId, UInt16 queueIndex)
{
    Xfm_OutputNode *output = &Xfm_mod.output;

    /* if already disconnected, nothing left to do */
    if (output->queue == MessageQ_INVALIDMESSAGEQ) {
        return;
    }

    /* validate connection information */
    if ((output->procId != procId) || (output->queueIndex != queueIndex)) {
        return;
    }

    /* close destination message queue */
    MessageQ_close(&output->queue);

    /* reset connection information */
    output->procId = MultiProc_INVALIDID;
    output->queueIndex = MessageQ_INVALIDMESSAGEQ;
    output->transId = -1;
}

/*
 *  ======== Xfm_ipcDone ========
 */
int Xfm_ipcDone(void)
{
    return (Xfm_mod.ipcReady ? 0 : 1);
}

/*
 *  ======== Xfm_ipcIsReady ========
 *  Inform this module that IPC is ready
 *
 *  This function is called from another execution context. Must
 *  be careful to ensure local resources have been created.
 */
void Xfm_ipcIsReady(void)
{
    Xfm_mod.ipcReady = TRUE;

    /* semaphore might not yet exist, check create flag */
    if (Xfm_mod.ipcSem != NULL) {
        Semaphore_post(Xfm_mod.ipcSem);
    }
}

/*
 *  ======== Xfm_isReady ========
 */
Bool Xfm_isReady(void)
{
    return (Xfm_mod.run);
}

/*
 *  ======== Xfm_shutdown ========
 */
void Xfm_shutdown(void)
{
    Xfm_mod.run = FALSE;
    MessageQ_unblock(Xfm_mod.input.queue);
}

/*
 *  ======== Xfm_destroy ========
 */
static void Xfm_destroy(void)
{
    int msgCount;
    Xfm_Desc *desc;
    MessageQ_Msg mqMsg;
    IHeap_Handle heap;

    Log_print0(Diags_INFO, "Xfm_destroy: -->");

    /* close destination queue if needed */
    if (Xfm_mod.output.queue != MessageQ_INVALIDMESSAGEQ) {
        Xfm_disconnect(Xfm_mod.output.procId, Xfm_mod.output.queueIndex);
    }

    /* reclaim data messages */
    msgCount = 0;

    /* look for data messages on output queue */
    while (!Queue_empty(Xfm_mod.outputQue)) {
        desc = (Xfm_Desc *)Queue_dequeue(Xfm_mod.outputQue);
        mqMsg = (MessageQ_Msg)desc->msg;
        MessageQ_free(mqMsg);
        msgCount++;
    }

    if (msgCount != PIPELINE_POOLSZ) {
        Log_error1("Xfm_destroy: missing %d data messages",
                PIPELINE_POOLSZ - msgCount);
    }

    /* delete the local message queue */
    MessageQ_delete(&Xfm_mod.input.queue);
    Xfm_mod.input.myQID = MessageQ_INVALIDMESSAGEQ;

    /* finalize the message heap */
    MessageQ_unregisterHeap(SysCfg_DataMsg_HeapID);
    HeapBuf_delete(&Xfm_mod.msgHeap);

    /* free heap memory back to SR-Zero heap */
    heap = (IHeap_Handle)SharedRegion_getHeap(0);
    Memory_free(heap, Xfm_mod.heapMem, Xfm_mod.heapSize);

    /* done using IPC */
    Xfm_mod.ipcReady = FALSE;

    /* destruct the semaphore */
    Xfm_mod.ipcSem = NULL;
    Semaphore_destruct(&Xfm_mod.ipcSemObj);

    /* disable log events */
    Log_print0(Diags_INFO, "Xfm_delete: <--");
    Diags_setMask(MODULE_NAME"-FEX");

    /*
     * Note that there isn't a Registry_removeModule() yet:
     *     https://bugs.eclipse.org/bugs/show_bug.cgi?id=315448
     *
     * ... but this is where we'd call it.
     */
}

/*
 *  ======== Xfm_exec ========
 */
static int Xfm_exec(void)
{
    int status;
    MessageQ_Msg mqMsg;
    MessageQ_QueueId replyQID;
    Xfm_Desc *desc;
    Data_Msg *dataMsg;
    Data_Msg *inputDataMsg;
    Data_Msg *outputDataMsg;
    Data_Buffer *inputBuf;
    Data_Buffer *outputBuf;
    Bool inputReady = FALSE;
    Bool outputReady = FALSE;

    Log_print0(Diags_INFO, "Xfm_exec: -->");
    Xfm_mod.run = TRUE;

    /* main loop */
    while (Xfm_mod.run) {

        /* wait for inbound message */
        status = MessageQ_get(Xfm_mod.input.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            Xfm_mod.run = FALSE;
            status = 0;
            continue;
        }
        else if (status < 0) {
            Log_error1("Xfm_exec: message get error=%d", (IArg)status);
            goto leave;
        }

        /* get available descriptor */
        desc = (Xfm_Desc *)Queue_dequeue(Xfm_mod.descriptors);

        /* bind descriptor to data message */
        dataMsg = (Data_Msg *)mqMsg;
        desc->msg = dataMsg;

        /* determine message ownership */
        replyQID = MessageQ_getReplyQueue(mqMsg);

        /* if my buffer, put empty return buffer onto output queue */
        if (replyQID == Xfm_mod.input.myQID) {
            Queue_enqueue(Xfm_mod.outputQue, &desc->link);
            Log_print0(Diags_INFO, "received output buffer");
            outputReady = TRUE;
        }
        else {
            /* put new full buffer onto input queue */
            Queue_enqueue(Xfm_mod.inputQue, &desc->link);
            Log_print0(Diags_INFO, "received input buffer");
            inputReady = TRUE;
        }

        /* if both input and output buffers available, transform data */
        if (inputReady && outputReady) {

            /* remove next buffer from input queue */
            desc = (Xfm_Desc *)Queue_dequeue(Xfm_mod.inputQue);
            inputReady = (Queue_empty(Xfm_mod.inputQue) ? FALSE : TRUE);
            inputDataMsg = desc->msg;
            inputBuf = &inputDataMsg->data;

            /* recycle descriptor */
            desc->msg = NULL;
            Queue_enqueue(Xfm_mod.descriptors, &desc->link);

            /* remove next buffer from output queue */
            desc = (Xfm_Desc *)Queue_dequeue(Xfm_mod.outputQue);
            outputReady = (Queue_empty(Xfm_mod.outputQue) ? FALSE : TRUE);
            outputDataMsg = desc->msg;
            outputBuf = &outputDataMsg->data;

            /* recycle descriptor */
            desc->msg = NULL;
            Queue_enqueue(Xfm_mod.descriptors, &desc->link);

            /* transform input data to output buffer */
            Log_print0(Diags_INFO, "process data");
            Xfm_transform(inputBuf, outputBuf);

            mqMsg = (MessageQ_Msg)outputDataMsg;
            if (Xfm_mod.output.queue != MessageQ_INVALIDMESSAGEQ) {
                /* send the output buffer to the downsteam node */
                MessageQ_setTransportId(mqMsg, Xfm_mod.output.transId);
                MessageQ_put(Xfm_mod.output.queue, mqMsg);
            }
            else {
                /* recycle output buffer */
                MessageQ_put(Xfm_mod.input.myQID, mqMsg);
            }

            /* return input buffer to the upstream node */
            mqMsg = (MessageQ_Msg)inputDataMsg;
            replyQID = MessageQ_getReplyQueue(mqMsg);
            MessageQ_put(replyQID, mqMsg);
        }
    }

leave:
    Log_print1(Diags_INFO, "Xfm_exec: <-- %d", (IArg)status);
    return(status);
}

/*
 *  ======== Xfm_setup ========
 */
static int Xfm_setup(void)
{
    int status = 0;
    int i;
    Error_Block eb;
    Semaphore_Params semParams;
    Registry_Result result;
    IHeap_Handle heap;
    SizeT align;
    SizeT blockSize;
    HeapBuf_Params heapParams;
    MessageQ_Params msgqParams;
    MessageQ_Msg mqMsg;

    Error_init(&eb);

    /* register with xdc.runtime to get a diags mask */
    result = Registry_addModule(&Registry_CURDESC, MODULE_NAME);

    if ((result != Registry_SUCCESS) && (result != Registry_ALREADY_ADDED)) {
        status = -1;
        goto leave;
    }

    /* enable some trace */
    Diags_setMask(MODULE_NAME"+F");
    Log_print0(Diags_INFO, "Xfm_setup: -->");

    /* put descriptors onto the available list */
    Queue_construct(&Xfm_mod.descQueObj, NULL);
    Xfm_mod.descriptors = Queue_handle(&Xfm_mod.descQueObj);

    for (i = 0; i < DESC_POOLSZ; i++) {
        Queue_enqueue(Xfm_mod.descriptors, &(descPool[i].link));
    }

    /* construct the input and output holding queues */
    Queue_construct(&Xfm_mod.inputQueObj, NULL);
    Xfm_mod.inputQue = Queue_handle(&Xfm_mod.inputQueObj);
    Queue_construct(&Xfm_mod.outputQueObj, NULL);
    Xfm_mod.outputQue = Queue_handle(&Xfm_mod.outputQueObj);

    /* construct the semaphore */
    Semaphore_Params_init(&semParams);
    semParams.mode = Semaphore_Mode_BINARY;

    Semaphore_construct(&Xfm_mod.ipcSemObj, 0, &semParams);

    if (Error_check(&eb)) {
        status = -1;
        goto leave;
    }
    Xfm_mod.ipcSem = Semaphore_handle(&Xfm_mod.ipcSemObj);

    /* wait here until IPC is ready */
    if (!Xfm_mod.ipcReady) {
        Semaphore_pend(Xfm_mod.ipcSem, BIOS_WAIT_FOREVER);
    }
    Log_print0(Diags_INFO, "Xfm_setup: IPC ready");

    /* acquire heap memory from SR-Zero, pad the block size to alignment */
    heap = (IHeap_Handle)SharedRegion_getHeap(0);
    align = Memory_getMaxDefaultTypeAlign();
    blockSize = (sizeof(Data_Msg) + (align-1)) & ~(align-1);
    Xfm_mod.heapSize = MESSAGE_POOLSZ * blockSize;

    Xfm_mod.heapMem = Memory_alloc(heap, Xfm_mod.heapSize, align, &eb);

    if (Error_check(&eb)) {
        status = -1;
        goto leave;
    }

    /* create a heap for data message pool */
    HeapBuf_Params_init(&heapParams);
    heapParams.align = align;
    heapParams.blockSize = blockSize;
    heapParams.numBlocks = MESSAGE_POOLSZ;
    heapParams.buf = Xfm_mod.heapMem;
    heapParams.bufSize = Xfm_mod.heapSize;

    Xfm_mod.msgHeap = HeapBuf_create(&heapParams, &eb);

    if (Error_check(&eb)) {
        Log_error0("Xfm_setup: heap create failed");
        status = -1;
        goto leave;
    }

    /* register heap with MessageQ */
    status = MessageQ_registerHeap(Xfm_mod.msgHeap, SysCfg_DataMsg_HeapID);

    if (status < 0) {
        Log_error0("Xfm_setup: heap already exists");
        status = -1;
        goto leave;
    }

    /* create reserved message queue (data queue) */
    MessageQ_Params_init(&msgqParams);
    msgqParams.queueIndex = Xfm_mod.input.queueIndex;

    Xfm_mod.input.queue = MessageQ_create(NULL, &msgqParams);

    if (Xfm_mod.input.queue == NULL) {
        status = -1;
        Log_error0("Xfm_setup: message queue create failed");
        goto leave;
    }

    Xfm_mod.input.myQID = MessageQ_getQueueId(Xfm_mod.input.queue);

    /* allocate output buffers to prime the pipeline */
    for (i = 1; i <= PIPELINE_POOLSZ; i++) {
        mqMsg = MessageQ_alloc(SysCfg_DataMsg_HeapID, sizeof(Data_Msg));

        if (mqMsg == NULL) {
            Log_error0("Xfm_setup: message alloc failed");
            status = -1;
            goto leave;
        }

        /* put message onto my own input queue */
        MessageQ_setMsgId(mqMsg, Data_MSGID);
        MessageQ_setReplyQueue(Xfm_mod.input.queue, mqMsg);
        MessageQ_put(Xfm_mod.input.myQID, mqMsg);
    }

leave:
    Log_print1(Diags_INFO, "Xfm_setup: <-- status=%d", status);
    return (status);
}

/*
 *  ======== Xfm_transform ========
 */
static void Xfm_transform(Data_Buffer *input, Data_Buffer *output)
{
    int i;
    int count;
    UInt *inDatum, *outDatum;

    count = input->count;
    inDatum = input->buffer;
    outDatum = output->buffer;

    for (i = 0; i < count; i++) {
        *outDatum++ = *inDatum++;
    }

    output->count = count;
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Ctrl.c ========
 */

/* Standard headers */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/interfaces/ITransport.h>
#include <ti/ipc/interfaces/INetworkTransport.h>
#include <ti/ipc/transports/TransportRpmsg.h>

/* example header files */
#include <ex45_host/transport/hlos/TransportPMQ.h>
#include "Consumer.h"
#include "Ctrl.h"
#include "Sink.h"
#include "../shared/Graph.h"
#include "../shared/SysCfg.h"

#define BUFSZ 64

/* module structure */
typedef struct {
    Char                node;           /* node name */
    Bool                run;            /* main loop run flag */
    MessageQ_Handle     queue;          /* inbound messages */
} Ctrl_Module;

/* private functions */
void Ctrl_destroy(void);
int Ctrl_exec(void);
int Ctrl_setup(Ctrl_Arg *arg);

/* private data */
static Ctrl_Module Ctrl_mod = {
    .node       = '-',
    .run        = FALSE,
    .queue      = NULL
};


/*
 *  ======== Ctrl_threadFxn ========
 */
void *Ctrl_threadFxn(void *param)
{
    int status = 0;
    Ctrl_Arg *arg = (Ctrl_Arg *)param;

    /* setup phase */
    status = Ctrl_setup(arg);

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Ctrl_exec();

leave:
    /* shutdown phase */
    Ctrl_destroy();

    return ((void *)status);
}

/*
 *  ======== Ctrl_destroy ========
 */
void Ctrl_destroy(void)
{

    /* wait until the pump module is finished using IPC */
    while (!Sink_ipcDone()) {
        usleep(100000);  /* 0.1 seconds */
    }

    /* delete the message queue */
    MessageQ_delete(&Ctrl_mod.queue);

    /* finalize the secondary transport */
    MessageQ_unregisterTransportId(SysCfg_TRANSPORT_PMQ_ID);
    TransportPMQ_destroy();

    /* finalize IPC */
    Ipc_stop();
}

/*
 *  ======== Ctrl_exec ========
 */
int Ctrl_exec(void)
{
    Int                 status;
    MessageQ_Msg        mqMsg;
    MessageQ_QueueId    qid;
    Graph_Msg          *msg;

    Ctrl_mod.run = TRUE;

    /* main loop */
    while (Ctrl_mod.run) {

        /* wait for inbound message */
        status = MessageQ_get(Ctrl_mod.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            Ctrl_mod.run = FALSE;
            status = 0;
            goto leave;
        }
        else if (status < 0) {
            fprintf(stderr, "Error: Ctrl_exec: message receive failed, "
                    "error=%d\n", status);
            goto leave;
        }

        /* process the message */
        msg = (Graph_Msg *)mqMsg;

        switch (msg->cmd) {

            case Graph_Cmd_ATTACH:
                /* host attach to dsp */
                status = Ipc_attach(msg->uarg1);
                if (status < 0) {
                    fprintf(stderr, "Error: Ipc_attach(%d) failed\n",
                            msg->uarg1);
                }
                break;

            case Graph_Cmd_DETACH:
                /* host detach from dsp */
                status = Ipc_detach(msg->uarg1);
                if (status < 0) {
                    fprintf(stderr, "Error: Ipc_detach(%d) failed\n",
                            msg->uarg1);
                }
                break;

            case Graph_Cmd_FLUSH:
                msg->status = Graph_S_SUCCESS; /* nothing to do */
                break;

            case Graph_Cmd_INPUTCONNECT:
                msg->status = Graph_S_SUCCESS; /* nothing to do */
                break;

            case Graph_Cmd_INPUTDISCONNECT:
                msg->status = Graph_S_SUCCESS; /* nothing to do */
                break;

            case Graph_Cmd_OUTPUTCONNECT:
                msg->status = Graph_E_FAIL;
                break;

            case Graph_Cmd_OUTPUTDISCONNECT:
                msg->status = Graph_E_FAIL;
                break;

            case Graph_Cmd_SHUTDOWN:
                Sink_shutdown();
                Ctrl_mod.run = FALSE;
                break;

            default:
                fprintf(stderr, "Error: Ctrl_exec: unknown command, cmd=%d\n",
                        msg->cmd);
                break;
        }

        if (msg->reply) {
            /* return the message to sender */
            msg->reply = FALSE;
            msg->ack = TRUE;
            qid = MessageQ_getReplyQueue(mqMsg);

            status = MessageQ_put(qid, mqMsg);

            if (status < 0) {
                fprintf(stderr, "Error: message send failed\n");
                status = -2;
                goto leave;
            }
        }
        else {
            /* free message here */
            MessageQ_free(mqMsg);
        }
    }

leave:
    return (status);
}

/*
 *  ======== Ctrl_setup ========
 */
int Ctrl_setup(Ctrl_Arg *arg)
{
    Int status = 0;
    TransportPMQ_Handle transPMQ;
    ITransport_Handle transport;
    INetworkTransport_Handle transNetwork;
    char qname[BUFSZ];
    MessageQ_Params msgqParams;

    /* initialize module state */
    Ctrl_mod.node = arg->n;
    Ctrl_mod.run = FALSE;
    Ctrl_mod.queue = NULL;

    /* configure the transport factory */
    Ipc_transportConfig(&TransportRpmsg_Factory);

    /* initialize IPC */
    status = Ipc_start();

    if (status < 0) {
        fprintf(stderr, "Error: IPC init failed, error=%d\n", status);
        goto leave;
    }

    /* setup the TransportPMQ module */
    status = TransportPMQ_setup();

    if (status < 0) {
        status = -1;
        fprintf(stderr, "Error: TransportPMQ setup failed\n");
        goto leave;
    }
    transPMQ = TransportPMQ_handle();

    /* register transport with MessageQ */
    transNetwork = TransportPMQ_upCast(transPMQ);
    transport = INetworkTransport_upCast(transNetwork);
    MessageQ_registerTransportId(SysCfg_TRANSPORT_PMQ_ID, transport);

    /* inform data module that IPC is ready */
    Sink_ipcIsReady();

    /* create message queue (inbound messages) */
    MessageQ_Params_init(&msgqParams);
    snprintf(qname, BUFSZ, Consumer_CMDQUE_FMT, Ctrl_mod.node);

    Ctrl_mod.queue = MessageQ_create(qname, &msgqParams);

    if (Ctrl_mod.queue == NULL) {
        status = -2;
        fprintf(stderr, "Error: message queue create failed\n");
        goto leave;
    }

leave:
    return (status);
}

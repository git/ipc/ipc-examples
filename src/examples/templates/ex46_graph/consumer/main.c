/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== main.c ========
 */

/* standard headers */
#include <pthread.h>            /* pthread_create, pthread_join */
#include <stdio.h>              /* printf */
#include <stdlib.h>             /* exit, atoi */

/* package header files */
#include <ti/ipc/Std.h>

/* example header files */
#include "Ctrl.h"
#include "Sink.h"

#define Main_USAGE "\
Usage:\n\
    consumer n r\n\
    consumer -h\n\
\n\
Arguments:\n\
    n   : single character node name [A-Z]\n\
    r   : reserved message queue number\n\
\n\
Options:\n\
    h   : print this help message\n\
\n"

/* private functions */
int parse_args(int argc, char *argv[]);

/* private data */
static char main_n_arg = ' ';           /* node name */
static int main_r_arg = -1;             /* reserved queue index */


/*
 *  ======== main ========
 */
int main(int argc, char *argv[])
{
    int status = 0;
    pthread_t   ctrlThr;                /* control thread */
    Ctrl_Arg    ctrlArg;
    int         ctrlStatus = 0;
    pthread_t   sinkThr;                /* sink thread */
    Sink_Arg    sinkArg;
    int         sinkStatus = 0;

    /* parse command line */
    status = parse_args(argc, argv);

    if (status < 0) {
        goto leave;
    }

    /* start the control thread */
    ctrlArg.n = main_n_arg;
    status = pthread_create(&ctrlThr, NULL, Ctrl_threadFxn, &ctrlArg);

    if (status < 0) {
        fprintf(stderr, "Error: control thread failed\n");
        goto leave;
    }

    /* start the sink thread */
    sinkArg.node = main_n_arg;
    sinkArg.queIdx = main_r_arg;
    status = pthread_create(&sinkThr, NULL, Sink_threadFxn, &sinkArg);

    if (status < 0) {
        fprintf(stderr, "Error: sink thread failed\n");
        goto leave;
    }

    /* wait for sink thread to exit */
    pthread_join(sinkThr, (void **)(&sinkStatus));

    /* wait for control thread to exit */
    pthread_join(ctrlThr, (void **)(&ctrlStatus));

    status = ((ctrlStatus < 0) || (sinkStatus < 0)) ? -1 : 0;

leave:
    return (status);
}

/*
 *  ======== parse_args ========
 */
int parse_args(int argc, char *argv[])
{
    int x, cp, opt, argNum;
    int status = 0;

    /* parse the command line options */
    for (opt = 1; (opt < argc) && (argv[opt][0] == '-'); opt++) {
        for (x = 0, cp = 1; argv[opt][cp] != '\0'; cp++) {
            x = (x << 8) | (int)argv[opt][cp];
        }

        switch (x) {
            case 'h': /* -h */
                fprintf(stderr, "%s", Main_USAGE);
                exit(0);
                break;

            default:
                fprintf(stderr, "Error: invalid option, %c\n", (char)x);
                fprintf(stderr, "%s", Main_USAGE);
                status = -1;
                goto leave;
        }
    }

    /* parse the command line arguments */
    for (argNum = 1; opt < argc; argNum++, opt++) {

        switch (argNum) {
            case 1:
                main_n_arg = argv[opt][0];
                break;

            case 2:
                main_r_arg = atoi(argv[opt]);
                break;

            default:
                fprintf(stderr, "Error: too many arguments\n");
                fprintf(stderr, "%s", Main_USAGE);
                status = -1;
                goto leave;
        }
    }

    if (main_n_arg == ' ') {
        fprintf(stderr, "Error: missing node argument\n");
        fprintf(stderr, "%s", Main_USAGE);
        status = -1;
        goto leave;
    }

    if (main_r_arg == -1) {
        fprintf(stderr, "Error: missing queue index argument\n");
        fprintf(stderr, "%s", Main_USAGE);
        status = -1;
        goto leave;
    }

leave:
    return (status);
}

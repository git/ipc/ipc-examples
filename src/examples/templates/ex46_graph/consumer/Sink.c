/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Sink.c ========
 */

/* Standard headers */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/eventfd.h>                /* eventfd */
#include <sys/stat.h>
#include <sys/types.h>

/* package header files */
#include <ti/ipc/Std.h>
#include <ti/ipc/Ipc.h>
#include <ti/ipc/MessageQ.h>
#include <ti/ipc/MultiProc.h>

/* example header files */
#include "Sink.h"
#include "Consumer.h"
#include "../shared/Data.h"

#define BUFSZ 64

/* module structure */
typedef struct {
    Char                node;           /* node name */
    Bool                run;            /* main loop run flag */
    Bool                okayToSignal;   /* indicates eventfd is created */
    Bool                ipcReady;       /* IPC ready flag */
    int                 waitEvent;
    MessageQ_QueueIndex queIndex;       /* reserved queue index */
    MessageQ_Handle     queue;          /* data queue, inbound messages */
} Sink_Module;

/* private functions */
void Sink_destroy(void);
int Sink_exec(void);
int Sink_setup(Sink_Arg *arg);

static void Sink_process(Data_Buffer *dataBuf);

/* private data */
static Sink_Module Sink_mod = {
    .node           = '-',
    .run            = FALSE,
    .okayToSignal   = FALSE,
    .ipcReady       = FALSE,
    .waitEvent      = -1,
    .queIndex       = MessageQ_INVALIDMESSAGEQ,
    .queue          = NULL
};

/*
 *  ======== Sink_threadFxn ========
 */
void *Sink_threadFxn(void *param)
{
    int status = 0;
    Sink_Arg *arg = (Sink_Arg *)param;

    /* setup phase */
    status = Sink_setup(arg);

    if (status < 0) {
        goto leave;
    }

    /* execute phase */
    status = Sink_exec();

leave:
    /* shutdown phase */
    Sink_destroy();

    return ((void *)status);
}

/*
 *  ======== Sink_ipcDone ========
 */
int Sink_ipcDone(void)
{
    return (Sink_mod.ipcReady ? 0 : 1);
}

/*
 *  ======== Sink_ipcIsReady ========
 *  Inform this task that IPC is ready
 *
 *  This function is called from another execution context. Must
 *  be careful to ensure local resources have been created.
 */
void Sink_ipcIsReady(void)
{
    uint64_t value = 1;

    Sink_mod.ipcReady = TRUE;

    /* event might not yet exist, must check if okay to signal */
    if (Sink_mod.okayToSignal) {
        write(Sink_mod.waitEvent, &value, sizeof(value));
    }
}

/*
 *  ======== Sink_shutdown ========
 */
void Sink_shutdown(void)
{
    Sink_mod.run = FALSE;
    MessageQ_unblock(Sink_mod.queue);
}

/*
 *  ======== Sink_destroy ========
 */
void Sink_destroy(void)
{

    /* delete the message queue */
    MessageQ_delete(&Sink_mod.queue);

    /* done using IPC */
    Sink_mod.ipcReady = FALSE;

    /* close wait event */
    Sink_mod.okayToSignal = FALSE;
    close(Sink_mod.waitEvent);
}

/*
 *  ======== Sink_exec ========
 */
int Sink_exec(void)
{
    Int                 status;
    MessageQ_Msg        mqMsg;
    MessageQ_QueueId    qid;
    Data_Buffer        *dataBuf;

    Sink_mod.run = TRUE;

    /* main loop */
    while (Sink_mod.run) {

        /* wait for inbound message */
        status = MessageQ_get(Sink_mod.queue, &mqMsg, MessageQ_FOREVER);

        if (status == MessageQ_E_UNBLOCKED) {
            Sink_mod.run = FALSE;
            status = 0;
            continue;
        }
        else if (status < 0) {
            fprintf(stderr, "Error: Sink_exec: message receive failed, "
                    "error=%d\n", status);
            goto leave;
        }

        /* process the message */
        dataBuf = &((Data_Msg *)mqMsg)->data;
        Sink_process(dataBuf);

        /* return the message to sender */
        qid = MessageQ_getReplyQueue(mqMsg);

        status = MessageQ_put(qid, mqMsg);

        if (status < 0) {
            fprintf(stderr, "Error: message send failed\n");
            status = -2;
            goto leave;
        }
    }

leave:
    return (status);
}

/*
 *  ======== Sink_setup ========
 */
int Sink_setup(Sink_Arg *arg)
{
    Int status = 0;
    uint64_t value;
    MessageQ_Params msgqParams;

    /* initialize module state */
    Sink_mod.node = arg->node;
    Sink_mod.queIndex = arg->queIdx;

    /* create event object */
    Sink_mod.waitEvent = eventfd(0, 0);

    if (Sink_mod.waitEvent == -1) {
        perror("Sink_setup: create eventfd failed");
        status = -1;
        goto leave;
    }

    Sink_mod.okayToSignal = TRUE;

    /* wait here until IPC is ready */
    if (!Sink_mod.ipcReady) {
        read(Sink_mod.waitEvent, &value, sizeof(value));
    }

    /* create reserved message queue (data queue) */
    MessageQ_Params_init(&msgqParams);
    msgqParams.queueIndex = Sink_mod.queIndex;

    Sink_mod.queue = MessageQ_create(NULL, &msgqParams);

    if (Sink_mod.queue == NULL) {
        status = -2;
        fprintf(stderr, "Error: message queue create failed\n");
        goto leave;
    }

leave:
    return (status);
}

/*
 *  ======== Sink_process ========
 */
static void Sink_process(Data_Buffer *dataBuf)
{
    Int count;
    UInt *buffer;
    int i;

    count = dataBuf->count;
    buffer = dataBuf->buffer;

    for (i = 0; i < count; i++) {
        buffer[i] = buffer[i] | 1;
    }
}

/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== SysCfg_shared.h ========
 */

#ifndef SysCfg_shared__include
#define SysCfg_shared__include
#if defined (__cplusplus)
extern "C" {
#endif


/* HOST reserved message queues */
#define SysCfg_PER_MsgQ     0           /* Processor Endpoint Registrar */
#define SysCfg_PEC_MsgQ     1           /* Processor Endpoint Client */
#define SysCfg_ResMgr       2           /* Resource Manager */

/* DSP reserved message queue */
#define SysCfg_Compute_CMD_RQUE         2
#define SysCfg_DATA_RQUE                3

/* transport message queue ID */
#define SysCfg_TRANSPORT_PMQ_ID         1

/* reserved message queue IDs */
#define SysCfg_Cmd_MSGID                1
#define SysCfg_Graph_MSGID              2
#define SysCfg_Data_MSGID               3

/* MessageQ Heap ID */
#define SysCfg_RPMsg_HeapID             0
#define SysCfg_CtrlMsg_HeapID           1
#define SysCfg_DataMsg_HeapID           2

#define SysCfg_CtrlHeapNameFmt          "CtrlHeap_Proc%d"
#define SysCfg_CtrlHeapNumBlocks        12


#if defined (__cplusplus)
}
#endif
#endif

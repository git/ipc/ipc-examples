/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Data.h ========
 */

#ifndef Data__include
#define Data__include

#include <ti/ipc/MessageQ.h>

#if defined (__cplusplus)
extern "C" {
#endif


#define Data_BUFSZ 50

typedef struct {
    Int     count;                      /* number of elements in buffer */
    UInt    buffer[Data_BUFSZ];         /* data buffer */
} Data_Buffer;

#define DB_buffer_offset ((UInt)&((Data_Buffer *)0)->buffer)
#define Data_DBADDR(buffer) (Data_Buffer *)((Char *)buffer - DB_buffer_offset)

typedef struct {
    MessageQ_MsgHeader  header;
    Data_Buffer         data;
} Data_Msg;

#define Data_Msg__header(header) (Data_Msg *)(header)

#define DM_data_offset ((UInt)&((Data_Msg *)0)->data)
#define Data_MQADDR(data) (MessageQ_Msg)((Char *)data - DM_data_offset)

/* reserved message queue ID */
#define Data_MSGID SysCfg_Data_MSGID

#if defined (__cplusplus)
}
#endif
#endif

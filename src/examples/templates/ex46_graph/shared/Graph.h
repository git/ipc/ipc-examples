/*
 * Copyright (c) 2015 Texas Instruments Incorporated - http://www.ti.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Graph_shared.h ========
 */

#ifndef Graph_shared__include
#define Graph_shared__include

#include <ti/ipc/MessageQ.h>

#if defined (__cplusplus)
extern "C" {
#endif


typedef struct {
    MessageQ_MsgHeader reserved;
    Int     cmd;
    Int     arg1;
    Int     arg2;
    UInt    uarg1;
    UInt    uarg2;
    UInt    dcid;       /* deferred command ID */
    Int     reply;      /* send reply message */
    Int     ack;        /* acknowledgement message */
    Int     status;
} Graph_Msg;

#define Graph_HEAPBLOCKSIZE     ((sizeof(Graph_Msg) + 3) & (~3))

#define Graph_Cmd_ATTACH           1
#define Graph_Cmd_DATAQUERY        2
#define Graph_Cmd_DETACH           3
#define Graph_Cmd_FLUSH            4
#define Graph_Cmd_INFO             5
#define Graph_Cmd_INPUTCONNECT     6    /* connect input from upstream node */
#define Graph_Cmd_INPUTDISCONNECT  7    /* disconnect from upstream node */
#define Graph_Cmd_OUTPUTCONNECT    8    /* connect output to downstream node */
#define Graph_Cmd_OUTPUTDISCONNECT 9    /* disconnect to downstream node */
#define Graph_Cmd_PAUSE            10
#define Graph_Cmd_RELEASE          11
#define Graph_Cmd_REQUEST          12
#define Graph_Cmd_RUN              13
#define Graph_Cmd_SHUTDOWN         14

/* reserved message queue ID */
#define Graph_MSGID SysCfg_Graph_MSGID

#define Graph_S_SUCCESS         (0)
#define Graph_E_FAIL            (-1)

#define Graph_Info_DATASRC      1

#if defined (__cplusplus)
}
#endif
#endif
